package com.ntels.nise.test;
import jssc.SerialPort;
import jssc.SerialPortList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by youngdong on 2017-11-27.
 */
public class RxtxJSSCTest {

    public static BlockingQueue<Byte[]> queue = new LinkedBlockingQueue<Byte[]>();
    public static List<Byte> current_list = Collections.synchronizedList(new ArrayList<Byte>());

    public static void main(String[] arg)
    {

        try {
            String[] portNames = SerialPortList.getPortNames();
            for (int i = 0; i < portNames.length; i++) {
                System.out.println(portNames[i]);
            }
            SerialPort serialPort = new SerialPort("COM3");
            serialPort.openPort();//Open serial port

            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            new ReadThread(serialPort).start();
//            new WriteThread(serialPort).start();
            new PollThread().start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public static class ReadThread extends Thread{
        SerialPort serial;
        ReadThread(SerialPort serial){
            this.serial = serial;
        }

        public void run() {
            try {
                while (true) {
                    byte[] read = serial.readBytes();
                    if(read != null && read.length > 0){
                        addHEX(read);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void addHEX(byte[] read)
    {

        for(final byte b: read) {
            current_list.add(b);
            if(b==0x04)
            {
                try {
                    queue.put((Byte[])current_list.toArray(new Byte[current_list.size()]));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                current_list = Collections.synchronizedList(new ArrayList<Byte>());
            }
        }
    }

    public static class WriteThread extends Thread {
        SerialPort serial;

        WriteThread(SerialPort serial) {
            this.serial = serial;
        }

        public void run() {
            try {
                int c = 0;

                System.out.println("\nKeyborad Input Read!!!!");
                while ((c = System.in.read()) > -1) {
                    serial.writeInt(c);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    public static class PollThread extends Thread {

        PollThread() {
        }

        public void run() {
            try {
                while(true)
                {
                    if(queue.size() > 0) {
                        Byte[] d;
                        while ((d = queue.poll()) != null)
                        {
                            if(d[0]!=0x06)
                                continue;

                            StringBuffer sb = new StringBuffer();
                            for(byte b : d)
                                sb.append(String.format("%02x ", b & 0xff));

                            System.out.println("# data : " + sb.toString());
                        }
                    }

                    Thread.sleep(1000);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
