package com.ntels.nise.test;

import com.ntels.nise.io.protocol.tcp.TCPClient;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;

/**
 * Created by youngdong on 2016-07-29.
 */
public class TCPTestVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        super.start();
        test();
    }

    private void test()
    {
        TCPClient client = new TCPClient(vertx, config().getJsonObject("tcp.client.test"), "yyd.tcp.send", "yyd.tcp.recv");

    }


}
