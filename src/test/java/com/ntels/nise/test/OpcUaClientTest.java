/*
 * Copyright 2016 Kevin Herron
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ntels.nise.test;

import static com.digitalpetri.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;
import static com.google.common.collect.Lists.asList;
import static com.google.common.collect.Lists.newArrayList;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.digitalpetri.opcua.sdk.client.SessionActivityListener;
import com.digitalpetri.opcua.sdk.server.api.config.OpcUaServerConfig;
import com.digitalpetri.opcua.server.ctt.CttNamespace;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.digitalpetri.opcua.sdk.client.OpcUaClient;
import com.digitalpetri.opcua.sdk.client.api.UaSession;
import com.digitalpetri.opcua.sdk.client.api.config.OpcUaClientConfig;
import com.digitalpetri.opcua.sdk.client.api.identity.UsernameProvider;
import com.digitalpetri.opcua.sdk.client.api.nodes.attached.UaVariableNode;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaSubscription;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaSubscriptionManager.SubscriptionListener;
import com.digitalpetri.opcua.sdk.server.OpcUaServer;
import com.digitalpetri.opcua.stack.client.UaTcpStackClient;
import com.digitalpetri.opcua.stack.core.AttributeId;
import com.digitalpetri.opcua.stack.core.Identifiers;
import com.digitalpetri.opcua.stack.core.UaException;
import com.digitalpetri.opcua.stack.core.UaServiceFaultException;
import com.digitalpetri.opcua.stack.core.security.SecurityPolicy;
import com.digitalpetri.opcua.stack.core.types.builtin.*;
import com.digitalpetri.opcua.stack.core.types.builtin.unsigned.UInteger;
import com.digitalpetri.opcua.stack.core.types.enumerated.MonitoringMode;
import com.digitalpetri.opcua.stack.core.types.enumerated.TimestampsToReturn;
import com.digitalpetri.opcua.stack.core.types.structured.*;
import com.digitalpetri.opcua.stack.server.tcp.SocketServer;

public class OpcUaClientTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static OpcUaClient client;
    private OpcUaServer server;


    public OpcUaClientTest(){

    }



    public static void main(String[] args) throws Exception
    {

//        serverTest();

        clientTest();
//        testReactivate();
    }

    private static OpcUaClientConfig clientConfig;

    private static void serverTest(){
        OpcUaServerConfig serverConfig = OpcUaServerConfig.builder()
                .setApplicationName(LocalizedText.english("digitalpetri opc-ua server"))
                .setApplicationUri("urn:digitalpetri:opcua:server")
                .setBindAddresses(newArrayList("localhost"))
                .setBindPort(12686)
                .setSecurityPolicies(EnumSet.of(SecurityPolicy.None, SecurityPolicy.None))
                .setProductUri("urn:digitalpetri:opcua:sdk")
                .setServerName("test-server")
                .build();

        OpcUaServer server = new OpcUaServer(serverConfig);

        // register a CttNamespace so we have some nodes to play with
        server.getNamespaceManager().registerAndAdd(
                CttNamespace.NAMESPACE_URI,
                idx -> new CttNamespace(server, idx));

        server.startup();
    }

    private static void clientTest() {

        try {
            EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints("opc.tcp://127.0.0.1:49321").get();

            EndpointDescription endpoint = Arrays.stream(endpoints)
                    .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
                    .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));

            clientConfig = OpcUaClientConfig.builder()
                    .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
                    .setApplicationUri("urn:digitalpetri:opcua:client")
                    .setEndpoint(endpoint)
                    .setRequestTimeout(uint(5000))
                    .setSessionTimeout(uint(3000))
                    .build();

            client = new OpcUaClient(clientConfig);



//            testSubscribeAll();

            testSubscribe();
//            testRead();
//            subscribe(client);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static String[] getTags()
    {
        String tagString = "SIM.DEV1.Random1,SIM.DEV1.Random2,SIM.DEV1.Random3,SIM.DEV1.Random4,SIM.DEV1.Random5,SIM.DEV1.Random6,SIM.DEV1.Random7,SIM.DEV1.Random8,SIM.DEV1.Random9,SIM.DEV1.Random10,SIM.DEV1.Random11";

        return tagString.split(",");
    }

    public static List<MonitoredItemCreateRequest> getTagList(String[] tags)
    {
        List<MonitoredItemCreateRequest> result = new LinkedList<MonitoredItemCreateRequest>();

        MonitoringParameters parameters = new MonitoringParameters(
                uint(10),    // client handle
                1000.0,     // sampling interval
                null,       // no (default) filter
                uint(10),   // queue size
                true);      // discard oldest
        int i=0;
        for(String tag : tags)
        {
            System.out.println("# " + tag);
            ReadValueId readValueId = new ReadValueId(
                    new NodeId(i++, tag),
                    AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);

            MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(
                    readValueId, MonitoringMode.Reporting, parameters);

            result.add(request);
        }

        return result;
    }


    public static void subscribe(OpcUaClient client) throws ExecutionException, InterruptedException, TimeoutException {

        UaSubscription subscription = client.getSubscriptionManager().createSubscription(1000.0).get();

        String[] tagArray = getTags();
        List<MonitoredItemCreateRequest> monitorList = getTagList(tagArray);

        List<UaMonitoredItem> items = subscription
                .createMonitoredItems(TimestampsToReturn.Both, monitorList).get();

        System.out.println(" size : " + items.size());

        while(true) {
            for (UaMonitoredItem item : items) {
                // do something with the value updates
//            UaMonitoredItem item = items.get(0);

                CompletableFuture<DataValue> f = new CompletableFuture<>();
                item.setValueConsumer(f::complete);

                DataValue dd = f.get(5, TimeUnit.SECONDS);

//                System.out.println("data : " + dd.toString());
                System.out.println("tag : " + item.getReadValueId().getNodeId().getIdentifier() + ", value : " + dd.getValue().getValue());
            }
        }
    }



    public void stopClientAndServer() {
        logger.info("stopClientAndServer()");

        try {
            client.disconnect().get();
        } catch (InterruptedException | ExecutionException e) {
            logger.warn("Error disconnecting client.", e);
        }
        server.shutdown();
        SocketServer.shutdownAll();
    }

    public static void testRead() throws Exception {

        UaVariableNode currentTimeNode = client.getAddressSpace()
            .getVariableNode(Identifiers.Server_ServerStatus_CurrentTime);

        System.out.println("## " + currentTimeNode.readValueAttribute().get().toString() );
    }

    public void testWrite() throws Exception {
        logger.info("testWrite()");

        NodeId nodeId = new NodeId(2, "/Static/AllProfiles/Scalar/Int32");

        UaVariableNode variableNode = client.getAddressSpace().getVariableNode(nodeId);

        // read the existing value
        Object valueBefore = variableNode.readValueAttribute().get();

        // write a new random value
        DataValue newValue = new DataValue(new Variant(new Random().nextInt()));
        StatusCode writeStatus = variableNode.writeValue(newValue).get();
        writeStatus.isGood();

        // read the value again
        Object valueAfter = variableNode.readValueAttribute().get();

    }

    public static void testSubscribeAll() throws Exception {

        UaVariableNode currentTimeNode = client.getAddressSpace()
                .getVariableNode(Identifiers.Server_ServerStatus_CurrentTime);

        DataValue value = currentTimeNode.readValue().get();


        try{ Thread.sleep(1000*60*5);}catch (Exception e){}

    }

    public static void testSubscribe() throws Exception {

        System.out.println("=====================================");
        System.out.println(" Start !!!");
        System.out.println("=====================================");

        // create a subscription and a monitored item
//        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0).get();
        client.connect().get();
        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0, UInteger.valueOf(100),UInteger.valueOf(100),UInteger.valueOf(100),true, null).get();

//        NodeId nodeId = new NodeId(2, "HV.1-8.Supply Fan VFD Modulation.PresentValue");
//        NodeId nodeId = new NodeId(2, "HV1-1.1109.AI_3.V");
        NodeId nodeId = new NodeId(2, "10HV.2701.BI_4.V");
//        NodeId nodeId =  Identifiers.Server_NamespaceArray;
        ReadValueId readValueId = new ReadValueId( nodeId, AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);


        MonitoringParameters parameters = new MonitoringParameters(
                uint(1),    // client handle
                300.0,     // sampling interval
                null,       // no (default) filter
                uint(20),   // queue size
                false);      // discard oldest

        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(readValueId, MonitoringMode.Reporting, parameters);

        List<UaMonitoredItem> items = subscription
                .createMonitoredItems(TimestampsToReturn.Server, newArrayList(request)).get();

        client.addSessionActivityListener(new SessionActivityListener() {

            private boolean isRun = false;

            @Override
            public void onSessionInactive(UaSession session) {

                while(!isRun) {
                    try {
                        Thread.sleep(1000);
                        System.out.println("----Start --------------------");
//                        client.connect().get();
                        client = new OpcUaClient(clientConfig);
                        System.out.println("----End --------------------");
                        testSubscribe();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onSessionActive(UaSession session) {
                isRun = true;
            }
        });

        client.getSubscriptionManager().addSubscriptionListener(new SubscriptionListener() {
            @Override
            public void onKeepAlive(UaSubscription uaSubscription, DateTime dateTime) {
                System.out.println("### 1");
            }

            @Override
            public void onStatusChanged(UaSubscription uaSubscription, StatusCode statusCode) {
                System.out.println("### 2");
            }

            @Override
            public void onPublishFailure(UaException e) {
                System.out.println("### 3 : " + e.getMessage());
            }

            @Override
            public void onNotificationDataLost(UaSubscription uaSubscription) {
                System.out.println("### 4");
            }

            @Override
            public void onSubscriptionTransferFailed(UaSubscription uaSubscription, StatusCode statusCode) {
                System.out.println("#### Subscription Failed : " + statusCode.toString());
            }
        });

        NodeId nodeId3 = new NodeId(2, "10HV.2701.BI_5.V");
//        NodeId nodeId2 = new NodeId(2, "EL.1-402921.1st-MONTH");

        UaVariableNode variableNode = client.getAddressSpace().getVariableNode(nodeId3);


        System.out.println("@@ check : " + variableNode.readValueAttribute().get());
        // write a new random value
        DataValue newValue = new DataValue(new Variant(1), null, null);
        StatusCode writeStatus = variableNode.writeValueAttribute(3.0).get();
        System.out.println("### 10HV.2701.BI_5.V  isGood : " + writeStatus.isGood());

        List<StatusCode> ss = client.writeValues(Arrays.asList(nodeId3), Arrays.asList(newValue)).get();
        System.out.println("### 10HV.2701.BI_5.V  isGood : " + ss.get(0).isGood());

        System.out.println(" size : " + items.size());

//        while(true) {
        for (UaMonitoredItem item : items) {
            // do something with the value updates
//            UaMonitoredItem item = items.get(0);

//                CompletableFuture<DataValue> f = new CompletableFuture<>();
//                item.setValueConsumer(f::complete);
            item.setValueConsumer( handler -> {
                System.out.println("data : " + handler.toString());
                System.out.println("tag : " + item.getReadValueId().getNodeId().getIdentifier());
                System.out.println("value : " + handler.getValue().getValue());
            });
                try{ Thread.sleep(1000*60*5);}catch (Exception e){}
//                DataValue dd = f.get(0, TimeUnit.SECONDS);
//
//                System.out.println("data : " + dd.toString());
//                System.out.println("value : " + dd.getValue().getValue());
        }
//        }


    }

    public void testTransferSubscriptions() throws Exception {
        logger.info("testTransferSubscriptions()");

        // create a subscription and a monitored item
        UaSubscription subscription = client.getSubscriptionManager().createSubscription(1000.0).get();

        NodeId nodeId = new NodeId(2, "/Static/AllProfiles/Scalar/Int32");

        ReadValueId readValueId = new ReadValueId(
            nodeId, AttributeId.Value.uid(),
            null, QualifiedName.NULL_VALUE);

        MonitoringParameters parameters = new MonitoringParameters(
            uint(1),    // client handle
            100.0,      // sampling interval
            null,       // no (default) filter
            uint(10),   // queue size
            true);      // discard oldest

        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(
            readValueId, MonitoringMode.Reporting, parameters);

        List<UaMonitoredItem> items = subscription
            .createMonitoredItems(TimestampsToReturn.Both, newArrayList(request)).get();

        // do something with the value updates
        UaMonitoredItem item = items.get(0);

        AtomicInteger updateCount = new AtomicInteger(0);

        item.setValueConsumer(v -> {
            int count = updateCount.incrementAndGet();
            logger.info("updateCount={}", count);
        });

        AtomicBoolean subscriptionTransferred = new AtomicBoolean(true);

        client.getSubscriptionManager().addSubscriptionListener(new SubscriptionListener() {
            @Override
            public void onKeepAlive(UaSubscription subscription, DateTime publishTime) {

            }

            @Override
            public void onStatusChanged(UaSubscription subscription, StatusCode status) {

            }

            @Override
            public void onPublishFailure(UaException exception) {

            }

            @Override
            public void onNotificationDataLost(UaSubscription subscription) {

            }

            @Override
            public void onSubscriptionTransferFailed(UaSubscription subscription, StatusCode statusCode) {
                subscriptionTransferred.set(false);
            }
        });

        logger.info("killing the session...");
        UaSession uaSession = client.getSession().get();
        server.getSessionManager().killSession(uaSession.getSessionId(), false);

        logger.info("sleeping while waiting for an update");
        Thread.sleep(5000);

        // one update for the initial subscribe, another after transfer

        subscriptionTransferred.get();

        client.disconnect().get();
    }


    /**
     * Test using a username and password long enough that the encryption requires multiple ciphertext blocks.
     *
     * @throws Exception
     */
    public void testUsernamePassword_MultiBlock() throws Exception {
        logger.info("testUsernamePassword_MultiBlock()");

        EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints("opc.tcp://localhost:12686/test-server").get();

        EndpointDescription endpoint = Arrays.stream(endpoints)
            .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
            .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));

        char[] cs = new char[1000];
        Arrays.fill(cs, 'a');
        String user = new String(cs);
        String pass = new String(cs);

        OpcUaClientConfig clientConfig = OpcUaClientConfig.builder()
            .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
            .setApplicationUri("urn:digitalpetri:opcua:client")
            .setEndpoint(endpoint)
            .setRequestTimeout(uint(60000))
            .setIdentityProvider(new UsernameProvider(user, pass))
            .build();

        OpcUaClient client = new OpcUaClient(clientConfig);

        client.connect().get();
    }

    public void testConnectAndDisconnect() throws Exception {
        logger.info("testConnectAndDisconnect()");

        EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints("opc.tcp://localhost:12686/test-server").get();

        EndpointDescription endpoint = Arrays.stream(endpoints)
            .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
            .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));

        class ConnectDisconnect implements Runnable {
            private final int threadNumber;

            private ConnectDisconnect(int threadNumber) {
                this.threadNumber = threadNumber;
            }

            private OpcUaClientConfig clientConfig = OpcUaClientConfig.builder()
                    .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
                    .setApplicationUri("urn:digitalpetri:opcua:client")
                    .setEndpoint(endpoint)
                    .setRequestTimeout(uint(10000))
                    .build();

            private OpcUaClient client = new OpcUaClient(clientConfig);

            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    try {
                        client.connect().get();

                        client.readValues(
                                0.0,
                                TimestampsToReturn.Both,
                                newArrayList(Identifiers.Server_ServerStatus_CurrentTime)
                        ).get();

                        client.disconnect().get();

                        Thread.sleep(10);
                    } catch (InterruptedException | ExecutionException e) {
                        e.getMessage();
                    }
                }
                logger.info("Thread {} done.", threadNumber);
            }
        }

        Thread[] threads = new Thread[4];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new ConnectDisconnect(i));
            threads[i].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
    }


    public static void testReactivate() throws Exception {
        System.out.println("testReactivate()");

        EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints("opc.tcp://192.168.0.6:49321").get();

        EndpointDescription endpoint = Arrays.stream(endpoints)
            .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
            .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));

        OpcUaClientConfig clientConfig = OpcUaClientConfig.builder()
            .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
            .setApplicationUri("urn:digitalpetri:opcua:client")
            .setEndpoint(endpoint)
            .setRequestTimeout(uint(10000))
            .build();

        OpcUaClient client = new OpcUaClient(clientConfig);

        UaVariableNode currentTimeNode = client.getAddressSpace()
            .getVariableNode(Identifiers.Server_ServerStatus_CurrentTime);

        currentTimeNode.readValueAttribute().get();

        // Kill the session. Client can't and won't be notified of this.
        System.out.println("killing session...");
        UaSession session = client.getSession().get();
//        server.getSessionManager().killSession(session.getSessionId(), true);

        // Expect the next action to fail because the session is no longer valid.
        try {
            System.out.println("reading, expecting failure...");
            currentTimeNode.readValueAttribute().get();
        } catch (Throwable t) {
            StatusCode statusCode = UaServiceFaultException.extract(t)
                .map(UaException::getStatusCode)
                .orElse(StatusCode.BAD);

//            statusCode.getValue(), StatusCodes.Bad_SessionIdInvalid;
        }

        Thread.sleep(1000);

        // Force a reactivate and read.
        System.out.println("reconnecting...");
        client.connect().get();

        System.out.println("reading, expecting success...");
        currentTimeNode.readValueAttribute().get();

        client.disconnect().get();
    }

}
