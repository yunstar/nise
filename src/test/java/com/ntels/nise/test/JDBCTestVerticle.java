package com.ntels.nise.test;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;

/**
 * Created by youngdong on 2016-07-15.
 */
public class JDBCTestVerticle extends BaseVerticle {

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

        JDBCClient client = JDBCClient.createShared(vertx, new JsonObject()
                .put("url", "jdbc:mariadb://192.168.0.18:3306/A00000000001")
                .put("driver_class", "org.mariadb.jdbc.Driver")
                .put("user", "root")
                .put("password", "ntels")
                .put("max_pool_size", 10));

//        client.getConnection( connection -> {
//            SQLConnection conn = null;
//            try {
//                JsonObject query = updateBatchQuery();
//                String sql = query.getString("sql");
//                JsonArray params = query.getJsonArray("params");
//
//                conn = connection.result();
//                if (params == null) {
//                    conn.batch(query.getJsonArray("sql").getList(), res -> {
//                        if(res.failed())
//                            System.out.println(res.cause().getMessage());
//                    });
//                } else {
//                    conn.batchWithParams(sql, params.getList(), res -> {
//                        if(res.failed())
//                            System.out.println(res.cause().getMessage());
//                    });
//                }
//            }catch(Exception e){
//                System.out.println(e.getMessage());
//            }finally {
//                try{ conn.close(); }catch(Exception e){}
//            }
//        });

        // insert, update, delete
//        vertx.eventBus().send(EventBusAddress.SQL.UPDATE, updateQuery(), rs -> {
//            if(rs.failed())
//                System.out.println(rs.cause().getMessage());
//        });


        // batch insert, update, delete
//        vertx.eventBus().send(EventBusAddress.SQL.UPDATE_BATCH, updateBatchQuery(), rs -> {
//            if(rs.failed())
//                System.out.println(rs.cause().getMessage());
//        });

        // select
//        vertx.eventBus().send(EventBusAddress.SQL.SELECT, selectQuery(), rs -> {
//            if(rs.failed()) {
//                System.out.println(rs.cause().getMessage());
//                return;
//            }
//
//            if(rs.succeeded())
//            {
//                JsonArray rows = (JsonArray)rs.result().body();
//
//                for(int i=0;i<rows.size();i++)
//                {
//                    JsonObject row = rows.getJsonObject(i);
//                    System.out.println("### name -> " + row.getString("name") + "   sn -> " + row.getInteger("sn"));
//                }
//
//            }
//
//        });

    }




    private JsonObject executeQuery()
    {
        JsonObject query = new JsonObject();
//        query.put("sql", "create table sample(name varchar(10), sn int)");
        query.put("sql", "truncate table yyd");
        return query;
    }

    private JsonObject updateQuery()
    {
        JsonObject query = new JsonObject();
        query.put("sql", "insert into yyd(name, sn) values(?, ?)");
        query.put("params", new JsonArray().add("bb").add(2) );
        return query;
    }

    private JsonObject insertBatchQuery()
    {
        JsonObject query = new JsonObject();
        query.put("sql", "insert into yyd(name, sn) values(?, ?)");

        JsonArray params = new JsonArray();
        query.put("params", params);
        params.add( new JsonArray().add("cc").add(3) );
        params.add( new JsonArray().add("dd").add(4) );
        params.add( new JsonArray().add("ee").add(5) );

        return query;
    }

    private JsonObject updateBatchQuery()
    {
        JsonObject query = new JsonObject();
        query.put("sql", "update yyd set sn=? where name=?");

        JsonArray params = new JsonArray();
        query.put("params", params);
        params.add( new JsonArray().add(13).add("cc") );
        params.add( new JsonArray().add(14).add("dd") );
        params.add( new JsonArray().add(15).add("ee") );

        return query;
    }

    private JsonObject selectQuery()
    {
        JsonObject query = new JsonObject();
        query.put("sql", "select * from yyd where sn > ?");
        query.put("params", new JsonArray().add(2) );
        return query;
    }


    public static void main(String args[]) {
        VertxOptions options = new VertxOptions().setClustered(true);

        Vertx.clusteredVertx(options, cRslt -> {
            Vertx vertx = cRslt.result();
            vertx.deployVerticle(JDBCTestVerticle.class.getName());
        });
    }
}
