package com.ntels.nise.test;

import com.ntels.nise.io.protocol.opc.OPCUAClient;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2017-01-19.
 */
public class OpcUaClientTestNew extends BaseVerticle {

    private String send = "send.test";
    private String recv = "recv.test";

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        vertx.eventBus().consumer(recv, handler ->{
           JsonObject data = (JsonObject)handler.body();
            System.out.println("# recv : " + data.toString());
        });

        JsonArray tags = new JsonArray();
        tags.add("TEST.Device1.1");
        tags.add("TEST.Device1.2");

        OPCUAClient client = new OPCUAClient(vertx, new JsonObject().put("endpoint", "opc.tcp://127.0.0.1:49321"), send, recv, tags);

        client.updateTagList(tags);

        client.createMonitoring();

//        vertx.setTimer(10000, t -> {
//            JsonObject msg = new JsonObject();
//            msg.put("id", "10HV.2701.BI_4.V");
//            msg.put("v", "11.11");
//            vertx.eventBus().send(send, msg, handler ->{
//                if(handler.failed())
//                {
//                    System.out.println("# send err : " + handler.cause().getMessage());
//                }
//            });
//        });

    }

    public static void main(String args[]) {
        VertxOptions options = new VertxOptions().setClustered(false);

        Vertx.clusteredVertx(options, cRslt -> {
            Vertx vertx = cRslt.result();
            vertx.deployVerticle(OpcUaClientTestNew.class.getName());
        });
    }
}
