package com.ntels.nise.test;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-13.
 */
public class SimpleVerticle extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();

        JsonObject testFormat = new JsonObject();
        testFormat.put("command_id", "a");
        testFormat.put("work_type", "b");
        testFormat.put("work_result", "c");

        System.out.println("test start !");

        vertx.eventBus().send(EventBusAddress.COMP_FORMAT_DOWNLOAD_APP.toString(), testFormat, rs -> {
            try {
                System.out.println("call");
                if (rs.succeeded()) {
                    JsonObject result = (JsonObject) rs.result().body();
                    System.out.println("#### data ####\n" + result.encodePrettily());
                } else {
                    System.out.println("fail");
                    System.out.println(rs.cause().getMessage());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        });


    }
}
