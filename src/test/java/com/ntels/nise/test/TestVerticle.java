package com.ntels.nise.test;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.EventBusAddress;
import io.vertx.core.*;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by youngdong on 2016-08-16.
 */
public class TestVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        super.start();

        System.out.println("init !!!");

        init();


        vertx.setTimer(1000, h -> {

            System.out.println(" recv data");
            getQueueData();

            System.out.println(" put data");
            byte[] b1 = {0x01,0x02,0x03,0x04};
            recv(b1[0]);
            recv(b1[1]);
            recv(b1[2]);
            recv(b1[3]);

            byte[] b2 = {0x05,0x06,0x07,0x04};
            recv(b2[0]);
            recv(b2[1]);
            recv(b2[2]);
            recv(b2[3]);


            vertx.setTimer(1000, h2 -> {


                byte[] b3 = {0x10,0x11,0x12,0x04};
                recv(b3[0]);
                recv(b3[1]);
                recv(b3[2]);
                recv(b3[3]);

            });


        });
    }


    private void init()
    {
        vertx.sharedData().<String, BlockingQueue<byte[]>>getClusterWideMap("hex.real", h -> {
            h.result().<String, BlockingQueue<byte[]>>put("q", new LinkedBlockingQueue<>(), res -> {
                if (res.failed())
                    System.out.println("ERR : " + res.cause().getMessage());
            });
        });

        vertx.sharedData().<String, List<Byte>>getClusterWideMap("hex.real", h -> {
            h.result().<String, List<Byte>>put("data", new ArrayList<Byte>(), res -> {
                if(res.failed())
                    System.out.println("ERR : " + res.cause().getMessage());
            });
        });
    }

    private void recv(byte b)
    {
        vertx.sharedData().<String, List<Byte>>getClusterWideMap("hex.real", h -> {
            AsyncMap<String, List<Byte>> m = h.result();
            m.get("data", res -> {

                if(res.succeeded())
                {

                    List<Byte> current_list = res.result();

                    current_list.add(b);

                    if(b==0x04)
                    {
                        StringBuffer sb = new StringBuffer();
                        for(Byte bb : current_list)
                            sb.append(String.format(" %02x ", bb & 0xff));
                        System.out.println("[" + sb.toString() + "]" );

                        byte[] bytes = toPrimitives((Byte[])current_list.toArray(new Byte[current_list.size()]));
                        putQueueData(bytes);

                        current_list.clear();
                    }

                    m.put("data", current_list, h2 -> {});
                }else{
                    System.out.println("## ERR : " + res.cause().getMessage() );
                }
            });
        });
    }


    private void putQueueData(byte[] bytes)
    {
        vertx.sharedData().<String, BlockingQueue<byte[]>>getClusterWideMap("hex.real", h -> {
            AsyncMap<String, BlockingQueue<byte[]>> m = h.result();
            m.get("q", res -> {
                if(res.succeeded())
                {
                    BlockingQueue<byte[]> queue = res.result();
                    try {
                        queue.put(bytes);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    m.put("q", queue, h2 -> {});
                }else{
                    System.out.println("## ERR : " + res.cause().getMessage() );
                }
            });
        });
    }


    private void getQueueData()
    {
        vertx.setPeriodic(1000, handler -> {

            vertx.sharedData().<String, BlockingQueue<byte[]>>getClusterWideMap("hex.real", h -> {
                h.result().<String, BlockingQueue<byte[]>>get("q", res -> {
                    if(res.succeeded())
                    {
                        BlockingQueue<byte[]> queue = res.result();
                        byte[] data;
                        while( (data = queue.poll()) != null)
                        {
                            StringBuffer sb = new StringBuffer();
                            for(byte bs : data)
                                sb.append(String.format("%02x ", bs & 0xff));
                            System.out.println("## data : " + sb.toString() );
                        }
                    }else{
                        System.out.println("## ERR : " + res.cause().getMessage() );
                    }
                });
            });

        });
    }

    private byte[] toPrimitives(Byte[] oBytes)
    {

        byte[] bytes = new byte[oBytes.length];
        for(int i = 0; i < oBytes.length; i++){
            bytes[i] = oBytes[i];
        }
        return bytes;
    }

    public void testRegisterUnregister() {
        MessageConsumer reg = vertx.eventBus().<String>consumer("aaa", h -> {
            vertx.sharedData().getClusterWideMap("init", (AsyncResult<AsyncMap<String,Boolean>> result) -> {
                AsyncMap<String, Boolean> asyncMap = result.result();

                asyncMap.get("sem.hex.regi", go -> {
                    if(go.failed() || go.result()==null || go.result())
                    {
                        System.out.println("new!!!");
                    }else{
                        System.out.println("aaaa");
                    }
                });
                asyncMap.put("sem.hex.regi", false, h22 -> {});
            });
        });
//        reg.unregister();

        vertx.setTimer(1000, h -> {
            vertx.eventBus().send("aaa", "test");

            vertx.setTimer(1000, h2 -> {
                vertx.eventBus().send("aaa", "test");
            });
        });
    }

    public void testUnregisterTwice() {

        MessageConsumer reg = vertx.eventBus().<String>consumer("bbb", h -> {
            System.out.println(h.body() + " in !!!!");
        });
        reg.unregister();
        reg.unregister(); // Ok to unregister twice

        vertx.eventBus().send("bbb", "test");
    }


    public static void main(String args[]) {

        VertxOptions options = new VertxOptions().setClustered(true);

        Vertx.clusteredVertx(options, cRslt -> {
            Vertx vertx = cRslt.result();
            vertx.deployVerticle(TestVerticle.class.getName());
        });
    }
}
