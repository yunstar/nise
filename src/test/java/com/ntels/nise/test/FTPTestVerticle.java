package com.ntels.nise.test;

import com.ntels.nise.io.protocol.ftp.FtpClient;
import com.ntels.nise.io.protocol.ftp.FtpFile;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.file.OpenOptions;

import java.util.function.Consumer;

/**
 * Created by youngdong on 2016-07-18.
 */
public class FTPTestVerticle extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();
//        ftpTest();
        ftpDownload();

    }



    private void ftpTest()
    {
        String host = "61.40.220.194";
        String user = "root";
        String pass = "ntels2016$%";
        int port = 22;

        login(host, port, user, pass, client -> {
            client.list(list-> {
                if(list.failed())
                {
                    System.out.println(list.cause());
                    return;
                }

                for (FtpFile f : FtpFile.listing(list.result().toString())) {
                    System.out.println("# " + f.getName());
                }

            });
        });
    }


    private void ftpDownload()
    {
        String host = "61.40.220.146";
        String user = "root";
        String password = "ntels2016$%";
        int port = 22;

        String local_path = "C:\\Users\\youngdong\\Desktop\\ntels-NISE\\tmp\\install.log";
        String server_path = "/install.log";

        login(host, port, user, password, client -> {
            vertx.fileSystem().open(local_path, new OpenOptions().setWrite(true).setTruncateExisting(true), arfile -> {
                if (arfile.succeeded()) {
                    client.retr(server_path, arfile.result(), progress -> {
                    }, retr -> {
                        if (retr.succeeded()) {
                        }
                        arfile.result().close(rs_close -> {rs_close.succeeded();});
                        client.close();
                    });
                }else{
                    client.close();
                }
            });
        });
    }



    private void login(String host, int port, String user, String pass, Consumer<FtpClient> handler)
    {
        FtpClient client = new FtpClient(vertx, host, port);
        client.connect(connect -> {
            if (connect.failed()) {
                return;
            }
            client.login(user, pass, login -> {
                if (login.failed()) {
                    System.out.println("failed connection : " + login.cause().getMessage());
                    return;
                }
                System.out.println("test");
                handler.accept(client);
                handler.andThen(aa -> {
                    aa.quit(rs_quit -> {
                        if(rs_quit.failed())
                            System.out.println( rs_quit.cause() );
                    });
                });
            });
        });
    }
}
