package com.ntels.nise.test;

/**
 * Created by youngdong on 2016-07-13.
 */
public class CutStringTest {

    public static void main(String[] args)
    {
        String body = "12";
        int message_length = body.length();
        int offset = 0;
        int limit_size = 3;
        int page_count = message_length / limit_size;

        if(message_length % limit_size > 0)
            page_count++;


        for (int i=1; i<=page_count; i++) {

            if(message_length - offset < limit_size) {
                limit_size = message_length - offset;
            }

            // 데이터 전송
            System.out.print("## offset:" + offset + ", limit_size:"+ (offset + limit_size)+"  ");
            System.out.println( body.substring(offset, offset + limit_size) );

            offset += limit_size;

        }
    }
}
