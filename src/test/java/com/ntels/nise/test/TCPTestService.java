package com.ntels.nise.test;

import com.ntels.nise.io.protocol.tcp.TCPClient;

import io.vertx.core.AbstractVerticle;

/**
 * Created by youngdong on 2016-07-29.
 */
public class TCPTestService extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        super.start();
        test();
    }

    private void test()
    {
        vertx.setPeriodic(1000*3, handler -> {
            vertx.eventBus().send("yyd.tcp.send", "test data ~~~ ");
        });


        vertx.eventBus().consumer("yyd.tcp.recv", rs_result -> {
            System.out.println("client recv data : " +  rs_result.body());
        });

    }


}
