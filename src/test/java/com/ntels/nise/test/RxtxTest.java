package com.ntels.nise.test;

//import gnu.io.CommPort;
//import gnu.io.CommPortIdentifier;
//import gnu.io.SerialPort;

import com.ntels.nise.common.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Checksum;

/**
 * Created by youngdong on 2017-11-27.
 */
public class RxtxTest {

    public static long calculateChecksum(byte[] buf) {
        int length = buf.length;
        int i = 0;

        long sum = 0;
        long data;

        // Handle all pairs
        while (length > 1) {
            // Corrected to include @Andy's edits and various comments on Stack Overflow
            data = (((buf[i] << 8) & 0xFF00) | ((buf[i + 1]) & 0xFF));
            sum += data;
            // 1's complement carry bit correction in 16-bits (detecting sign extension)
            if ((sum & 0xFFFF0000) > 0) {
                sum = sum & 0xFFFF;
                sum += 1;
            }

            i += 2;
            length -= 2;
        }

        // Handle remaining byte in odd length buffers
        if (length > 0) {
            // Corrected to include @Andy's edits and various comments on Stack Overflow
            sum += (buf[i] << 8 & 0xFF00);
            // 1's complement carry bit correction in 16-bits (detecting sign extension)
            if ((sum & 0xFFFF0000) > 0) {
                sum = sum & 0xFFFF;
                sum += 1;
            }
        }

        // Final 1's complement value correction to 16-bits
        sum = ~sum;
        sum = sum & 0xFFFF;
        return sum;

    }


    public static void main(String[] arg)
    {
        try
        {
//            System.loadLibrary("rxtxSerial");

//            (new RxtxTest()).connect("COM5"); //입력한 포트로 연결

            String inverter_id = "01";
            byte[] b = inverter_id.getBytes();


            byte[] bb = {0x05, 0x30, 0x31, 0x52, 0x30, 0x31, 0x65, 0x30, 0x30, 0x33, 0x00, 0x00, 0x00, 0x00, 0x04};

            int size = bb.length;
            int check_sum = 0;
            for(int i=1;i<size;i++)
            {
                if(bb[i] == 0x04)
                    break;
                check_sum += Integer.parseInt(String.format("%02x",bb[i]), 16);
            }
            String hex_Str = Long.toHexString(check_sum);
            if(hex_Str.length() % 2 != 0)
                hex_Str = "0" + hex_Str;



            byte[] result_b = hex_Str.getBytes();
            bb[10] = result_b[0];
            bb[11] = result_b[1];
            bb[12] = result_b[2];
            bb[13] = result_b[3];

            System.out.println(String.format("%02x ", bb[10] & 0xff));
            System.out.println(String.format("%02x ", bb[11] & 0xff));
            System.out.println(String.format("%02x ", bb[12] & 0xff));
            System.out.println(String.format("%02x ", bb[13] & 0xff));


            String hexstr = "06 30 31 52 30 30 32 30 30 31 36 33 30 30 32 30 30 33 30 31 04";

            byte[] bbb = Utils.hexStringToByteArray(hexstr.replaceAll(" ",""));

            for(byte bs : bbb)
            {
                System.out.print(String.format("%02x ", bs & 0xff));
            }

            System.out.println( System.currentTimeMillis());


        }
        catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

//
//
//    public void connect(String portName) throws Exception
//    {
//        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
//        if ( portIdentifier.isCurrentlyOwned() )
//        {
//            System.out.println("Error: Port is currently in use");
//        }
//        else
//        {
//            //클래스 이름을 식별자로 사용하여 포트 오픈
//            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
//
//            if ( commPort instanceof SerialPort)
//            {
//                //포트 설정(통신속도 설정. 기본 9600으로 사용)
//                SerialPort serialPort = (SerialPort) commPort;
//                serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
//
//                //Input,OutputStream 버퍼 생성 후 오픈
//                InputStream in = serialPort.getInputStream();
//                OutputStream out = serialPort.getOutputStream();
//
//                //읽기, 쓰기 쓰레드 작동
//                (new Thread(new SerialReader(in))).start();
//                (new Thread(new SerialWriter(out))).start();
//
//            }
//            else
//            {
//                System.out.println("Error: Only serial ports are handled by this example.");
//            }
//        }
//    }
//
//
//
//    /** */
//    //데이터 수신
//    public static class SerialReader implements Runnable
//    {
//        InputStream in;
//
//        public SerialReader ( InputStream in )
//        {
//            this.in = in;
//        }
//
//        public void run ()
//        {
//            byte[] buffer = new byte[1024];
//            int len = -1;
//            try
//            {
//                while ( ( len = this.in.read(buffer)) > -1 )
//                {
//                    System.out.print("## data : " + new String(buffer,0,len));
//                }
//            }
//            catch ( IOException e )
//            {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /** */
//    //데이터 송신
//    public static class SerialWriter implements Runnable
//    {
//        OutputStream out;
//
//        public SerialWriter ( OutputStream out )
//        {
//            this.out = out;
//        }
//
//        public void run ()
//        {
//            try
//            {
//                int c = 0;
//                while ( ( c = System.in.read()) > -1 )
//                {
//                    this.out.write(c);
//                }
//            }
//            catch ( IOException e )
//            {
//                e.printStackTrace();
//            }
//        }
//    }
}
