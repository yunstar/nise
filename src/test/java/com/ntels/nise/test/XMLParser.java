package com.ntels.nise.test;

import static org.joox.JOOX.$;

import java.io.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import com.ntels.nise.common.def.ErrorCode;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.joox.Each;
import org.xml.sax.SAXException;

/**
 * Created by youngdong on 2016-10-11.
 */
public class XMLParser {


    // the input file location
    private static final String fileLocation = "D:\\업무\\작업\\ISBCP\\201609_Canada_QRC_PoC\\kepware\\설정파일\\notebook.xml";

    public static void main(String[] args) throws Exception
    {
        opcXMLParser();

    }



    public static Each searchSAX(String prefix)
    {
        return ctx -> {
            String name = $(ctx).child("Name").text();

            $(ctx)
                    .children("TagGroupList")
                    .children("TagGroup")
                    .each(ctx_tag_g -> {
                        $(ctx_tag_g)
                                .each(searchSAX(prefix + "." + name));
                    });

            $(ctx)
                    .children("TagList")
                    .children("Tag")
                    .each(ctx_tag -> {
                        String tag_name = $(ctx_tag).child("Name").text();

                        System.out.println(prefix + "." + name + "." + tag_name);
                    });
        };
    }


    public static void opcXMLParser() throws IOException, SAXException {
        $(new File(fileLocation))
                .children("ChannelList")
                .children("Channel")
                .each(ctx -> {
                    String channel_name = $(ctx).child("Name").text();
                    $(ctx)
                            .children("DeviceList")
                            .children("Device")
                            .each(searchSAX(channel_name));
                });
    }


    public static void oldParser() throws Exception
    {
//
        FileInputStream fileInputStream = new FileInputStream(fileLocation);
        XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(fileInputStream);

        while (xmlStreamReader.hasNext()) {

            int eventCode = xmlStreamReader.next();

            switch(eventCode) {
                case XMLEvent.CDATA:
                case XMLEvent.SPACE:
                case XMLEvent.CHARACTERS:
                    System.out.println(xmlStreamReader.getText());
                    break;
                case XMLEvent.END_ELEMENT:
                    System.out.println(xmlStreamReader.getLocalName());
                    break;
                case XMLEvent.START_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("Channel"))
                    {

                    }
                    break;
            }
        }
    }
}
