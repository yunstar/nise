package com.ntels.nise.test;

import org.junit.Assert;
import org.junit.Test;

/**,
 * Created by youngdong on 2016-07-12.
 */
public class Base64Test {


    @Test
    public void encodeTest()
    {
        Assert.assertEquals("YWJjZDI0dDU2QCEjJSReJiomXihfKSgqJl4lJFIjRUBERkdITUpuZ2ZkYmduaHltdGZydmRmYm5obXU="
                , java.util.Base64.getEncoder().encodeToString("abcd24t56@!#%$^&*&^(_)(*&^%$R#E@DFGHMJngfdbgnhymtfrvdfbnhmu".getBytes() ));
    }

    @Test
    public void decodeTest()
    {
        Assert.assertEquals("abcd24t56@!#%$^&*&^(_)(*&^%$R#E@DFGHMJngfdbgnhymtfrvdfbnhmu"
                , new String( java.util.Base64.getDecoder().decode("YWJjZDI0dDU2QCEjJSReJiomXihfKSgqJl4lJFIjRUBERkdITUpuZ2ZkYmduaHltdGZydmRmYm5obXU=") ));
    }

    @Test
    public void performanceTest()
    {
        for(int i=0;i<1000;i++)
            java.util.Base64.getEncoder().encodeToString("abcd24t56@!#%$^&*&^(_)(*&^%$R#E@DFGHMJngfdbgnhymtfrvdfbnhmu".getBytes() );
    }


    public static void main(String[] args){
    }
}
