/*
 * Copyright 2016 Kevin Herron
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ntels.nise.test;

import static com.digitalpetri.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;
import static com.google.common.collect.Lists.newArrayList;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.digitalpetri.opcua.sdk.client.OpcUaClient;
import com.digitalpetri.opcua.sdk.client.api.config.OpcUaClientConfig;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaSubscription;
import com.digitalpetri.opcua.stack.client.UaTcpStackClient;
import com.digitalpetri.opcua.stack.core.AttributeId;
import com.digitalpetri.opcua.stack.core.security.SecurityPolicy;
import com.digitalpetri.opcua.stack.core.types.builtin.LocalizedText;
import com.digitalpetri.opcua.stack.core.types.builtin.NodeId;
import com.digitalpetri.opcua.stack.core.types.builtin.QualifiedName;
import com.digitalpetri.opcua.stack.core.types.builtin.unsigned.UInteger;
import com.digitalpetri.opcua.stack.core.types.enumerated.MonitoringMode;
import com.digitalpetri.opcua.stack.core.types.enumerated.TimestampsToReturn;
import com.digitalpetri.opcua.stack.core.types.structured.EndpointDescription;
import com.digitalpetri.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
import com.digitalpetri.opcua.stack.core.types.structured.MonitoringParameters;
import com.digitalpetri.opcua.stack.core.types.structured.ReadValueId;

public class OpcUaClientExcelRolling implements Runnable{

    private static OpcUaClient client;
    private static OpcUaClientConfig clientConfig;
    private SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Override
    public void run() {

        try {

            connectOPCUA();

            // subscription tags
            String[] tags = {"Channel1.Device1.test1", "Channel1.Device1.test1"};

            for(String t : tags)
                testSubscribe(t);

            Thread.sleep(10000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void connectOPCUA() {

        try {
            EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints("opc.tcp://127.0.0.1:49320").get();

            EndpointDescription endpoint = Arrays.stream(endpoints)
                    .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
                    .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));

            clientConfig = OpcUaClientConfig.builder()
                    .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
                    .setApplicationUri("urn:digitalpetri:opcua:client")
                    .setEndpoint(endpoint)
                    .setRequestTimeout(uint(5000))
                    .setSessionTimeout(uint(3000))
                    .build();

            client = new OpcUaClient(clientConfig);

//            client.connect().get();

        }catch (Exception e){
            e.printStackTrace();
        }
    }




    public void testSubscribe(String tag) throws Exception {

        // create a subscription and a monitored item
        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0, UInteger.valueOf(100),UInteger.valueOf(100),UInteger.valueOf(100),true, null).get();

        NodeId nodeId = new NodeId(2, tag);
//        NodeId nodeId =  Identifiers.Server_NamespaceArray;
        ReadValueId readValueId = new ReadValueId( nodeId, AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);


        MonitoringParameters parameters = new MonitoringParameters(
                uint(1),    // client handle
                300.0,     // sampling interval
                null,       // no (default) filter
                uint(20),   // queue size
                false);      // discard oldest

        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(readValueId, MonitoringMode.Reporting, parameters);

        List<UaMonitoredItem> items = subscription
                .createMonitoredItems(TimestampsToReturn.Server, newArrayList(request)).get();

        for (UaMonitoredItem item : items) {
            // do something with the value updates
            item.setValueConsumer( handler -> {

                System.out.println("--------------------------------------------------------------");
                System.out.println("data : " + handler.toString());
                System.out.println("Time : " + sf.format(handler.getServerTime().getJavaDate()));
                System.out.println("tag : " + item.getReadValueId().getNodeId().getIdentifier());
                System.out.println("value : " + handler.getValue().getValue());
            });
        }
    }





    public static void main(String[] args) throws Exception
    {
        Thread t = new Thread(new OpcUaClientExcelRolling(), "sample");
        t.start();


    }
}
