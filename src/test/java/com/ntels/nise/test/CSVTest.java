package com.ntels.nise.test;

import com.ntels.nise.common.query.Query;
import io.vertx.core.json.JsonArray;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by youngdong on 2016-07-14.
 */
public class CSVTest {

    public static void main(String[] args)
    {
//        readCSV(Paths.get("C:\\Users\\youngdong\\Desktop\\node_info.csv"));
    }

    private static int i=0;

    public static void readCSV(Path path)
    {

        try {
            System.out.println(Files.isReadable(path));

            Stream<String> lines = Files.lines(path);

            List<List<String>> values = lines
                    .skip(1)
                    .map(line -> Arrays.asList(line.split(",")))
//                    .filter(list -> list.get(0).equals("Neda")) // keep only items where the name is "Neda"
                    .collect(Collectors.toList());

            JsonArray result = new JsonArray(values);

            System.out.print(result);

//            values.forEach(list -> {
//
//                System.out.print("\n" + i++ + "   ");
//
//                list.forEach(item -> {
//                    System.out.print(item + " ## ");
//                });
//            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
