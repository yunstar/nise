package com.ntels.nise.io;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by youngdong on 2016-07-11.
 */
public class MqttSimpleClient implements MqttCallback {

//    private String url = "tcp://61.40.220.194:3002";
//    private String user = "D42616873175";
//    private String passwd = "0a1838c8aa2a4518";
//    private String client_id = "D42616873175";

    private String url = "tcp://61.40.220.153:1883";
    private String user = "test";
    private String passwd = "test";
    private String client_id = "123123123";


    public static void main(String[] args)
    {
        MqttSimpleClient simple_client = new MqttSimpleClient();
        simple_client.test();
    }


    public void test()
    {
        String tmpDir = System.getProperty("java.io.tmpdir");
        MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir);
        MqttClient client = null;

        try {

            client = new MqttClient(url, client_id, new MemoryPersistence());

            MqttConnectOptions options = new MqttConnectOptions();
//            options.setCleanSession(true);

            options.setPassword(passwd.toCharArray());
            options.setUserName(user);
            options.setConnectionTimeout(10000);

            if(url.startsWith("ssl"))
                options.setSocketFactory(trustAllHosts().getSocketFactory());

//            client.setCallback(this);

            client.connect(options);

            Long starttim, endtim;

            int count = 500;

            byte[] aaa = "[{\"id\": \"Channel1.Device1aaaaaaaaaaaaaaaaaaaaaaabbbbbadfadfadfadfgadgabbbbbbbbbbbbbbbbccccccccccccccccccc.Tag1\",\"v\": 42}]".getBytes();

            MqttMessage message = new MqttMessage(aaa);
            message.setQos(1);

            System.out.println(aaa.length);

            starttim = System.nanoTime();
            for(int i=0;i<count;i++)
//                client.publish("/aaaa", aaa, 1, false);
            client.publish("/aaaa", message);

            endtim = System.nanoTime();
            System.out.println( (endtim - starttim) / 1000000000.0 + " sec"  );

            System.out.println( (float)count / ((endtim - starttim) / 1000000000.0) + " EPS"  );


            System.out.println("isConnected : " + client.isConnected());

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try {
                client.close();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }


    private static SSLContext trustAllHosts() {
        SSLContext sc = null;
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
                // TODO Auto-generated method stub

            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
                // TODO Auto-generated method stub

            }
        }};

        try {
            sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            return sc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sc;
    }
}
