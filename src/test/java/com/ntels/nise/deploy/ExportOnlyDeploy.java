package com.ntels.nise.deploy;


import com.ntels.nise.component.common.ParserComponent;
import com.ntels.nise.component.common.WriteComponent;
import com.ntels.nise.component.device.control.DeviceControlComponent;
import com.ntels.nise.component.device.control.NodeControlComponent;
import com.ntels.nise.component.device.data.CollectComponentAsync;
import com.ntels.nise.component.device.install.ApplicationInstallComponent;
import com.ntels.nise.component.device.install.FirmwareInstallComponent;
import com.ntels.nise.component.device.regist.RegisterComponent;
import com.ntels.nise.component.repository.CacheService;
import com.ntels.nise.component.repository.JDBCService;
import com.ntels.nise.io.router.*;
import com.ntels.nise.service.collect.NodeDataCollectService;
import com.ntels.nise.service.control.DeviceControlService;
import com.ntels.nise.service.control.DeviceRetrieveService;
import com.ntels.nise.service.control.NodeControlService;
import com.ntels.nise.service.export.ExcelRollingService;
import com.ntels.nise.service.install.ApplicationInstallService;
import com.ntels.nise.service.install.FirmwareInstallService;
import com.ntels.nise.service.regist.NodeRegisterService;

import io.vertx.core.*;

public class ExportOnlyDeploy extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        vertx.executeBlocking(future -> {
            vertx.fileSystem().readFile("nise-config.json", file -> {
                if (file.succeeded()) {

                    DeploymentOptions workerOptions = new DeploymentOptions().setHa(false).setWorker(true).setConfig(file.result().toJsonObject());
                    DeploymentOptions options = new DeploymentOptions().setInstances(1).setHa(false).setConfig(file.result().toJsonObject());

//                    vertx.deployVerticle(MqttRouterIn.class.getName(), workerOptions);

//                    vertx.deployVerticle(CacheService.class.getName(), workerOptions);
//                    vertx.deployVerticle(SimpleVerticle.class.getName(), workerOptions);



                    // JDBC Test
//                    vertx.deployVerticle(JDBCService.class.getName(), workerOptions, rs-> {
//                        if( rs.succeeded() )
//                        {
//                            vertx.deployVerticle(JDBCTestVerticle.class.getName(), workerOptions);
//                        }
//                    });


                    // Component
                    vertx.deployVerticle(CacheService.class.getName(), workerOptions);
                    vertx.deployVerticle(ParserComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(WriteComponent.class.getName(), workerOptions);


                    // Service
                    vertx.deployVerticle(ExcelRollingService.class.getName(), workerOptions);


                    // Router
                    vertx.deployVerticle(OPCUARouter.class.getName(), options);

                } else {
                    future.fail(file.cause());
                }
            });
        }, result -> {
            if (result.succeeded()) {
                System.out.println("DeployTest Success!!!");
                startFuture.complete();
            }
        });
    }

    public static void main(String args[]) {
        VertxOptions options = new VertxOptions().setClustered(false);
        Vertx.clusteredVertx(options, cRslt -> {
            Vertx vertx = cRslt.result();
            vertx.deployVerticle(ExportOnlyDeploy.class.getName());
        });
    }

}
