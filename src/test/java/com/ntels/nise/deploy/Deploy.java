package com.ntels.nise.deploy;


import com.ntels.nise.component.common.ParserComponent;
import com.ntels.nise.component.device.control.DeviceControlComponent;
import com.ntels.nise.component.device.control.NodeControlComponent;
import com.ntels.nise.component.device.data.CollectComponent;
import com.ntels.nise.component.device.data.CollectComponentAsync;
import com.ntels.nise.component.device.install.ApplicationInstallComponent;
import com.ntels.nise.component.device.install.FirmwareInstallComponent;
import com.ntels.nise.component.device.regist.ReceiveComponent;
import com.ntels.nise.component.device.regist.RegisterComponent;
import com.ntels.nise.component.repository.CacheService;
import com.ntels.nise.component.repository.JDBCService;
import com.ntels.nise.component.sys.sem.hexpower.HexpowerComponent;
import com.ntels.nise.io.router.*;
import com.ntels.nise.service.collect.ACSDataCollectService;
import com.ntels.nise.service.collect.AMRDataCollectService;
import com.ntels.nise.service.collect.sem.HexpowerDataCollectService;
import com.ntels.nise.service.control.DeviceControlService;
import com.ntels.nise.service.control.DeviceRetrieveService;
import com.ntels.nise.service.collect.NodeDataCollectService;
import com.ntels.nise.service.control.NodeControlService;
import com.ntels.nise.service.install.ApplicationInstallService;
import com.ntels.nise.service.install.FirmwareInstallService;
import com.ntels.nise.service.regist.NodeRegisterService;
import io.vertx.core.*;

public class Deploy extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        vertx.executeBlocking(future -> {
            vertx.fileSystem().readFile("nise-config.json", file -> {
                if (file.succeeded()) {

                    DeploymentOptions workerOptions = new DeploymentOptions().setHa(false).setWorker(true).setConfig(file.result().toJsonObject());
                    DeploymentOptions options = new DeploymentOptions().setInstances(1).setHa(false).setConfig(file.result().toJsonObject());

//                    vertx.deployVerticle(MqttRouterIn.class.getName(), workerOptions);

//                    vertx.deployVerticle(CacheService.class.getName(), workerOptions);
//                    vertx.deployVerticle(SimpleVerticle.class.getName(), workerOptions);



                    // JDBC Test
//                    vertx.deployVerticle(JDBCService.class.getName(), workerOptions, rs-> {
//                        if( rs.succeeded() )
//                        {
//                            vertx.deployVerticle(JDBCTestVerticle.class.getName(), workerOptions);
//                        }
//                    });


                    // Component
                    vertx.deployVerticle(CacheService.class.getName(), workerOptions);
                    vertx.deployVerticle(JDBCService.class.getName(), workerOptions);
                    vertx.deployVerticle(ParserComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(RegisterComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(ReceiveComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(NodeControlComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(DeviceControlComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(ApplicationInstallComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(FirmwareInstallComponent.class.getName(), workerOptions);
                    vertx.deployVerticle(CollectComponentAsync.class.getName(), workerOptions);
//                    vertx.deployVerticle(HexpowerComponent.class.getName(), workerOptions);


                    // Service
                    vertx.deployVerticle(NodeRegisterService.class.getName(), options);
                    vertx.deployVerticle(NodeDataCollectService.class.getName(), options);
                    vertx.deployVerticle(DeviceRetrieveService.class.getName(), options);
//                    vertx.deployVerticle(ACSDataCollectService.class.getName(), options);
//                    vertx.deployVerticle(AMRDataCollectService.class.getName(), options);
                    vertx.deployVerticle(NodeControlService.class.getName(), options);
                    vertx.deployVerticle(DeviceControlService.class.getName(), options);
                    vertx.deployVerticle(ApplicationInstallService.class.getName(), workerOptions);
                    vertx.deployVerticle(FirmwareInstallService.class.getName(), workerOptions);
//                    vertx.deployVerticle(HexpowerDataCollectService.class.getName(), workerOptions);


                    // Router
                    vertx.deployVerticle(MqttMASOutRouter.class.getName(), options);
                    vertx.deployVerticle(MqttMASInRouter.class.getName(), options);
                    vertx.deployVerticle(FTPRouter.class.getName(), workerOptions);
//                    vertx.deployVerticle(TCPACSServerRouter.class.getName(), options);
//                    vertx.deployVerticle(TCPAMRServerRouter.class.getName(), options);
                    vertx.deployVerticle(OPCUARouter.class.getName(), options);
                    vertx.deployVerticle(RESTIEQInRouter.class.getName(), options);
//                    vertx.deployVerticle(SerialHexpowerRouter.class.getName(), options);

                } else {
                    future.fail(file.cause());
                }
            });
        }, result -> {
            if (result.succeeded()) {
                System.out.println("DeployTest Success!!!");
                startFuture.complete();
            }
        });
    }

    public static void main(String args[]) {
        VertxOptions options = new VertxOptions().setClustered(false);
        Vertx.clusteredVertx(options, cRslt -> {
            Vertx vertx = cRslt.result();
            vertx.deployVerticle(Deploy.class.getName());
        });
    }

}
