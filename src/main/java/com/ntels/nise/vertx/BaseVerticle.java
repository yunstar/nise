package com.ntels.nise.vertx;

import io.vertx.core.AbstractVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sgkim on 2016-06-03.
 */
public abstract class BaseVerticle extends AbstractVerticle   {

    public final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void start() throws Exception{
        this.onInit();
        super.start();
        this.onReady();
    }

    @Override
    public void stop() throws Exception{
    }

    /**
     * Component Initialize
     */
    protected void onInit(){
        logger.debug( "BaseVerticle.OnInit()" );
    }

    /**
     * Component Running
     */
    protected void onReady(){
    }

    /**
     * Component Stop
     */
    protected void onShutdown(){
        try{
            this.stop();
        }
        catch(Exception e){

        }
    }
}
