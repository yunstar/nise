package com.ntels.nise.vertx;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sgkim on 2016-06-15.
 */
public class EventBus {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    DeliveryOptions options;
    Vertx  vertx;

    public EventBus(Vertx vertx) {
        options = new DeliveryOptions();
        options.addHeader( "result", " " );
        this.vertx = vertx;
    }

    /**
     * Register Event Bus Consumer
     * @param address : address
     * @param handler : event bus handler
     */
    public void registConsumer(String address, Handler<Message<JsonObject>> handler) {
        vertx.eventBus().consumer(  address ,  handler );
    }

    public void request(String address, Message<JsonObject> message, Handler<AsyncResult<Message<JsonObject>>> handler )
    {
        JsonObject object = message.body();
        options.setHeaders( message.headers() );
        //options.addHeader( "command_class", "request" );
        vertx.eventBus().send( address, object , options , handler );
        //address : mobile.pub, mas.pub, ...

    }

    public void request(String address, JsonObject object,Handler<AsyncResult<Message<JsonObject>>> handler )
    {
        //options.addHeader( "command_class", "request" );
        vertx.eventBus().send( address, object , options , handler );
        //address : mobile.pub, mas.pub, ...

    }

    /**
     * in-bounding send
     * @param address : address
     * @param message : header + body(Json)
     */
    public void send(String address, Message<JsonObject> message )
    {
        options.setHeaders( message.headers() );
        //options.addHeader("command_class", "publish" );
        vertx.eventBus().send( address, message.body() , options );
    }

    /**
     * out-bounding send
     * @param address
     * @param object
     */
    public void send(String address, JsonObject object )
    {
        //ToDo. 헤더 추가로 인해 메모리 증가
        //options.addHeader("command_class", "publish" );
        logger.debug( options.getHeaders().toString() );
        vertx.eventBus().send( address, object , options );
    }

    /**
     * Broadcast Send
     * @param address : address
     * @param object : json object
     */
    public void publish(String address, JsonObject object, Message<JsonObject> message)
    {
        options.setHeaders( message.headers() );
        //options.addHeader("command_class", "broadcast" );
        vertx.eventBus().publish( address, object , options );
    }

//    //eventBus.replySuccess( snd, message );
//    public void replySuccess(JsonObject object, Message<JsonObject> message){
//        options.setHeaders( message.headers() );
//        options.addHeader("result", "success");
//        message.reply(object, options);
//    }
//
//    //eventBus.replyFail( "Device Meta Info get Failed", message );
//    public void replyFail(String msg, Message<JsonObject> message){
//        options.setHeaders( message.headers() );
//        options.addHeader("result", "fail" );
//        options.addHeader("result_msg", msg );
//        message.reply(message.body(), options);
//    }


}
