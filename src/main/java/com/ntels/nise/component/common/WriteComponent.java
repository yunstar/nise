package com.ntels.nise.component.common;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by youngdong on 2018-03-15.
 */
public class WriteComponent extends BaseVerticle {

    @Override
    public void start() throws Exception {
        super.start();

        vertx.eventBus().consumer(EventBusAddress.FILE_APPEND.toString(), onWriteCSVData());
    }


    private Handler<Message<JsonObject>> onWriteCSVData() {
        return result -> {
            JsonObject data = result.body();

            appendFile(data);
        };
    }


    private void appendFile(JsonObject data)
    {
        try {
            Files.write(Paths.get(data.getString("file")), data.getString("data").getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

//        vertx.fileSystem().open(data.getString("file"), new OpenOptions().setWrite(true), ar -> {
//            if (ar.succeeded()) {
//                AsyncFile ws = ar.result();
//
//                try{
//                    ws.write( Buffer.buffer(data.getString("data")) );
//                }catch(Exception e){
//                    logger.error(e.getMessage());
//                }finally {
//                    ws.close();
//                }
//            } else {
//                System.err.println("Could not open file");
//            }
//        });
    }
}
