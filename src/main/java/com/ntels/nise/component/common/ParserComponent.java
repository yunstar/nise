package com.ntels.nise.component.common;

import java.io.*;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import jdk.nashorn.internal.parser.JSONParser;
import org.joox.Each;
import org.xml.sax.SAXException;

import static org.joox.JOOX.$;

/**
 * Created by youngdong on 2016-07-14.
 */
public class ParserComponent extends BaseVerticle {

    private JsonArray node_list = new JsonArray();
    private final String[] KEY = {"channels","devices","tag_groups", "tags"};

    @Override
    protected void onInit() {
        super.onInit();
        vertx.eventBus().consumer(EventBusAddress.SIG_READ_CSV.toString(), onReadCSVData());

        vertx.eventBus().consumer(EventBusAddress.SIG_READ_JSON.toString(), onIReadJsonData());
    }


    private Handler<Message<String>> onReadCSVData()
    {
        return result -> {

            try {

                String csv_path = result.body();
                logger.info("-----------------------------------------------------------------");
                logger.info("node info csv_path : " + csv_path);
                logger.info("-----------------------------------------------------------------");

                opcXMLParser(csv_path);

                logger.info("node info size : " + node_list.size());

                result.reply(node_list);

                node_list.clear();

            }catch(Exception e){
                logger.error("NISE-000004 "+result.body()+" (csv) failed parsing : " + e.getMessage());
                result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
            }
        } ;
    }


    private Handler<Message<String>> onIReadJsonData()
    {
        return result -> {

            try {

                String json_path = result.body();
                logger.info("-----------------------------------------------------------------");
                logger.info("node info json_path : " + json_path);
                logger.info("-----------------------------------------------------------------");

                vertx.fileSystem().readFile(json_path, load -> {
                    if (load.succeeded()) {
                        Buffer buff = load.result();
                        String f = buff.toString();
                        if(f.startsWith("\uFEFF"))
                            f = f.substring(1);

                        JsonObject nodesJson = new JsonObject(f);
                        addNodes("", nodesJson.getJsonObject("Project"));

                        logger.info("node info size : " + node_list.size());

                        result.reply(node_list);

                        node_list.clear();

                    } else {
                        logger.error("NISE-000004 (json) failed loading : " + load.cause().getMessage());
                        result.fail(ErrorCode.SERVER_ERROR, load.cause().getMessage());
                    }
                });

            }catch(Exception e){
                logger.error("NISE-000004 "+result.body()+" (json) failed parsing : " + e.getMessage());
                result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
            }
        } ;
    }

    private Each searchSAX(String prefix)
    {
        return ctx -> {
            String name = $(ctx).child("Name").text();

            $(ctx)
                    .children("TagGroupList")
                    .children("TagGroup")
                    .each(ctx_tag_g -> {
                        $(ctx_tag_g)
                                .each(searchSAX(prefix + "." + name));
                    });

            $(ctx)
                    .children("TagList")
                    .children("Tag")
                    .each(ctx_tag -> {
                        String tag_name = $(ctx_tag).child("Name").text();

//                        System.out.println(prefix + "." + name + "." + tag_name);
                        node_list.add(prefix + "." + name + "." + tag_name);
                    });
        };
    }


    private void opcXMLParser(String file_path) throws IOException, SAXException {
        $(new File(file_path))
                .children("ChannelList")
                .children("Channel")
                .each(ctx -> {
                    String channel_name = $(ctx).child("Name").text();
                    $(ctx)
                            .children("DeviceList")
                            .children("Device")
                            .each(searchSAX(channel_name));
                });
    }


    private void addNodes(String prefix, JsonObject g)
    {
        for(String k : KEY)
            if(g.containsKey(k))
            {
                JsonArray g2 = g.getJsonArray(k);
                int size = g2.size();
                switch (k)
                {
                    case "tags":
                        for(int i=0;i<size;i++){
                            node_list.add(prefix +"." + g.getString("common.ALLTYPES_NAME") + "." + g2.getJsonObject(i).getString("common.ALLTYPES_NAME"));
//                            System.out.println("tags : " + prefix +"."+ g2.getJsonObject(i).getString("common.ALLTYPES_NAME"));
                        }
                        break;
                    default:
                        for(int i=0;i<size;i++){
                            String name = g.getString("common.ALLTYPES_NAME");
                            if(name==null)
                                name = "";
//                            System.out.println(String.format("[%s][%s][%s][%s]", k, prefix, g.getString("common.ALLTYPES_NAME"), g2.getJsonObject(i).getString("common.ALLTYPES_NAME")));
                            addNodes((prefix.length()>0?prefix +".":"")+ name, g2.getJsonObject(i));
                        }

                }
            }
    }

}
