package com.ntels.nise.component.device.control;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-21.
 */
public class NodeControlComponent extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();
        // Json format 으로 수정 적용
        vertx.eventBus().consumer(EventBusAddress.COMP_FORMAT_NODE_EXEC_CMD.toString(), onConvertExecFormat());
        vertx.eventBus().consumer(EventBusAddress.COMP_FORMAT_NODE_EXEC_RES.toString(), onConvertResponseFormat());

    }


    private Handler<Message<JsonObject>> onConvertExecFormat()
    {
        return result -> {

            try{
                JsonObject control_format = result.body();

                JsonObject query = new JsonObject();
                query.put("map", CacheAddress.NODEID_TAG.toString());
                query.put("key", control_format.getString("node_id"));
                vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, rs_tag -> {
                    if(rs_tag.succeeded())
                    {
                        String aommand_type = control_format.getString("command_type");

                        switch (aommand_type)
                        {
                            case "opc.control":

//                                String control_msg = new String(java.util.Base64.getDecoder().decode(control_format.getString("request_value")));
//
//                                JsonObject cmd_json = new JsonObject();
//                                cmd_json.put("id", (String)rs_tag.result().body());
//                                cmd_json.put("v", control_msg);

                                JsonObject cmd_json = new JsonObject( new String(java.util.Base64.getDecoder().decode(control_format.getString("request_value"))) );
                                logger.info("### Control data : " + cmd_json.toString());
                                result.reply(cmd_json);
                                break;
                            default:
                                logger.error("NISE-000039 undefined command_type : " + aommand_type);
                                break;
                        }
                    }else
                        logger.error("NISE-000040 node not found : " + control_format.getString("node_id"));
                });

            }catch(Exception e){
                result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
            }

        };
    }




    private Handler<Message<JsonObject>> onConvertResponseFormat()
    {
        return result -> {

            try{
                JsonObject control_format = result.body();
                JsonObject response_format = new JsonObject();
                response_format.put("message_type", control_format.getString("message_type"));
                response_format.put("device_id", "");
                response_format.put("device_key", "");
                response_format.put("node_id", control_format.getString("node_id"));
                response_format.put("command_id", control_format.getString("command_id"));
                response_format.put("command_type", control_format.getString("command_type"));
//                response_format.put("response_value", "");

                result.reply(response_format);

            }catch(Exception e){
                result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
            }

        };
    }
}
