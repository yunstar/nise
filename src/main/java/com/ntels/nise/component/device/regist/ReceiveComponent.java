package com.ntels.nise.component.device.regist;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.query.Query;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-20.
 */
public class ReceiveComponent extends BaseVerticle {


    @Override
    protected void onInit() {
        super.onInit();

        // Node 정보 전체 삭제 (Cache & Database)
        vertx.eventBus().consumer(EventBusAddress.COMP_NODE_DATA_CLEAR.toString(), onClearNode());

        // Node 정보 Cache에 등록
        vertx.eventBus().consumer(EventBusAddress.COMP_NODE_CACHE_INSERT.toString(), onAddCache());

        // Node 정보 DB에 등록
        vertx.eventBus().consumer(EventBusAddress.COMP_NODE_DB_INSERT.toString(), onDBInsertNodeData());

        // Node정보와 Tag정보 맵핑 (제어명령시 사용, 왜냐하면 MAS에서는 Node_id를 기준으로 하기 때문에 tag에 해당하는 node_id가 필요)
        vertx.eventBus().consumer(EventBusAddress.COMP_NODE_MAPPING_TAG.toString(), onMappingTag());

        // Gateway Heartbeat
        vertx.eventBus().consumer(EventBusAddress.COMP_GW_PING.toString(), onSendPing());
    }




    /**
     *
     * @return
     */
    private Handler<Message<JsonArray>> onSendPing()
    {
        return result -> {
            JsonObject reply_data = new JsonObject();
            reply_data.put("message_type", "device.content");
            reply_data.put("device_id", "");
            reply_data.put("device_key", "");
            reply_data.put("content_type", "ping");
            reply_data.put("content_value", Base64.getEncoder().encodeToString("1".getBytes()));

            vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), reply_data, rs -> {
                if(rs.failed())
                    logger.error("send data Error!! ");
            });
        };
    }


    private Handler<Message<JsonArray>> onClearNode() {
        return result -> {
            try {

                // 1. NODEID_TAG Cache clear
                JsonObject clear_nodeid_tag = new JsonObject();
                clear_nodeid_tag.put("map", CacheAddress.NODEID_TAG.toString());
                vertx.eventBus().send(EventBusAddress.CACHE_REMOVE.toString(), clear_nodeid_tag, handler -> {
                    if (handler.succeeded()) {



                        // 2. TAG_NODEID Cache clear
                        JsonObject clear_tag_nodeid = new JsonObject();
                        clear_tag_nodeid.put("map", CacheAddress.TAG_NODEID.toString());
                        vertx.eventBus().send(EventBusAddress.CACHE_REMOVE.toString(), clear_tag_nodeid, res_tag -> {
                            if (res_tag.succeeded()) {


                                // 3. Database clear
                                vertx.eventBus().send(EventBusAddress.SQL_EXEC.toString(), new JsonObject().put("sql",Query.TRUNCATE_NODE_INFO.toString()), rs_truncate ->
                                {
                                    if (rs_truncate.succeeded()) {
                                        result.reply(200);
                                    } else {
                                        result.fail(500, rs_truncate.cause().getMessage());
                                    }
                                });



                            } else {
                                logger.error("NISE-000043 failed to clear cache : " + res_tag.cause());
                                result.fail(500, res_tag.cause().getMessage());
                            }
                        });


                    }else{
                        logger.error("NISE-000043 failed to clear cache : " + handler.cause());
                        result.fail(500, handler.cause().getMessage());
                    }
                });


            } catch (Exception e) {
                result.fail(500, e.getMessage());
            }
        };
    }



    private Handler<Message<JsonArray>> onAddCache()
    {
        return result -> {

            try {

                JsonArray rows = result.body();
                Map<String, String> m = new HashMap<>();

                for (int i = 0; i < rows.size(); i++) {

                    try {
                        JsonObject row = rows.getJsonObject(i);

                        JsonObject put_tag_id = new JsonObject();
                        put_tag_id.put("map", CacheAddress.NODEID_TAG.toString());
                        put_tag_id.put("key", row.getString("node_id"));
                        put_tag_id.put("value", row.getString("tag"));
                        vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), put_tag_id);

                        JsonObject put_id_tag = new JsonObject();
                        put_id_tag.put("map", CacheAddress.TAG_NODEID.toString());
                        put_id_tag.put("key", row.getString("tag"));
                        put_id_tag.put("value", row.getString("node_id"));
                        vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), put_id_tag);

                        // array처리시 사용하기위해 통으로 저장
                        m.put(row.getString("tag"), row.getString("node_id"));
                    }catch (Exception e){
                        logger.error("NISE-000012 failed to save node info : " + e.getMessage());
                    }
                }

                vertx.sharedData().getClusterWideMap(CacheAddress.NODEID_TAG_MAP.toString(), resMap -> {
                    resMap.result().put(CacheAddress.NODEID_TAG_MAP.toString(), m, res -> {
                        if(res.failed())
                            logger.error("NISE-000012 failed to save node info : " + res.cause().getMessage());
                    });
                });

            }catch (Exception e){
                logger.error("NISE-000012 failed to save node info : " + e.getMessage());
            }

        };
    }





    private Handler<Message<JsonArray>> onDBInsertNodeData() {
        return result -> {

            try {
                JsonArray nodes = result.body();

                vertx.setTimer(3000, handler -> {
                    for (int i = 0; i < nodes.size(); i++) {
                        JsonObject node = nodes.getJsonObject(i);
                        JsonObject query = new JsonObject();
                        JsonArray params = new JsonArray();
                        query.put("sql", Query.INSERT_NODE_BATCH.toString());
                        query.put("params", params);

                        params.add(node.getString("tag"))
                                .add(node.getString("node_id"))
                                .add(node.getString("node_mf_id"))
                                .add(node.getString("node_model_id"))
                                .add(node.getString("node_sn"))
                                .add(node.getString("node_id"))
                                .add(node.getString("node_mf_id"))
                                .add(node.getString("node_model_id"))
                                .add(node.getString("node_sn"));

                        // insert bulk node data
                        vertx.eventBus().send(EventBusAddress.SQL_UPDATE.toString(), query, rs_select ->
                        {
                            if (rs_select.failed())
                                logger.error(rs_select.cause().getMessage());
                        });

                    }

//                /* 2017-05-19   batch update 로 변경 */
//                vertx.setTimer(3000, handler -> {
//                    JsonObject query = new JsonObject();
//                    JsonArray list = new JsonArray();
//                    query.put("sql", Query.INSERT_NODE_BATCH.toString());
//                    query.put("params", list);
//
//                    for (int i = 0; i < nodes.size(); i++) {
//                        JsonArray params = new JsonArray();
//                        list.add(params);
//
//                        JsonObject node = nodes.getJsonObject(i);
//                        params.add(node.getString("tag"))
//                                .add(node.getString("node_id"))
//                                .add(node.getString("node_mf_id"))
//                                .add(node.getString("node_model_id"))
//                                .add(node.getString("node_sn"));
//                    }
//
//                    // insert bulk node data
//                    if(nodes.size() > 0)
//                    vertx.eventBus().send(EventBusAddress.SQL_UPDATE_BATCH.toString(), query, rs_select ->
//                    {
//                        if (rs_select.failed())
//                            logger.error(rs_select.cause().getMessage());
//                    });

                });
            }catch (Exception e){
                logger.error(e.getMessage());
            }
        };
    }


    /**
     * Tag와 Node_id 매핑
     * @return
     */
    private Handler<Message<JsonObject>> onMappingTag() {
        return result -> {
            JsonObject resData = new JsonObject();
            JsonObject recv_data = result.body();
//            JsonArray tags = recv_data.getJsonArray("tag");
            JsonArray rows = recv_data.getJsonArray("node_info");
            
            // Signal node인지 확인
            // 내부 전용 Node id 는 별도로 관리하여 사용
//            if(rows.size()==1)
//            {
//                JsonObject row = rows.getJsonObject(0);
//                if( (row.getString("node_mf_id") + "." + row.getString("node_model_id") + "." + row.getString("node_sn")).equals("SIG.SIG.SIG"))
//                {
//                    vertx.sharedData().getClusterWideMap(CacheAddress.SIG_NODEID.toString(), res_sig -> {
//                        res_sig.result().put(CacheAddress.SIG_NODEID.toString(), row.getString("node_id"), res_sig_put -> {});
//                    });
//
//                    result.fail(204, "");
//                    return;
//                }
//            }
            
            vertx.sharedData().getClusterWideMap(CacheAddress.TAGS.toString(), res -> {
                res.result().get(CacheAddress.TAGS.toString(), res_data -> {
                    if(res_data.succeeded()) {
                        JsonArray tags = (JsonArray) res_data.result();

//                        Map<String, String> tmp_tag = new HashMap<String,String>();
                        Map<String, String> tmp_tag_nodeid = new HashMap<String,String>();
                        
                        int tag_size = tags.size();

//                        for (int j = 0; j < tag_size; j++) {
//                            try {
//                                String tag = tags.getString(j);
//                                if(tag!=null) {
//                                    String[] arg = tag.split("\\.");
//                                    if(arg.length > 2) {
//                                        tmp_tag.put(arg[0] + "." + arg[1] + "." + arg[2], tag.substring(0, tag.lastIndexOf(".")));
//                                    }
//                                }
//                            }catch (Exception e){
//                                logger.warn("NISE-000041 node info("+j+") is wrong " + e.getMessage());
//                            }
//                        }

                        
                        for (int i = 0; i < rows.size(); i++) {
                            try {
                                JsonObject row = rows.getJsonObject(i);
                                row.put("tag", row.getString("node_mf_id") + "." + row.getString("node_model_id") + "." + row.getString("node_sn"));
                                tmp_tag_nodeid.put(row.getString("node_mf_id") + "." + row.getString("node_model_id") + "." + row.getString("node_sn"), row.getString("node_id"));
                            }catch (Exception e){
                                logger.warn("NISE-000041 node info("+i+") is wrong " + e.getMessage());
                            }
                        }
                        // Tag list 추가
                        JsonArray nodes = new JsonArray();
                        for (int j = 0; j < tag_size; j++) {
                            try {
                                String tag = tags.getString(j);
                                if(tag!=null) {
                                    String node_id = tmp_tag_nodeid.get(tag);
                                    if(node_id != null) {
                                        JsonObject d = new JsonObject();
                                        d.put("node_id", node_id);
                                        d.put("tag", tag);
                                        nodes.add(d);
                                    }
                                }

                            }catch (Exception e){
                                logger.warn("NISE-000041 node info("+j+") is wrong " + e.getMessage());
                            }
                        }
                        
                        resData.put("rows", rows);
                        resData.put("node_map", nodes);
                        
                        result.reply(resData);
                    }else{
                        result.fail(500, res_data.cause().getMessage());
                    }
                });
            });

        };
    }


}
