package com.ntels.nise.component.device.control;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.query.Query;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonObject;
import static com.ntels.nise.component.device.control.command.DeviceCommand.*;

/**
 * Created by youngdong on 2016-07-21.
 */
public class DeviceControlComponent extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();
        // Json format 으로 수정 적용
        vertx.eventBus().consumer(EventBusAddress.COMP_CONTROL.toString(), onCommandExec());
    }


    private Handler<Message<JsonObject>> onCommandExec()
    {
        return result -> {

            JsonObject command = result.body();
            JsonObject response_format =  onConvertResponseFormat(command);
            String command_type = null;
            try{
                response_format.put("command_status", "SUCCESS");
                command_type = command.getString("command_type");

                if(command_type!=null) {
                    // pre-defined
                    switch (command_type) {
                        case "reboot":
                            reboot();
                            result.reply(response_format);
                            break;
                        case "report_on":
                            report_on(vertx);
                            result.reply(response_format);
                            break;
                        case "report_off":
                            report_off(vertx);
                            result.reply(response_format);
                            break;
                        case "pause":
                            pause(vertx);
                            result.reply(response_format);
                            break;
                        case "start":
                            startApp(vertx);
                            result.reply(response_format);
                            break;
                        case "status_check":
                            // 전송 및 프로세스 시작/중지 상태 조회 (cache에 상태값 저장)
                            JsonObject cache_report = Utils.getCacheQuery(CacheAddress.GW_STATUS.toString(), "report");
                            vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), cache_report, rs_report -> {
                                if(rs_report.succeeded()){
                                    JsonObject cache_operstate = Utils.getCacheQuery(CacheAddress.GW_STATUS.toString(), "operstate");
                                    vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), cache_operstate, rs_operstate -> {
                                        if(rs_operstate.succeeded())
                                        {
                                            String operstate = (String)rs_operstate.result().body();
                                            String report = (String)rs_report.result().body();

                                            response_format.put("response_value", (operstate==null?"operstate_start":operstate) + ", " + (report==null?"report_on":report));
                                            result.reply(response_format);
                                        }else{
                                            response_format.put("command_status", "FAIL");
                                            result.reply(response_format);
                                        }
                                    });
                                }else{
                                    response_format.put("command_status", "FAIL");
                                    result.reply(response_format);
                                }
                            });
                            break;
                        case "time_sync":
                            time_sync(command.getString("request_value"));
                            result.reply(response_format);
                            break;
                        case "signal_power":
                            signal_power();
                            result.reply(response_format);
                            break;
                        case "diagnostic":
                            diagnostic();
                            result.reply(response_format);
                            break;
                        case "profile_reset":
                            profile_reset();
                            result.reply(response_format);
                            break;
                        // 여기서 부터 사용자 정의
                        case "retrieve.node":
                            vertx.eventBus().send(EventBusAddress.SQL_SELECT.toString(), new JsonObject().put("sql", Query.SELECT_NODE_INFO_TAG_NODEID.toString()), rs_select -> {
                                if(rs_select.succeeded())
                                    response_format.put("response_value", java.util.Base64.getEncoder().encodeToString(rs_select.result().body().toString().getBytes()));
                                else {
                                    if( ((ReplyException)rs_select.cause()).failureCode() == ErrorCode.NO_DATA) {
                                        logger.warn("NISE-000015 no node data");
                                        result.fail(ErrorCode.NO_DATA, "");
                                    }else {
                                        logger.error("NISE-000015 failed to execute query : " + rs_select.cause().getMessage());
                                        response_format.put("command_status", "FAIL");
                                    }
                                }
                                logger.info("response : " + response_format.toString());
                                result.reply(response_format);
                            });
                            break;
                        default:
                            logger.error("NISE-000033 "+command_type+" is undefined type ");
                            result.fail(ErrorCode.NOT_FOUND, command_type + " is undefined type");
                            response_format.put("command_status", "FAIL");
                            result.reply(response_format);
                    }
                }else{
                    // user-defined
                    logger.error("NISE-000033 command_type is null");
                    result.fail(ErrorCode.NOT_FOUND, "command_type is null");
                    response_format.put("command_status", "FAIL");
                    result.reply(response_format);
                }

            }catch(Exception e){
                logger.error( "NISE-000004 failed to execute "+ command_type + " : " + e.getMessage());
                response_format.put("command_status", "FAIL");
                result.reply(response_format);
            }

        };
    }




    private JsonObject onConvertResponseFormat(JsonObject control_format)
    {

        JsonObject response_format = new JsonObject();
        response_format.put("message_type", control_format.getString("message_type"));
        response_format.put("device_id", "");
        response_format.put("device_key", "");
        response_format.put("command_id", control_format.getString("command_id"));
        response_format.put("command_type", control_format.getString("command_type"));
//                response_format.put("response_value", "");
        return response_format;
    }
}
