package com.ntels.nise.component.device.install;


import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-29.
 */
public class FirmwareInstallComponent extends BaseVerticle{

    @Override
    protected void onInit() {
        super.onInit();

        vertx.eventBus().consumer(EventBusAddress.COMP_FORMAT_DOWNLOAD_FIRMWARE.toString(), onDownloadResult());
        vertx.eventBus().consumer(EventBusAddress.IN_MAS.FIRMWARE_INSTALL.toString(), onInstall());

    }

    private Handler<Message<JsonObject>> onDownloadResult()
    {
        return result -> {
            JsonObject ftp_command = result.body();
            JsonObject ftp_reply = new JsonObject();
            ftp_reply.put("message_type", "firmware");
            ftp_reply.put("device_id", "");
            ftp_reply.put("device_key", "");
            ftp_reply.put("command_id", ftp_command.getString("command_id"));
            ftp_reply.put("work_type", ftp_command.getString("work_type"));
            ftp_reply.put("work_result", ftp_command.getString("work_result"));
            result.reply(ftp_reply);
        };
    }

    private Handler<Message<JsonObject>> onInstall()
    {
        return result -> {

            JsonObject install_command = result.body();
            String status = "FAIL";

            try {

                // todo firmware upgrade 설치 관련 내용 추가

                status = "SUCCESS";

            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), getResultFormat(status, install_command.getString("command_id")));
        };
    }



    private JsonObject getResultFormat(String status, String command_id)
    {
        JsonObject result = new JsonObject();
        result.put("message_type", "firmware");
        result.put("device_id", "");
        result.put("device_key", "");
        result.put("command_id", command_id);
        result.put("work_type", "upgrade");
        result.put("work_result", status);

        return result;
    }
}
