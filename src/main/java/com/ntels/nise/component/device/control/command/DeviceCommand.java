package com.ntels.nise.component.device.control.command;


import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-21.
 */
public class DeviceCommand {


    public static void reboot() throws Exception
    {
        Utils.command_exec( "cmd.exe", "/c", "shutdown -r -f -t 10" );
    }


    public static void report_on(Vertx vertx) throws Exception
    {
        vertx.eventBus().publish(EventBusAddress.SIG_DEVICE_GW_REPORT_ON_OFF.toString(), "on");
        JsonObject cache = Utils.getCacheQuery(CacheAddress.GW_STATUS.toString(), "report", "report_on");
        vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), cache);
    }


    public static void report_off(Vertx vertx) throws Exception
    {
        vertx.eventBus().publish(EventBusAddress.SIG_DEVICE_GW_REPORT_ON_OFF.toString(), "off");
        JsonObject cache = Utils.getCacheQuery(CacheAddress.GW_STATUS.toString(), "report", "report_off");
        vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), cache);
    }


    public static void pause(Vertx vertx) throws Exception
    {
        JsonObject cache = Utils.getCacheQuery(CacheAddress.GW_STATUS.toString(), "operstate", "operstate_pause");
        vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), cache);
    }


    public static void startApp(Vertx vertx) throws Exception
    {
        JsonObject cache = Utils.getCacheQuery(CacheAddress.GW_STATUS.toString(), "operstate", "operstate_start");
        vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), cache);
    }



    public static void time_sync(String date) throws Exception
    {
        Utils.command_exec(  "cmd.exe", "/c", "date " + date.substring(0,10));
        Utils.command_exec(  "cmd.exe", "/c", "time " + date.substring(11,22) );
    }


    public static void signal_power() throws Exception
    {
    }


    public static void diagnostic() throws Exception
    {
    }


    public static void profile_reset() throws Exception
    {
    }



}
