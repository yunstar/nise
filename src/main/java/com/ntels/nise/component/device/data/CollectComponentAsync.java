package com.ntels.nise.component.device.data;

import java.util.Base64;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.ntels.nise.common.def.ErrorCode;
import io.vertx.core.AbstractVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-09-08.
 */
public class CollectComponentAsync extends AbstractVerticle {

    public final Logger logger = LoggerFactory.getLogger(getClass());
    private BlockingQueue<JsonObject> queue = new LinkedBlockingQueue<JsonObject>();

    public void start() {

        // 기본 데이터 전송 (OPC-UA 방식과 동일 {"id":"", "v" : ""}
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_DEFAULT.toString(), onOPCUAData());

        // OPC UA
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_OPCUA.toString(), onOPCUAData());

        // ACS (Database to TCP)
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_ACS.toString(), onEncodeTCPData());

        // AMR (File to TCP)
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_AMR.toString(), onEncodeTCPData());

        // IEQ 센서 데이터 전송
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_IEQ.toString(), onIEQData());

        // 일정 간격으로 bulk 전송
        sendDataScheduler();

    }

    /**
     * 주기적으로 queue 데이터 전송
     */
    private void sendDataScheduler()
    {
        vertx.setPeriodic(config().getJsonObject("opc.ua").getInteger("send.interval"), handler -> {

            if(queue.size() > 0)
            {
                JsonArray nodes_data = new JsonArray();
                JsonObject data = null;
                while( (data = queue.poll()) != null)
                    nodes_data.add(data);

                logger.debug("nodes_data size : " + nodes_data.size());
//                System.out.println("========= send data ============================");

                JsonObject reply_data = new JsonObject();
                reply_data.put("message_type", "node.content");
                reply_data.put("device_id", "");
                reply_data.put("device_key", "");
                reply_data.put("node_content", nodes_data);

//                System.out.println("## send data : " + reply_data.toString());
                vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), reply_data, rs -> {
                    if(rs.failed())
                        logger.error("send data Error!! ");
                    nodes_data.clear();
                });


            }
        });
    }


    /**
     * Receive Sample : {"timestamp":1468903943073,"values":[{"id":"Simulation Examples.SimBACNet.1st.SimRampA1","v":91,"q":true,"t":1468903942199}, ....
     * Send Sample : {"node_content":[{"content_value":"eyJxIjp0cnV....","creation_time":"2016-07-19T13:32:42.409Z","content_type":"periodic_data","node_id":"N37170034285"}],"message_type":"node.content","device_key":"06ef7cb4a1334af8","device_id":"D55568534748"}
     */
    private Handler<Message<JsonObject>> onOPCUAData()
    {
        return result -> {
            try {
                JsonObject recv_body = result.body();

                // 1. get node_id
                JsonObject node = new JsonObject();
                String tag = recv_body.getString("id");

//                logger.info("## data : " + recv_body.toString());
                JsonObject query = new JsonObject();
                query.put("map", CacheAddress.TAG_NODEID.toString());
                query.put("key", tag);
                vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, handler -> {
                    if(handler.succeeded()) {

                        node.put("node_id", (String) handler.result().body() );

                        if(node.getString("node_id")==null) {
//                                logger.warn("NISE-000007 undefined node : Tag["+recv_body.getString("id")+"]");
                        }else{
                            node.put("content_type", "periodic_data");
                            node.put("creation_time", Utils.getCurrentTime());
//                                node.put("content_value", Base64.getEncoder().encodeToString(recv_body.toString().getBytes()));
                            node.put("content_value", Base64.getEncoder().encodeToString(recv_body.getValue("v").toString().getBytes()));

                            try {
                                queue.put(node);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }catch(Exception e) {
                logger.error("NISE-000008 failed to convert OPC data to MAS format : " + e.getMessage());
            }
        };
    }


    /**
     * Receive Sample : { [{"id":"Simulation Examples.SimBACNet.1st.SimRampA1","v":91}, ....
     * Send Sample : {"node_content":[{"content_value":"eyJxIjp0cnV....","creation_time":"2016-07-19T13:32:42.409Z","content_type":"periodic_data","node_id":"N37170034285"}],"message_type":"node.content","device_key":"06ef7cb4a1334af8","device_id":"D55568534748"}
     */
    private Handler<Message<JsonArray>> onEncodeTCPData()
    {
        return result -> {

            vertx.sharedData().getClusterWideMap(CacheAddress.NODEID_TAG_MAP.toString(), resMap -> {
                resMap.result().get(CacheAddress.NODEID_TAG_MAP.toString(), res -> {
                    try {

                        Map<String,String> m = (Map)res.result();
                        JsonArray values = result.body();

                        for (int i = 0; i < values.size(); i++) {

                            JsonObject node_data = values.getJsonObject(i);

                            // 1. get node_id
                            JsonObject node = new JsonObject();
                            String tag = node_data.getString("id");
                            node.put("node_id", m.get(tag.substring(0,tag.lastIndexOf("."))) );
                            if(node.getString("node_id")==null) {
                                logger.warn("NISE-000007 node_id 조회 실패 : Tag["+node_data.getString("id")+"]");
                                continue;
                            }
                            node.put("content_type", "periodic_data");
                            node.put("content_value", Base64.getEncoder().encodeToString(node_data.toString().getBytes()) );

                            queue.put(node);
                        }

                        //                System.out.println("reply data : " + reply_data.toString());
                    }catch(Exception e){
                        result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
                    }
                });
            });
        };
    }


    /**
     * IEQ 로부터 전송받은 데이터 처리
     * @return
     */
    private Handler<Message<JsonObject>> onIEQData()
    {
        return result -> {
            try {
                JsonObject recv_body = result.body();

                // lux 필드 예외처리 lux를 illumi로 변경
                if(recv_body.containsKey("lux"))
                {
                    int lux = recv_body.getInteger("lux");
                    recv_body.put("illumi", lux);
                    recv_body.remove("lux");
                }

                // 1. 노드아이디 조회
                JsonObject node = new JsonObject();
                String tag = recv_body.getString("mf_id") + "." + recv_body.getString("model_id") + "." + recv_body.getString("sn");

                JsonObject query = new JsonObject();
                query.put("map", CacheAddress.TAG_NODEID.toString());
                query.put("key", tag);
                vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, handler -> {
                    if(handler.succeeded()) {

                        node.put("node_id", (String) handler.result().body() );

                        if(node.getString("node_id")==null) {

                            // 2-1. 등록된 노드가 없는경우 신규노드 등록 요청
                            vertx.eventBus().send(EventBusAddress.COMP_FORMAT_NODE_REGIST_CSV.toString(), new JsonArray().add(tag));

                            // 2-2. 5초 지연 후 다시 호출
                            vertx.setTimer(1500, h -> {
                                vertx.eventBus().send(EventBusAddress.COMP_COLLECT_IEQ.toString(), recv_body, res_send_data -> {
                                    if(res_send_data.succeeded())
                                    {
                                    }
                                });
                            });

                        }else{
                            // 3. Content 데이터 queue에 저장
                            node.put("content_type", "periodic_data");
                            node.put("creation_time", Utils.getCurrentTime());
//                                node.put("content_value", Base64.getEncoder().encodeToString(recv_body.toString().getBytes()));
                            recv_body.remove("mf_id");
                            recv_body.remove("model_id");
                            recv_body.remove("sn");
                            node.put("content_value", Base64.getEncoder().encodeToString(recv_body.toString().getBytes()));

                            try {
                                queue.put(node);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }else{
                        logger.error(handler.cause().getMessage());
                    }
                });
                result.reply(new JsonObject());
            }catch(Exception e) {
                logger.error("NISE-000008 failed to convert IEQ data to MAS format : " + e.getMessage());
                result.fail(500, e.getMessage());
            }
        };
    }
}
