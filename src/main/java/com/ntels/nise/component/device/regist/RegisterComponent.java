package com.ntels.nise.component.device.regist;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.query.Query;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by youngdong on 2016-07-20.
 */
public class RegisterComponent extends BaseVerticle {

    private int interval = 2000;

    @Override
    protected void onInit() {
        super.onInit();

        interval = config().getInteger("nodes.regi.interval", 4000);


        // MAS로 노드 등록 요청 (한번에 요청하면 버퍼문제로 인해 n개 단위로 나눠서 전송)
        vertx.eventBus().consumer(EventBusAddress.COMP_FORMAT_NODE_REGIST_CSV.toString(), onNodeRegistFormatByCSV(false));

        // MAS로 노드 등록 요청 (한번에 요청하면 버퍼문제로 인해 n개 단위로 나눠서 전송) OPC subscribe 까지 처리
        vertx.eventBus().consumer(EventBusAddress.COMP_FORMAT_NODE_REGIST_OPC.toString(), onNodeRegistFormatByCSV(true));

    }




    /**
     * 노드 나눠서 등록
     * @param is_opcua
     * @return
     */
    private Handler<Message<JsonArray>> onNodeRegistFormatByCSV(boolean is_opcua)
    {
        return result -> {

            JsonArray nodes = result.body();


            vertx.executeBlocking( future -> {
                int interval_sum = interval;

                try {
                    JsonObject node_format = new JsonObject();
                    // main
                    node_format.put("message_type", "node.regi");
                    node_format.put("device_id", "");
                    node_format.put("device_key", "");
                    JsonArray node_info = new JsonArray();
                    node_format.put("node_info", node_info);

                    // 한 번에 너무 많은 Node를 등록하면 문제가 되기 때문에 paging처리하여 등록
                    int page_size = config().getInteger("nodes.regi.page.size", 200);
                    int i = 0;
                    for ( i = 0; i < nodes.size(); i++) {
                        try {
                            String[] tag = nodes.getString(i).split("\\.");
                            JsonObject node = new JsonObject();
                            node.put("node_mf_id", tag[0]);
                            node.put("node_model_id", tag[1]);
                            node.put("node_sn", nodes.getString(i).split(tag[0] + "." + tag[1] + ".")[1]);
                            // option
                        /* 2017-09-12 노드이름에 tag명 full name으로 저장 */
                            node.put("node_name", nodes.getString(i));
//            node.put("user_define_url", "");
//            node.put("coap_user_define_url", "");
//            node.put("node_location", "");
//            node.put("node_type", "");
                            node.put("content_push", "Y");
                            node.put("content_save", "N");
                            node.put("command_push", "Y");
                            node.put("command_save", "N");
//            node.put("stream_device_type", "");
//            node.put("stream_url", "");
//            node.put("description", "");

                            node_info.add(node);

                            if(i > 0 && i%page_size==0) {
                                JsonArray tmp = new JsonArray();
                                tmp.addAll(node_info);
                                vertx.setTimer(interval_sum, h -> {
                                    sendNode(tmp);
                                });
                                interval_sum += interval;
                                node_info.clear();
                            }
                        } catch (Exception e) {
                        }
                    }

                    if(node_info.size() > 0) {
                        vertx.setTimer(interval_sum, h -> {
                            sendNode(node_info);
                        });
                        interval_sum += interval;
                    }
                }catch(Exception e){
                    logger.error(e.getMessage());
                }finally {
                    future.complete(interval_sum);
                }

            }, res -> {
                if(is_opcua)
                {
                    int term = (int)res.result();
                    term += (interval * 2);
                    vertx.setTimer(term, handler -> {
                        logger.info("# 4. OPC tags subscribe size[" + nodes.size() + "]");
                        vertx.eventBus().send(EventBusAddress.IN_GATEWAY_OPCUA_SUBSCRIBE.toString(), nodes);
                    });
                }
            });
        };
    }



    private void sendNode(JsonArray node_info)
    {
        JsonObject node_format = new JsonObject();
        // main
        node_format.put("message_type", "node.regi");
        node_format.put("device_id", "");
        node_format.put("device_key", "");
        node_format.put("node_info", node_info);

        logger.info("# Create Node size : " + node_info.size());
        vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), node_format);

    }



}
