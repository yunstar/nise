package com.ntels.nise.component.device.data;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.Map;

/**
 * Created by youngdong on 2016-07-14.
 */
public class CollectComponent extends AbstractVerticle {

    public final Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public void start() {
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_OPC.toString(), this.onEncodeOPCData());
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_ACS.toString(), this.onEncodeACSData());
        vertx.eventBus().consumer(EventBusAddress.COMP_COLLECT_AMR.toString(), this.onEncodeACSData());
    }


    /**
     * Receive Sample : {"timestamp":1468903943073,"values":[{"id":"Simulation Examples.SimBACNet.1st.SimRampA1","v":91,"q":true,"t":1468903942199}, ....
     * Send Sample : {"node_content":[{"content_value":"eyJxIjp0cnV....","creation_time":"2016-07-19T13:32:42.409Z","content_type":"periodic_data","node_id":"N37170034285"}],"message_type":"node.content","device_key":"06ef7cb4a1334af8","device_id":"D55568534748"}
     */
    private Handler<Message<JsonObject>> onEncodeOPCData()
    {
        return result -> {
            vertx.sharedData().getClusterWideMap(CacheAddress.NODEID_TAG_MAP.toString(), resMap -> {
                resMap.result().get(CacheAddress.NODEID_TAG_MAP.toString(), res -> {
                    try {
                        Map<String,String> m = (Map)res.result();

                        JsonObject recv_body = result.body();
                        JsonArray values = recv_body.getJsonArray("values");

                        JsonObject reply_data = new JsonObject();
                        JsonArray nodes_data = new JsonArray();
                        reply_data.put("message_type", "node.content");
                        reply_data.put("device_id", "");
                        reply_data.put("device_key", "");
                        reply_data.put("node_content", nodes_data);

                        for (int i = 0; i < values.size(); i++) {

                            JsonObject node_data = values.getJsonObject(i);

                            // 1. get node_id
                            JsonObject node = new JsonObject();
                            String tag = node_data.getString("id");
                            node.put("node_id", m.get(tag.substring(0, tag.lastIndexOf("."))));
                            if (node.getString("node_id") == null) {
                                logger.warn("NISE-000007 undefined node : Tag[" + node_data.getString("id") + "]");
                                continue;
                            }
                            node.put("content_type", "periodic_data");
                            node.put("creation_time", Utils.getFormatDate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", node_data.getLong("t")));
                            node_data.remove("q");
                            node_data.remove("t");
                            node.put("content_value", Base64.getEncoder().encodeToString(node_data.toString().getBytes()));

                            nodes_data.add(node);
                        }

//                System.out.println("reply data : " + reply_data.toString());
                        result.reply(reply_data);
                    } catch (Exception e) {
                        result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
                    }
                });
            });
        };
    }



    /**
     * Receive Sample : { [{"id":"Simulation Examples.SimBACNet.1st.SimRampA1","v":91}, ....
     * Send Sample : {"node_content":[{"content_value":"eyJxIjp0cnV....","creation_time":"2016-07-19T13:32:42.409Z","content_type":"periodic_data","node_id":"N37170034285"}],"message_type":"node.content","device_key":"06ef7cb4a1334af8","device_id":"D55568534748"}
     */
    private Handler<Message<JsonArray>> onEncodeACSData()
    {
        return result -> {

            vertx.sharedData().getClusterWideMap(CacheAddress.NODEID_TAG_MAP.toString(), resMap -> {
                resMap.result().get(CacheAddress.NODEID_TAG_MAP.toString(), res -> {
                    try {

                        Map<String,String> m = (Map)res.result();
                        JsonArray values = result.body();

                        JsonObject reply_data = new JsonObject();
                        JsonArray nodes_data = new JsonArray();
                        reply_data.put("message_type", "node.content");
                        reply_data.put("device_id", "");
                        reply_data.put("device_key", "");
                        reply_data.put("node_content", nodes_data);

                        for (int i = 0; i < values.size(); i++) {

                            JsonObject node_data = values.getJsonObject(i);

                            // 1. get node_id
                            JsonObject node = new JsonObject();
                            String tag = node_data.getString("id");
                            node.put("node_id", m.get(tag.substring(0,tag.lastIndexOf("."))) );
                            if(node.getString("node_id")==null) {
                                logger.warn("NISE-000007 node_id 조회 실패 : Tag["+node_data.getString("id")+"]");
                                continue;
                            }
                            node.put("content_type", "periodic_data");
                            node.put("content_value", Base64.getEncoder().encodeToString(node_data.toString().getBytes()) );

                            nodes_data.add(node);
                        }

    //                System.out.println("reply data : " + reply_data.toString());
                        result.reply(reply_data);
                    }catch(Exception e){
                        result.fail(ErrorCode.SERVER_ERROR, e.getMessage());
                    }

                });
            });
        };
    }


//    @Suspendable
//    private String getNodeID(String key)
//    {
//        JsonObject query = new JsonObject();
//        query.put("map", CacheAddress.TAG_NODEID.toString());
//        query.put("key", key);
//        Message<String> rs_nodeid = awaitResult(h -> vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, h));
//        return rs_nodeid.body();
//    }

}
