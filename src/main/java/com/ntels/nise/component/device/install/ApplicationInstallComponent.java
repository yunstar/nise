package com.ntels.nise.component.device.install;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by youngdong on 2016-07-27.
 */
public class ApplicationInstallComponent extends BaseVerticle{

    @Override
    protected void onInit() {
        super.onInit();

        vertx.eventBus().consumer(EventBusAddress.COMP_FORMAT_DOWNLOAD_APP.toString(), onDownloadResult());
        vertx.eventBus().consumer(EventBusAddress.COMP_INSTALL_APP.toString(), onInstall());

        // 원격패치 완료 후 상태값을 MAS에 전송
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_MAS_PUBLISH_READY.toString(), onInstallResult());
    }

    private Handler<Message<JsonObject>> onDownloadResult()
    {
        return result -> {
            JsonObject ftp_command = result.body();
            JsonObject ftp_reply = new JsonObject();
            ftp_reply.put("message_type", "app");
            ftp_reply.put("device_id", "");
            ftp_reply.put("device_key", "");
            ftp_reply.put("command_id", ftp_command.getString("command_id"));
            ftp_reply.put("work_type", ftp_command.getString("work_type"));
            ftp_reply.put("work_result", ftp_command.getString("work_result"));
            result.reply(ftp_reply);
        };
    }

    private Handler<Message<JsonObject>> onInstall()
    {
        return result -> {

            JsonObject install_command = result.body();

            // 1. 현 프로세스는 재실행될것으로 전달시 필요한 값을 파일로 저장
            Utils.writeFile(Utils.getHome() + "/command_id", install_command.getString("command_id"));

            String new_file_path = "";
            String bak_file_path = "";
            try {
                // 2. 다운로드된 파일을 설치 경로로 이동
                new_file_path = Utils.replaceEnv(config().getString("ftp.tmp.path")) + "/" + install_command.getString("file_name");
                bak_file_path = Utils.getHome() + "/" + "lib" + "/" + "nise-1.0-SNAPSHOT-fat.jar.bak";

                Path new_file = Paths.get(new_file_path);
                Path bak_file = Paths.get(bak_file_path);
                if(Files.exists(bak_file)) Files.delete(bak_file);
                Files.move(new_file, bak_file);

                // 3. 자기 자신을 재실행 할 수 없으므로 restart 라는 파일을 만들어서 재실행 signal 보냄 -> bat로 작성된 재실행 데몬
                Utils.createFile(Utils.getHome() + "/" + "restart");

            } catch (Exception e) {
                logger.error("NISE-000009 failed to move lib \n" +
                        new_file_path + " TO " + bak_file_path + "\n" + e.getMessage());
                vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), getResultFormat("FAIL"));
            }
        };
    }

    private Handler<Message<String>> onInstallResult()
    {
        return result -> {
            Path ok_file    = Paths.get(Utils.getHome() + "/app.patch.ok"),
                fail_file   = Paths.get(Utils.getHome() + "/app.patch.fail");
            String status=null;


            if (Files.exists(ok_file)) {
                status = "SUCCESS";
                try { Files.delete(ok_file); }catch (Exception e){}
            }
            if (Files.exists(fail_file)) {
                status = "FAIL";
                try { Files.delete(fail_file); }catch (Exception e){}
            }

            if(status!=null)
            {
                vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), getResultFormat(status));
            }
        };
    }



    private JsonObject getResultFormat(String status)
    {
        JsonObject result = new JsonObject();
        result.put("message_type", "app");
        result.put("device_id", "");
        result.put("device_key", "");
        result.put("command_id", Utils.readLine(Utils.getHome() + "/command_id", ""));
        result.put("work_type", "install");
        result.put("work_result", status);

        return result;
    }
}
