package com.ntels.nise.component.sys.sem.hexpower;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by youngdong on 2017-11-29.
 */
public class HexpowerComponent extends BaseVerticle {
    public final Logger logger = LoggerFactory.getLogger(getClass());

    final private String PREFIX = "SEM.HEX";
    public static BlockingQueue<byte[]> queue = new LinkedBlockingQueue<byte[]>();
    public static List<Byte> current_list = Collections.synchronizedList(new ArrayList<Byte>());

    @Override
    public void start() throws Exception {
        super.start();

        // 1. 마지막 전송된 시간을 초기화 (1분단위로 전송하기 위한 값)
        vertx.sharedData().getClusterWideMap("sem.hex", (AsyncResult<AsyncMap<String,Long>> result) -> {
            AsyncMap<String, Long> asyncMap = result.result();
            asyncMap.put("send.time", 0L, h ->{});
        });


        vertx.eventBus().consumer(EventBusAddress.SIG_START_HEX_COMP.toString(), handler -> {

            // 1. 노드 등록
            createNodes();

            // 2. 데이터 수집
            vertx.eventBus().consumer(EventBusAddress.IN_HEXP_SERIAL.toString(), recvData());

            // 3. 주기적 데이터 전송
            sendData();

        });

        // OPC 재실행하는 경우 데이터 태양광 노드도 새롭게 등록
        vertx.eventBus().consumer(EventBusAddress.SIG_GEN_HEX_NODE.toString(), handler -> {
            createNodes();
        });

    }


    /**
     *  주기적으로 데이터 전송
     */
    private void sendData()
    {
        vertx.setPeriodic(1000, handler -> {
            if (queue.size() > 0) {

                // 태양광 데이터가 너무 많아 1분단위로 전송
                vertx.sharedData().getClusterWideMap("sem.hex", (AsyncResult<AsyncMap<String,Long>> result) -> {
                    AsyncMap<String, Long> asyncMap = result.result();
                    asyncMap.get("send.time", send_time -> {
                        long last_time = (long)send_time.result();
                        boolean is_send = (System.currentTimeMillis() - last_time) >= 60000;

                        if(is_send)
                            asyncMap.put("send.time", System.currentTimeMillis(), h -> {});

                        byte[] data;
                        while( (data = queue.poll()) != null)
                        {
//                    StringBuffer sb = new StringBuffer();
//                    for(byte b : data)
//                    {
//                        sb.append(String.format("%02x", b & 0xff));
//                    }
//                    logger.info("## recv data : " + sb.toString() );

                            JsonArray d = new JsonArray();

                            if(data[4]==0x30 && data[5]==0x30 && data[6]==0x30 && data[7]==0x34){  // 1. Fault 정보 명령 (data 16byte)
//                        d = new HexpowerParser().parseFaultData(data);
                                // 알람의 경우는 값이 변경된 경우만 데이터 전송
                                getAlarmData(new HexpowerParser().parseFaultData(data));
                            }else if(is_send){
                                if(data[4]==0x30 && data[5]==0x30 && data[6]==0x32 && data[7]==0x30){  // 2. 태양전지 계측 정보 명령 (data 8byte)
                                    d = new HexpowerParser().parseVoltageCurrentData(data);
                                }else if(data[4]==0x30 && data[5]==0x30 && data[6]==0x35 && data[7]==0x30){  // 3. 계통 계측 정보 명령 (data 28byte)
                                    d = new HexpowerParser().parseLineVoltageCurrentFrequencyData(data);
                                }else if(data[4]==0x30 && data[5]==0x30 && data[6]==0x36 && data[7]==0x30){  // 4. 전력량 계측 정보 명령 (data 32byte)
                                    d = new HexpowerParser().parseSolarINVPowerData(data);
                                }else if(data[4]==0x30 && data[5]==0x31 && data[6]==0x65 && data[7]==0x30){  // 5. 시스템 정보 명령 (data 12byte)
                                    // 시스템 정보 명령은 전송하지 않음
                                }else if(data[4]==0x30 && data[5]==0x30 && data[6]==0x37 && data[7]==0x30){  // 6. 태양전지 환경 계측 명령 (data 16byte)
                                    d = new HexpowerParser().parseCircumstanceData(data);
                                }
                            }

                            // Parsing 이 정상적으로 처리되었다면 MQTT로 데이터 전송 (queue를 통해 일괄 전송)
                            int s = d.size();
                            for(int i=0;i<s;i++)
                                vertx.eventBus().send(EventBusAddress.COMP_COLLECT_DEFAULT.toString(), d.getJsonObject(i));
                        }
                    });
                });
            }
        });
    }


    /**
     * 알람이 변경 된 경우만 전송
     * @param alarm_data
     */
    private void getAlarmData(JsonArray alarm_data)
    {
        // 1. Cache에 저장 된 이전 값 로딩
        vertx.sharedData().<String, String>getClusterWideMap("hex.alarm", h -> {
            AsyncMap<String, String> map = h.result();

            // 1-1. 알람 결과 loop
            int size = alarm_data.size();
            for(int i=0;i<size;i++)
            {
                JsonObject d = alarm_data.getJsonObject(i);

                // 1-2. 동일 데이터 값이 다른 경우만 데이터 전송 (이전에 값이 0이고 현재 1이면 데이터 전송
                map.get(d.getString("id"), handler -> {
                    if(handler.succeeded())
                    {
                        String v = handler.result();
                        if(v==null)
                            map.put(d.getString("id"), d.getString("v"), h2 -> {});
                        else if(!v.equals(d.getString("v")))
                        {
                            vertx.eventBus().send(EventBusAddress.COMP_COLLECT_DEFAULT.toString(), d);
                            map.put(d.getString("id"), d.getString("v"), h2 -> {});
                        }
                    }else{
                        map.put(d.getString("id"), d.getString("v"), h2 -> {});
                    }
                });
            }
        });
    }

    /**
     * 실시간 데이터 수집
     * @return
     */
    private Handler<Message<String>> recvData()
    {
        return result -> {
            try {
                String s = result.body();
//                logger.info("### recv d : " + s);
                byte[] bs = Utils.hexStringToByteArray(s.replaceAll(" ",""));

//                StringBuffer sb = new StringBuffer();
                for(final byte b: bs) {
//                    sb.append(String.format("## %02x", b & 0xff));
                    current_list.add(b);
                    if(b==0x04)
                    {
//                        logger.info("## " + sb.toString());
                        try {
                            byte[] bytes = toPrimitives((Byte[])current_list.toArray(new Byte[current_list.size()]));
                            queue.put(bytes);
                        } catch (InterruptedException e) {
                            logger.error(e.getMessage());
                        }
                        current_list.clear();
                    }
                }

            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        };
    }



    private byte[] toPrimitives(Byte[] oBytes)
    {

        byte[] bytes = new byte[oBytes.length];
        for(int i = 0; i < oBytes.length; i++){
            bytes[i] = oBytes[i];
        }
        return bytes;

    }

    /**
     * 노드 등록
     */
    private void createNodes()
    {
        vertx.eventBus().send(EventBusAddress.COMP_FORMAT_NODE_REGIST_CSV.toString(), getTagList());
    }


    /**
     * 정의된 Tag를 목록 전달
     * tag 생성 규칙 : 인버터 국번 . 번지 . 시퀀스
     * @return
     */
    private JsonArray getTagList()
    {
        JsonArray arr = new JsonArray();

        // 1. 환경설정에서 sem 연동 정보 조회 (인버터 수 만큼)
        JsonArray sem_configs = config().getJsonObject("hexpower.serial.master").getJsonArray("inverter");
        int sem_size = sem_configs.size();

        for(int i=0;i<sem_size;i++)
        {
            String inverter_id = sem_configs.getString(i);
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "1"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "2"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "3"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "4"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "5"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "6"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "7"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "8"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "9"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "10"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "11"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "12"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "13"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "14"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "15"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "16"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "17"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "18"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "19"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "20"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "21"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "22"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "23"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "24"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "25"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "26"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "27"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "28"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "29"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "30"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "31"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "32"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "33"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "34"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "35"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "36"));
            arr.add(String.format("%s.%s.%s", PREFIX, inverter_id, "37"));


        }

        return arr;
    }
}
