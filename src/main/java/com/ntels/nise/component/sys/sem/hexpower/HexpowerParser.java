package com.ntels.nise.component.sys.sem.hexpower;

import com.ntels.nise.common.util.Utils;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Created by youngdong on 2017-11-29.
 */
public class HexpowerParser {

    public final Logger logger = LoggerFactory.getLogger(getClass());

    final private String PREFIX = "SEM.HEX";

    public HexpowerParser(){

    }

    /**
     * Fault 정보 명령 Parsing
     * 06 30 31 52 30 30 30 34 | Data 16byte | CheckSum 4byte | 04
     * -------------------------------------------------------------
     * 아래와 같이 4바이트씩 끊어서 뒤에서부터 Data0 ~ Data3 순으로 처리된다.
     * 0x0810 >> 30h 38h 31h 30h
     * @param b
     * @return
     */
    public JsonArray parseFaultData(byte[] b)
    {
        JsonArray arr = new JsonArray();

        if(b.length!=29) {
            logger.warn(String.format("HEXPower packet different 'Fault Info Data' length 29 != %d ", b.length));

                    StringBuffer sb = new StringBuffer();
                    for(byte bs : b)
                        sb.append(String.format("%02x ", bs & 0xff));
                    logger.warn(sb.toString() );

            return null;
        }

        try {
            String model_id = new String(Arrays.copyOfRange(b, 1, 3)); // 인버터 번호

            String[] d1 = Utils.hexa2BinaryReverse(Arrays.copyOfRange(b, 8, 12));    // Data0 ~ Data3
            String[] d2 = Utils.hexa2BinaryReverse(Arrays.copyOfRange(b, 12, 16));   // Data4 ~ Data7
            String[] d3 = Utils.hexa2BinaryReverse(Arrays.copyOfRange(b, 16, 20));   // Data8 ~ Data11
            String[] d4 = Utils.hexa2BinaryReverse(Arrays.copyOfRange(b, 20, 24));   // Data12 ~ Data15

//            logger.info(String.format("length : %d,   d1=%d  d2=%d  d3=%d  d4=%d", b.length, d1.length, d2.length, d3.length, d4.length));
//            logger.info(String.format("%s%s%s%s", d1[0], d1[1], d1[2], d1[3]));
//            logger.info(String.format("%s%s%s%s", d2[0], d2[1], d2[2], d2[3]));
//            logger.info(String.format("%s%s%s%s", d3[0], d3[1], d3[2], d3[3]));
//            logger.info(String.format("%s%s%s%s", d4[0], d4[1], d4[2], d4[3]));

            // 0값을 1로 1값을 0으로 변경
            d1 = Utils.reverseValue(d1);
            d2 = Utils.reverseValue(d2);
            d3 = Utils.reverseValue(d3);
            d4 = Utils.reverseValue(d4);


            // Data0 ~ Data3
            arr.add(getFaultV(model_id, "1", d1[3]));
            arr.add(getFaultV(model_id, "2", d1[4]));
            arr.add(getFaultV(model_id, "3", d1[11]));

            // Data4 ~ Data7
            arr.add(getFaultV(model_id, "4", d2[11]));

            // Data8 ~ Data11
            arr.add(getFaultV(model_id, "5", d3[0]));
            arr.add(getFaultV(model_id, "6", d3[2]));
            arr.add(getFaultV(model_id, "7", d3[7]));

            // Data12 ~ Data15
            arr.add(getFaultV(model_id, "8", d4[0]));
            arr.add(getFaultV(model_id, "9", d4[2]));
            arr.add(getFaultV(model_id, "10", d4[3]));
            arr.add(getFaultV(model_id, "11", d4[4]));
            arr.add(getFaultV(model_id, "12", d4[5]));
            arr.add(getFaultV(model_id, "13", d4[6]));
            arr.add(getFaultV(model_id, "14", d4[7]));
            arr.add(getFaultV(model_id, "15", d4[8]));
            arr.add(getFaultV(model_id, "16", d4[9]));
            arr.add(getFaultV(model_id, "17", d4[10]));


//            logger.info("@@ send data : " + arr.toString());

        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return arr;
    }


    /**
     * 데이터 포맷팅
     * @param model_id
     * @param sn
     * @param v
     * @return
     */
    private JsonObject getFaultV(String model_id, String sn, String v)
    {
        JsonObject o = new JsonObject();
        o.put("id", PREFIX + "." + model_id + "." + sn);
        o.put("v", v);
        return o;
    }


    /**
     * 전압, 전류 계측 Parsing
     * 06 30 31 52 30 30 32 30 | Data 8byte | CheckSum 4byte | 04
     * -------------------------------------------------------------
     * Data0 ~ Data3 : 태양전지 전압
     * Data4 ~ Data7 : 태양전지 전류
     * @param b
     * @return
     */
    public JsonArray parseVoltageCurrentData(byte[] b)
    {
        JsonArray arr = new JsonArray();

        if(b.length!=21) {
            logger.warn(String.format("HEXPower packet different 'Fault Info Data' length 21 != %d ", b.length));

            StringBuffer sb = new StringBuffer();
            for(byte bs : b)
                sb.append(String.format("%02x ", bs & 0xff));
            logger.warn(sb.toString() );

            return null;
        }

        try {
            String model_id = new String(Arrays.copyOfRange(b, 1, 3)); // 인버터 번호

            int voltage  = Utils.hexa2Integer(Arrays.copyOfRange(b, 8, 12));    // Data0 ~ Data3 전압
            int current  = Utils.hexa2Integer(Arrays.copyOfRange(b, 12, 16));   // Data4 ~ Data7 전류

            // Data0 ~ Data3
            arr.add(getFaultV(model_id, "18", voltage+""));

            // Data4 ~ Data7
            arr.add(getFaultV(model_id, "19", current+""));

        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return arr;
    }



    /**
     * 계통 계측정보 (전압, 전류, 주파수) Parsing
     * 06 30 31 52 30 30 35 30 | Data 28byte | CheckSum 4byte | 04
     * -------------------------------------------------------------
     * Data0 ~ Data3 : 계통 RS 선간 전압
     * Data4 ~ Data7 : 계통 ST 선간 전압
     * Data8 ~ Data11 : 계통 TR 선간 전압
     * Data12 ~ Data15 : 계통 R 상 전류
     * Data16 ~ Data19 : 계통 S 상 전류
     * Data20 ~ Data23 : 계통 T 상 전류
     * Data24 ~ Data27 : 계통 주파수
     *
     * @param b
     * @return
     */
    public JsonArray parseLineVoltageCurrentFrequencyData(byte[] b)
    {
        JsonArray arr = new JsonArray();

        if(b.length!=41) {
            logger.warn(String.format("HEXPower packet different 'Fault Info Data' length 41 != %d ", b.length));

            StringBuffer sb = new StringBuffer();
            for(byte bs : b)
                sb.append(String.format("%02x ", bs & 0xff));
            logger.warn(sb.toString() );

            return null;
        }

        try {
            String model_id = new String(Arrays.copyOfRange(b, 1, 3)); // 인버터 번호

            int line_RS_voltage  = Utils.hexa2Integer(Arrays.copyOfRange(b, 8, 12));    // Data0 ~ Data3 계통 RS 선간 전압
            int line_ST_voltage  = Utils.hexa2Integer(Arrays.copyOfRange(b, 12, 16));   // Data4 ~ Data7 계통 ST 선간 전압
            int line_TR_voltage  = Utils.hexa2Integer(Arrays.copyOfRange(b, 16, 20));   // Data8 ~ Data11 계통 TR 선간 전압
            int line_R_current  = Utils.hexa2Integer(Arrays.copyOfRange(b, 20, 24));    // Data12 ~ Data15 계통 R 상 전류
            int line_S_current  = Utils.hexa2Integer(Arrays.copyOfRange(b, 24, 28));    // Data16 ~ Data19 계통 S 상 전류
            int line_T_current  = Utils.hexa2Integer(Arrays.copyOfRange(b, 28, 32));    // Data20 ~ Data23 계통 T 상 전류
            int line_frequency  = Utils.hexa2Integer(Arrays.copyOfRange(b, 32, 36));    // Data24 ~ Data27 계통 주파수


            arr.add(getFaultV(model_id, "20", line_RS_voltage+""));
            arr.add(getFaultV(model_id, "21", line_ST_voltage+""));
            arr.add(getFaultV(model_id, "22", line_TR_voltage+""));
            arr.add(getFaultV(model_id, "23", line_R_current+""));
            arr.add(getFaultV(model_id, "24", line_S_current+""));
            arr.add(getFaultV(model_id, "25", line_T_current+""));
            arr.add(getFaultV(model_id, "26", ((line_frequency>0)?line_frequency / 10.0f:0 )+""));

//            logger.info("@@ send data : " + arr.toString());

        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return arr;
    }


    /**
     * 전력량 계측 정보2 (Solar and INV power) Parsing
     * 06 30 31 52 30 30 36 30 | Data 32byte | CheckSum 4byte | 04
     * -------------------------------------------------------------
     * Data0 ~ Data3 : 태양전지 현재 전력(kW)
     * Data4 ~ Data7 : 인버터 적산 전력량 Low Bit
     * Data8 ~ Data11 : 인버터 적산 전력량 High Bit
     * Data12 ~ Data15 : 인버터 현재 전력(kVA)
     * Data16 ~ Data19 : 최대 출력 전력(kW)
     * Data20 ~ Data23 : 하루 발전량(kWh)
     * Data24 ~ Data27 : 0
     * Data24 ~ Data27 : 인버터 역률
     *
     * @param b
     * @return
     */
    public JsonArray parseSolarINVPowerData(byte[] b)
    {
        JsonArray arr = new JsonArray();

        if(b.length!=45) {
            logger.warn(String.format("HEXPower packet different 'Fault Info Data' length 45 != %d ", b.length));

            StringBuffer sb = new StringBuffer();
            for(byte bs : b)
                sb.append(String.format("%02x ", bs & 0xff));
            logger.warn(sb.toString() );

            return null;
        }

        try {
            String model_id = new String(Arrays.copyOfRange(b, 1, 3)); // 인버터 번호

            int solar_kw        = Utils.hexa2Integer(Arrays.copyOfRange(b, 8, 12));    // Data0 ~ Data3 태양전지 현재 전력(kW)
            int inv_low_kwh     = Utils.hexa2Integer(Arrays.copyOfRange(b, 12, 16));   // Data4 ~ Data7 인버터 적산 전력량 Low Bit
            int inv_high_kwh    = Utils.hexa2Integer(Arrays.copyOfRange(b, 16, 20));   // Data8 ~ Data11 인버터 적산 전력량 High Bit
            int inv_kva         = Utils.hexa2Integer(Arrays.copyOfRange(b, 20, 24));    // Data12 ~ Data15 인버터 현재 전력(kVA)
            int max_gen_kw      = Utils.hexa2Integer(Arrays.copyOfRange(b, 24, 28));    // Data16 ~ Data19 최대 출력 전력(kW)
            int today_gen_kw    = Utils.hexa2Integer(Arrays.copyOfRange(b, 28, 32));    // Data20 ~ Data23 하루 발전량(kWh)
            // 0 Data24 ~ Data27 (사용안함)
            int inv_PF          = Utils.hexa2Integer(Arrays.copyOfRange(b, 36, 40));    // Data28 ~ Data31 인버터 역률


            arr.add(getFaultV(model_id, "27", ((solar_kw>0)?solar_kw / 10.0f:0 )+""));
            arr.add(getFaultV(model_id, "28", inv_low_kwh+""));
            arr.add(getFaultV(model_id, "29", inv_high_kwh+""));
            arr.add(getFaultV(model_id, "30", ((inv_kva>0)?inv_kva / 10.0f:0 )+""));
            arr.add(getFaultV(model_id, "31", ((max_gen_kw>0)?max_gen_kw / 10.0f:0 )+""));
            arr.add(getFaultV(model_id, "32", today_gen_kw+""));
            arr.add(getFaultV(model_id, "33", ((inv_PF>0)?inv_PF / 10.0f:0 )+""));

            // ((line_frequency>0)?line_frequency / 10.0f:0 )+""

//            logger.info("@@ send data : " + arr.toString());

        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return arr;
    }


    /**
     * 태양전지 환경 계측 정보 Parsing
     * 06 30 31 52 30 30 37 30 | Data 16byte | CheckSum 4byte | 04
     * -------------------------------------------------------------
     * Data0 ~ Data3 : 경사면 일사량 (T-Radiation)
     * Data4 ~ Data7 : 수평면 일사량 (H-Radiation)
     * Data8 ~ Data11 : 외부주위 온도 (Out Temperature)
     * Data12 ~ Data15 : 태양전지 모듈 온도 (Module Temperature)
     *
     * @param b
     * @return
     */
    public JsonArray parseCircumstanceData(byte[] b)
    {
        JsonArray arr = new JsonArray();

        if(b.length!=29) {
            logger.warn(String.format("HEXPower packet different 'Fault Info Data' length 29 != %d ", b.length));

            StringBuffer sb = new StringBuffer();
            for(byte bs : b)
                sb.append(String.format("%02x ", bs & 0xff));
            logger.warn(sb.toString() );

            return null;
        }

        try {
            String model_id = new String(Arrays.copyOfRange(b, 1, 3)); // 인버터 번호

            int t_radiation     = Utils.hexa2Integer(Arrays.copyOfRange(b, 8, 12));    // Data0 ~ Data3 경사면 일사량 (T-Radiation)
            int h_radiation     = Utils.hexa2Integer(Arrays.copyOfRange(b, 12, 16));   // Data4 ~ Data7 수평면 일사량 (H-Radiation)
            int out_temp        = Utils.hexa2Integer(Arrays.copyOfRange(b, 16, 20));   // Data8 ~ Data11 외부주위 온도 (Out Temperature)
            int module_temp     = Utils.hexa2Integer(Arrays.copyOfRange(b, 20, 24));    // Data12 ~ Data15 태양전지 모듈 온도 (Module Temperature)

            /*
              공통) 인버터 400 ~ 2000 의 수치로 전송되며 400 이하의 값은 400으로 본다 -> 0 ~ 1600 으로 처리
              1) 일사량은 0 ~ 2000 의 수치로 변환  (인버터의 0 ~ 1600 값의 비율 대비)
              2) 온도는 -20 ~ 80 의 수치로 변환  (인버터의 0 ~ 1600 값의 비율 대비)
             */
            double IRRADIATION_AMOUNT_UNIT = 2000.0 / 1600.0;
            double TEMP_UNIT = 100.0 / 1600.0;

            arr.add(getFaultV(model_id, "34", ( (((t_radiation<400)?400:t_radiation) - 400) * IRRADIATION_AMOUNT_UNIT )+""));
            arr.add(getFaultV(model_id, "35", ( (((h_radiation<400)?400:h_radiation) - 400) * IRRADIATION_AMOUNT_UNIT )+""));
            arr.add(getFaultV(model_id, "36", ( (((out_temp<400)?400:out_temp) - 400) * TEMP_UNIT - 20 )+""));
            arr.add(getFaultV(model_id, "37", ( (((module_temp<400)?400:module_temp) - 400) * TEMP_UNIT - 20 )+""));

            // ((line_frequency>0)?line_frequency / 10.0f:0 )+""

//            logger.info("@@ send data : " + arr.toString());

        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return arr;
    }


    public static void main(String[] args)
    {
        HexpowerParser hp = new HexpowerParser();

        // 1. Fault Data Sample
        byte[] b = {0x06,0x30,0x31,0x52,0x30,0x30,0x30,0x34,
                0x62,0x66,0x66,0x66,0x32,0x66,0x66,0x65,0x30,0x30,0x30,0x30,0x36,0x37,0x66,0x66,0x30,0x36,0x36,0x07,0x04};
        JsonArray arr = hp.parseFaultData(b);
        System.out.println(arr.encodePrettily());

        // 2. Voltage Data Sample
        byte[] b2 = {0x06,0x30,0x31,0x52,0x30,0x30,0x32,0x30,
                0x30,0x31,0x35,0x34,0x30,0x30,0x33,0x32,
                0x30,0x36,0x36,0x07,0x04};
        JsonArray arr2 = hp.parseVoltageCurrentData(b);
        System.out.println(arr2.encodePrettily());
    }
}
