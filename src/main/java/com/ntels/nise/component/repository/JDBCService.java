package com.ntels.nise.component.repository;

import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;

import java.util.List;

/**
 * Created by youngdong on 2016-07-14.
 */
public class JDBCService extends BaseVerticle {

    private JDBCClient client;

    @Override
    protected void onInit() {
        super.onInit();

        initJDBCClient();

        vertx.eventBus().consumer(EventBusAddress.SQL_SELECT.toString(), selectQuery());
        vertx.eventBus().consumer(EventBusAddress.SQL_EXEC.toString(), executeQuery());
        vertx.eventBus().consumer(EventBusAddress.SQL_UPDATE.toString(), updateQuery());
//        vertx.eventBus().consumer(EventBusAddress.SQL_UPDATE_BATCH.toString(), updateBatchQuery());

    }




    private void initJDBCClient()
    {
        JsonObject config = config().getJsonObject("jdbc.info");

        client = JDBCClient.createShared(vertx, new JsonObject()
                .put("url", config.getString("url"))
                .put("driver_class", config.getString("driver_class"))
                .put("user", config.getString("user"))
                .put("password", config.getString("password"))
                .put("max_pool_size", config.getInteger("max_pool_size")));
    }


    private Handler<Message<JsonObject>> selectQuery() {
        return result -> {
            client.getConnection( connection -> {
                SQLConnection conn = null;
                try {
                    JsonObject query = result.body();
                    String sql = query.getString("sql");
                    JsonArray params = query.getJsonArray("params");

                    conn = connection.result();
                    if (params == null) {
                        conn.query(sql, res -> {
                            if(res.failed()) {
                                result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
                                result.reply(ErrorCode.SQL_ERROR);
                            }else{
                                if(res.result().getNumRows()==0)
                                    result.fail(ErrorCode.NO_DATA, "no data");
                                result.reply(new JsonArray(res.result().getRows()));
                            }

                        });
                    } else {
                        conn.queryWithParams(sql, params, res -> {
                            if(res.failed()){
                                result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
                                result.reply(ErrorCode.SQL_ERROR);
                            }else {
                                if(res.result().getNumRows()==0)
                                    result.fail(ErrorCode.NO_DATA, "no data");
                                result.reply(new JsonArray(res.result().getRows()));
                            }
                        });
                    }
                }catch(Exception e){
                    result.fail(ErrorCode.SQL_ERROR, e.getMessage());
                    result.reply(ErrorCode.SQL_ERROR);
                }finally {
                    try{ conn.close(); }catch(Exception e){}
                }
            });

        };
    }



    private Handler<Message<JsonObject>> executeQuery() {
        return result -> {
            client.getConnection( connection -> {
                SQLConnection conn = null;
                try {
                    JsonObject query = result.body();
                    String sql = query.getString("sql");

                    conn = connection.result();
                    conn.execute(sql, res -> {
                        if(res.failed()) {
                            result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
                            result.reply(ErrorCode.SQL_ERROR);
                        }else{
                            result.reply(ErrorCode.OK);
                        }
                    });

                }catch(Exception e){
                    result.fail(ErrorCode.SQL_ERROR, e.getMessage());
                    result.reply(ErrorCode.SQL_ERROR);
                }finally {
                    try{ conn.close(); }catch(Exception e){}
                }
            });
        };
    }

    private Handler<Message<JsonObject>> updateQuery() {
        return result -> {
            client.getConnection( connection -> {
                SQLConnection conn = null;
                try {
                    JsonObject query = result.body();
                    String sql = query.getString("sql");
                    JsonArray params = query.getJsonArray("params");

                    conn = connection.result();
                    if (params == null) {
                        conn.update(sql, res -> {
                            if(res.failed()) {
                                result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
                                result.reply(ErrorCode.SQL_ERROR);
                            }else
                                result.reply(ErrorCode.OK);
                        });
                    } else {
                        conn.updateWithParams(sql, params, res -> {
                            if(res.failed()) {
                                result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
                                result.reply(ErrorCode.SQL_ERROR);
                            }else
                                result.reply(ErrorCode.OK);
                        });
                    }
                }catch(Exception e){
                    result.fail(ErrorCode.SQL_ERROR, e.getMessage());
                    result.reply(ErrorCode.SQL_ERROR);
                }finally {
                    try{ conn.close(); }catch(Exception e){}
                }
            });

        };
    }


//    private Handler<Message<JsonObject>> updateBatchQuery() {
//        return result -> {
//            client.getConnection( connection -> {
//                SQLConnection conn = null;
//                try {
//                    JsonObject query = result.body();
//                    String sql = query.getString("sql");
//                    JsonArray params = query.getJsonArray("params");
//
//                    conn = connection.result();
//                    if (params == null) {
//                        conn.batch(query.getJsonArray("sql").getList(), res -> {
//                            if(res.failed()) {
//                                result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
//                                result.reply(ErrorCode.SQL_ERROR);
//                            }else
//                                result.reply(ErrorCode.OK);
//                        });
//                    } else {
//                        conn.batchWithParams(sql, params.getList(), res -> {
//                            if(res.failed()) {
//                                result.fail(ErrorCode.SQL_ERROR, res.cause().getMessage());
//                                result.reply(ErrorCode.SQL_ERROR);
//                            }else
//                                result.reply(ErrorCode.OK);
//                        });
//                    }
//                }catch(Exception e){
//                    result.fail(ErrorCode.SQL_ERROR, e.getMessage());
//                    result.reply(ErrorCode.SQL_ERROR);
//                }finally {
//                    try{ conn.close(); }catch(Exception e){}
//                }
//            });
//
//        };
//    }

}
