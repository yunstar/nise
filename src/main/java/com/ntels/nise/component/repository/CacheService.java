package com.ntels.nise.component.repository;

import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheService extends BaseVerticle {

    public final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected void onInit() {
        super.onInit();

        vertx.eventBus().consumer(EventBusAddress.CACHE_PUT.toString(), putCache());
        vertx.eventBus().consumer(EventBusAddress.CACHE_GET.toString(), getCache());
        vertx.eventBus().consumer(EventBusAddress.CACHE_REMOVE.toString(), removeCache());
        vertx.eventBus().consumer(EventBusAddress.CACHE_ADD.toString(), addCache());
    }

    private Handler<Message<JsonObject>> putCache() {
        return message -> {
            JsonObject body = message.body();
            String map = body.getString("map");
            String key = body.getString("key");
            String value = body.getString("value");

            if(value!=null)
            vertx.sharedData().getClusterWideMap(map, result -> {
                result.result().put(key, value, p_result -> {
                    if (p_result.succeeded()) {
                        message.reply(ErrorCode.OK);
                    } else {
                        logger.error("NISE-000016 failed to save data in cache : " + p_result.cause().getMessage());
                        message.fail(ErrorCode.BAD_REQUEST, p_result.cause().getMessage());
                    }
                });
            });
        };
    }

    private Handler<Message<JsonObject>> removeCache() {
            return message -> {
                
            JsonObject body = message.body();
            String map = body.getString("map");
            String key = body.containsKey("key")?body.getString("key"):null;
            vertx.sharedData().getClusterWideMap(map, result -> {
                if(result.succeeded())
                {
                    if(key==null)
                        result.result().clear(r_result -> {
                            if (r_result.succeeded()) {
                                message.reply(ErrorCode.OK);
                            } else {
                                logger.error("NISE-000017 failed to remove data in cache : " + result.cause().getMessage());
                                message.fail(ErrorCode.BAD_REQUEST, result.cause().getMessage());
                            }
                        });
                    else
                        result.result().remove(key, r_result -> {
                            if (r_result.succeeded()) {
                                message.reply(ErrorCode.OK);
                            } else {
                                logger.error("NISE-000017 failed to remove data in cache : " + result.cause().getMessage());
                                message.fail(ErrorCode.BAD_REQUEST, result.cause().getMessage());
                            }
                        });
                }else{
                    message.reply(ErrorCode.OK);
                }
            });
        };
    }

    private Handler<Message<JsonObject>> getCache() {
        return message -> {
            JsonObject body = message.body();
            String map = body.getString("map");
            String key = body.getString("key");

            vertx.sharedData().getClusterWideMap(map, result -> {
                result.result().get(key, g_result -> {
                    if (g_result.succeeded())  {
                        message.reply(g_result.result());
                    } else {
                        logger.error("NISE-000017 failed to read data in cache : " + result.cause().getMessage());
                        message.fail(ErrorCode.BAD_REQUEST, result.cause().getMessage());
                    }
                });
            });
        };
    }

    private Handler<Message<JsonObject>> addCache() {
        return message -> {
            JsonObject body = message.body();
            String map = body.getString("map");
            String key = body.getString("key");
            String value = body.getString("value");

            if(value!=null)
            vertx.sharedData().getClusterWideMap(map, result -> {
                result.result().get(key, g_result -> {
                    String values = new String();
                    if (g_result.result() != null) {
                        values = (String) g_result.result();
                    }

                    if (!values.contains(value)) {
                        values = value + ",";
                    }

                    result.result().put(key, values, p_result -> {
                        logger.debug("cache Insert : " + p_result.succeeded());

                        if (p_result.succeeded()) {
                            message.reply(p_result.result());
                        } else {
                            logger.error(result.cause().getMessage());
                            message.fail(ErrorCode.BAD_REQUEST, result.cause().getMessage());
                        }

                    });

                });
            });

        };
    }

}
