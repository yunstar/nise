package com.ntels.nise.deploy;

import com.ntels.nise.common.util.Utils;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.shell.ShellService;
import io.vertx.ext.shell.ShellServiceOptions;
import io.vertx.ext.shell.term.HttpTermOptions;
import io.vertx.ext.shell.term.TelnetTermOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.server.UnicastRemoteObject;
import java.util.Set;

/**
 * Created by ohjunho on 16. 7. 7.
 */
public class DeployShell extends AbstractVerticle {
    public static final Logger logger = LoggerFactory.getLogger(DeployShell.class);
    private static final int RUN = 0;
    private static final int STOP = 1;

    private String thisVerticle;

        // Convenience method so you can run it in your IDE
        public static void main(String[] args) {
            VertxOptions vertxOptions = new VertxOptions().setMaxEventLoopExecuteTime(15000000000L).setMaxWorkerExecuteTime(300000000000L).setClustered(true);

            Vertx.clusteredVertx(vertxOptions, cRslt -> {
                Vertx vertx = cRslt.result();
                vertx.deployVerticle(DeployShell.class.getName());

            });
        }

    private void trayIcon()
    {

        TrayIconHandler.registerTrayIcon(Toolkit.getDefaultToolkit().getImage( Utils.replaceEnv(config().getString("tray.icon.path")) ), "NISE", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Open your application here.
            }

        });


        TrayIconHandler.addItem("Run", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deploy();
            }
        });


        TrayIconHandler.addItem("Stop", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undeploy();
            }
        });

        TrayIconHandler.addItem("Exit", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                terminate();
            }
        });


        TrayIconHandler.displayMessage("Ntels NISE", "start nise", TrayIcon.MessageType.INFO);
        deploy();
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        // init & shutdown logging
        startLogging();
        setShutdownHook();

        runTelnetService(vertx);
        runHttpService(vertx);

        Utils.writeFile(Utils.getHome() + "/PID.txt", Utils.getPID());

        if(Utils.isWindows()) {
            Set<String> verticles = vertx.deploymentIDs();
            for (String verticle : verticles) {
                thisVerticle = verticle;
            }
            trayIcon();
        }
    }


    public void runTelnetService(Vertx vertx) throws Exception {
        ShellService service = ShellService.create(vertx,
                new ShellServiceOptions().setTelnetOptions(
                        new TelnetTermOptions()
                                .setHost("localhost")
                                .setPort(4000)
                )
        );
        service.start();
    }

    public void runHttpService(Vertx vertx) throws Exception {
        ShellService service = ShellService.create(vertx,
                new ShellServiceOptions().setHttpOptions(
                        new HttpTermOptions().
                                setHost("localhost").
                                setPort(4001)
                )
        );
        service.start();
    }



    private void deploy()
    {
        deployVerticle(rs -> {
            if(rs.failed())
            {
                logger.error("failed to start tray icon ");
                TrayIconHandler.displayMessageIgnore("failed to run NISE", rs.cause().getMessage(), TrayIcon.MessageType.ERROR);
            }else{
                logger.debug("start tray icon");
                TrayIconHandler.displayMessageIgnore("run NISE", "", TrayIcon.MessageType.INFO);
                TrayIconHandler.enableMenu(RUN, false);
                TrayIconHandler.enableMenu(STOP, true);
            }
        });
    }

    private void undeploy()
    {
        undeployVerticle(false, rs -> {
            if(rs.failed())
            {
                logger.error("failed to stop tray icon");
                TrayIconHandler.displayMessageIgnore("failed to stop NISE", rs.cause().getMessage(), TrayIcon.MessageType.ERROR);
            }else{
                logger.debug("stop tray icon");
                TrayIconHandler.displayMessageIgnore("stop NISE", "", TrayIcon.MessageType.INFO);
                TrayIconHandler.enableMenu(RUN, true);
                TrayIconHandler.enableMenu(STOP, false);
            }
        });
    }

    private void terminate()
    {
        undeployVerticle(true, rs -> {
            if(rs.failed())
            {
                logger.error("terminate ");
                TrayIconHandler.displayMessageIgnore("failed to terminate NISE", rs.cause().getMessage(), TrayIcon.MessageType.ERROR);
            }else{
                logger.debug("terminate ");
                TrayIconHandler.displayMessageIgnore("terminate NISE", "", TrayIcon.MessageType.INFO);
            }

            System.exit(0);
        });
    }

    private void deployVerticle(Handler<AsyncResult<String>> result)
    {
        vertx.executeBlocking(future -> {
            try {
                vertx.fileSystem().readFile( Utils.replaceEnv(config().getString("deploy.path")), file -> {
                    if (file.succeeded()) {
                        JsonArray jsonArray = file.result().toJsonArray();

                        for (int i = 0; i < jsonArray.size(); i++) {
                            JsonObject jsonObject = jsonArray.getJsonObject(i);
                            String clazz = jsonObject.getString("class");
                            String worker = jsonObject.getString("worker");
                            String ha = jsonObject.getString("ha");
                            String filePath = Utils.replaceEnv(jsonObject.getString("conf"));


                            DeploymentOptions options = new DeploymentOptions();

                            if (worker != null && "true".equals(worker)) {
                                options.setWorker(true);
                            } else {
                                options.setWorker(false);
                            }
                            if (ha != null && "true".equals(ha)) {
                                options.setHa(true);
                            } else {
                                options.setHa(false);
                            }
                            if (filePath != null && !"".equals(filePath)) {
                                vertx.fileSystem().readFile(filePath, fileItem -> {
                                    if (file.succeeded()) {
                                        System.out.println("### " + jsonObject.toString());
                                        options.setConfig(fileItem.result().toJsonObject());
                                        logger.info("It will be deploy : " + clazz);
                                        vertx.deployVerticle(clazz, options);
                                    }
                                });
                            } else {
                                logger.info("It will be deploy : " + clazz);
                                vertx.deployVerticle(clazz, options);
                            }
                        }
                        result.handle(Future.succeededFuture());
                    } else {
                        future.fail(file.cause());
                        result.handle(Future.failedFuture("deploy.txt file not found"));
                    }
                });
            }catch(Exception e){
                result.handle(Future.failedFuture(e));
            }
        }, result);

    }


    private void undeployVerticle(boolean isterminate, Handler<AsyncResult<String>> result)
    {
        vertx.executeBlocking(future -> {
            try {
                Set<String> verticles = vertx.deploymentIDs();

                for (String verticle : verticles) {
                    if (isterminate || !thisVerticle.equals(verticle)) {
                        logger.debug("## unDeploy : " + verticle);
                        vertx.undeploy(verticle);
                    }
                }
                result.handle(Future.succeededFuture());
            }catch(Exception e){
                result.handle(Future.failedFuture(e));
            }
        }, result);

    }






    private void setShutdownHook()
    {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                endLogging();
            }
        });
    }


    private void startLogging()
    {
        logger.info("#######################################################");
        logger.info("##   new NISE start.....                             ##");
        logger.info("#######################################################");
    }

    private void endLogging()
    {
        logger.info("#######################################################");
        logger.info("##   NISE shutdown !!                                ##");
        logger.info("#######################################################");
    }

}