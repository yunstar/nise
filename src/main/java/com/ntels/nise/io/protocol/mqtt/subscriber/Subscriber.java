package com.ntels.nise.io.protocol.mqtt.subscriber;

import java.util.function.Consumer;

import com.ntels.nise.common.def.LockAddress;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.Lock;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Subscriber implements MqttCallback {

    public final Logger logger = LoggerFactory.getLogger(getClass());

    private Vertx vertx;
    private MqttClient client;
    private MqttConnectOptions options;
    private Consumer<MqttMessage> consumer;
    private String topicName;
    private int qos;

    public Subscriber(Vertx vertx, MqttClient client, MqttConnectOptions options, int qos) {
        this.client = client;
        this.options = options;
        this.qos = qos;
        this.vertx = vertx;
    }

    /**
     * The QoS specified is the maximum level that messages will be sent to the client at.
     * For instance if QoS 1 is specified, any messages originally published at QoS 2 will
     * be downgraded to 1 when delivering to the client but messages published at 1 and 0
     * will be received at the same level they were published at.
     *
     * @param topicName The topic to listen to.
     */
    public void subscribe(String topicName, Consumer<MqttMessage> consumer) {

        this.topicName = topicName;
        this.consumer = consumer;

        vertx.setTimer(3000, h -> {
            connect();
        });

        try{
            client.subscribe(topicName, qos);
        }catch (MqttException e){
            logger.error("failed to subscribe : " + e.getMessage());
            vertx.setTimer( 1000 * 10, h -> {
                subscribe(topicName, consumer);
            });
        }
    }


    @Override
    public void connectionLost(Throwable throwable) {
        logger.info("failed to connect MQTT....");
        connect();
    }


    private void connect()
    {
        vertx.sharedData().getLock(LockAddress.MQTT_RE_CONN.toString(), lockHandler -> {
            if (lockHandler.succeeded()) {

                Lock lock = lockHandler.result();

                try {

                    if(client!=null && !client.isConnected())
                    {
                        logger.info("try MQTT connect....");

                        client.connect(options);

                        client.subscribe(topicName, qos);

                        logger.info("completed MQTT connect !! ");
                    }

                    // 접속이 정상적으로 처리되면 lock 해제
                    lock.release();

                } catch (Exception e) {
                    logger.error("NISE-000020 failed to connect to mqtt server : " + e.getMessage());
                    try{ client.unsubscribe(topicName); }catch (Exception ex){}
                    try{ client.disconnect(); }catch (Exception ex){}

                    // 접속실패시 30초 마다 재접속 시도
                    vertx.setTimer(30000, h -> {
                        lock.release();
                        connect();
                    });
                }
            }
        });


    }


    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        consumer.accept(mqttMessage);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
