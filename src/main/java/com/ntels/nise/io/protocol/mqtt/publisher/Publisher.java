package com.ntels.nise.io.protocol.mqtt.publisher;

import java.util.function.Supplier;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.eclipse.paho.client.mqttv3.*;


/**
 * MQTT Publisher implementation.
 * Based on the Eclipse Paho default implementation found here:
 * http://git.eclipse.org/c/paho/org.eclipse.paho.mqtt.java.git/tree/
 *
 * Created by dervism on 13/11/14.
 */

public class Publisher implements MqttCallback {

    private Vertx vertx;
    private MqttClient client;
    private MqttConnectOptions options;
    private Handler<AsyncResult<Void>> resultHandler;
    private int qos;

    public Publisher(Vertx vertx, MqttClient client, MqttConnectOptions options, int qos) {
        this.vertx = vertx;
        this.client = client;
        this.options = options;
        this.qos = qos;
    }

    public void publish(String topicName, Supplier<byte[]> supplier, Handler<AsyncResult<Void>> resultHandler) {
        this.resultHandler = resultHandler;
        if (!client.isConnected()) {
            try {
                client.connect(options);

                vertx.sharedData().getClusterWideMap("mqtt.status", result -> {
                    result.result().put("status", false, h2 -> {});
                });
            } catch (MqttException e) {
                resultHandler.handle(Future.failedFuture(e));

                return;
            }
        }

        try {
            MqttMessage message = new MqttMessage(supplier.get());
            message.setQos(qos);

            // Send the message to the server, control is not returned until
            // it has been delivered to the server meeting the specified
            // quality of service.
            client.publish(topicName, message);
//            client.disconnect();
        } catch (MqttException e) {
            resultHandler.handle(Future.failedFuture(e));
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
    }

    /**
     * For a QoS of 0, when the message has been written to the network, for a QoS of 1, when the message
     * publication has been acknowledged and for a QoS of 2 when the message publication has not only been
     * acknowledged but confirmed to have been the only copy of the message delivered.
     *
     * @param iMqttDeliveryToken
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        resultHandler.handle(Future.succeededFuture());
    }
}
