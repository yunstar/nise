package com.ntels.nise.io.protocol.rxtx;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import jssc.SerialPort;
import jssc.SerialPortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by youngdong on 2017-11-29.
 */
public class RXTXClient {

    private final Logger logger = LoggerFactory.getLogger(RXTXClient.class);

    private JsonObject config;
    private Vertx vertx;
    private String recv_ev_address;
    private SerialPort serialPort;

    public RXTXClient(Vertx vertx, JsonObject config, String recv_ev_address){
        super();

        this.vertx = vertx;
        this.config = config;
        this.recv_ev_address = recv_ev_address;
        init();
    }


    private void init()
    {
        int sem_size = config.getJsonArray("inverter").size();
        this.serialPort = new SerialPort(config.getString("com.port"));

        try {
            logger.info("------------------------------------------------------");
            logger.info("Start Serial(RXTX) Client");
            logger.info("------------------------------------------------------");
            serialPort.openPort();  //Open serial portl
            serialPort.setParams(config.getInteger("speed.bps"),
                    config.getInteger("data.bit"),
                    config.getInteger("stop.bit"),
                    config.getInteger("parity.bit"));


            for(int i=0;i<sem_size;i++) {

                    String inverter_id = config.getJsonArray("inverter").getString(i);

                    // 1. 데이터 수집
                    vertx.setPeriodic(1000, h -> {
                        byte[] read = new byte[0];
                        try {
                            read = serialPort.readBytes();
                            if(read != null && read.length > 0){
                                StringBuffer sb = new StringBuffer();
                                for(byte b : read) {
                                    if(sb.length()==0)
                                        sb.append(String.format("%02x", b & 0xff));
                                    else
                                        sb.append(String.format(" %02x", b & 0xff));
                                }
        //                        logger.info("## recv data : " + sb.toString() );

                                vertx.eventBus().send(this.recv_ev_address, sb.toString());
                            }
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    });
    //                new ReadThread(serialPort).start();

            }

        } catch (SerialPortException e) {
            logger.error(e.getMessage());
        }
    }


    /**
     * 데이터 요청 Frame
     * @param interval
     * @param frams
     */
    public void onSendFrame(int interval, JsonArray frams)
    {
        JsonArray sem_config = config.getJsonArray("inverter");
        int sem_size = sem_config.size();
        final int frame_size = frams.size();
        JsonArray send_frame = new JsonArray();


//        logger.info(String.format("### interval : %d,  sem_size : %d,  frame_size : %d ", interval, sem_size, frame_size));

        // 1. 요청할 Frame 구성
        for(int i=0;i<sem_size;i++) {

            byte[] inverter_id_b = sem_config.getString(i).getBytes();

            for(int j=0;j<frame_size;j++)
            {

                byte[] bb = frams.getBinary(j);

                bb[1] = inverter_id_b[0];
                bb[2] = inverter_id_b[1];

                byte[] check_sum_b = checkSum(bb);
                bb[10] = check_sum_b[0];
                bb[11] = check_sum_b[1];
                bb[12] = check_sum_b[2];
                bb[13] = check_sum_b[3];

                logger.info(String.format("## resuest check sum : %02x   %02x %02x   %02x   %02x %02x %02x %02x   %02x %02x   %02x %02x %02x %02x   %02x "
                        , bb[0], bb[1], bb[2], bb[3], bb[4], bb[5], bb[6], bb[7], bb[8], bb[9], bb[10]& 0xff, bb[11]& 0xff, bb[12]& 0xff, bb[13]& 0xff, bb[14]) );
                send_frame.add(bb);
            }
        }


        // 2. 주기적으로 Frame 요청
        int size = send_frame.size();
        vertx.setPeriodic(interval, h -> {
            long interval_add = 1000;
            for(int i=0;i<size;i++) {
                final int ii = i;

                vertx.setTimer(interval_add, h2 -> {
                    try {
                        serialPort.writeBytes(send_frame.getBinary(ii));
                    } catch (SerialPortException e) {
                        logger.error("HEX Power request error : " + e.getMessage());
                    }
                });

                interval_add += 1000;
            }
        });
    }



    private byte[] checkSum(byte[] b)
    {
        int size = b.length;
        int check_sum = 0;
        for(int i=1;i<size;i++)
        {
            if(b[i] == 0x04)
                break;
            check_sum += Integer.parseInt(String.format("%02x",b[i]), 16);
        }
        String hex_Str = Long.toHexString(check_sum);
        if(hex_Str.length() % 2 != 0)
            hex_Str = "0" + hex_Str;

//        logger.info("# check sum : " + hex_Str);
        return hex_Str.getBytes();
    }

//    /**
//     * Serial Data 읽어서 전송
//     */
//    public class ReadThread extends Thread{
//        SerialPort serial;
//        ReadThread(SerialPort serial){
//            this.serial = serial;
//        }
//
//        public void run() {
//            try {
//                while (true) {
//                    byte[] read = serial.readBytes();
//                    if(read != null && read.length > 0){
//                        StringBuffer sb = new StringBuffer();
//                        for(byte b : read) {
//                            if(sb.length()==0)
//                                sb.append(String.format("%02x", b & 0xff));
//                            else
//                                sb.append(String.format(" %02x", b & 0xff));
//                        }
////                        logger.info("## recv data : " + sb.toString() );
//
//                        vertx.eventBus().send(EventBusAddress.IN_HEXP_SERIAL.toString(), sb.toString());
//                    }
//                }
//            } catch (Exception e) {
//                logger.error(e.getMessage());
//            }
//        }
//    }
}
