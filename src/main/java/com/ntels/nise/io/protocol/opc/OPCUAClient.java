package com.ntels.nise.io.protocol.opc;

import static com.digitalpetri.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;
import static com.google.common.collect.Lists.newArrayList;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.digitalpetri.opcua.sdk.client.api.UaClient;
import com.digitalpetri.opcua.stack.core.types.builtin.DataValue;
import com.digitalpetri.opcua.stack.core.types.builtin.LocalizedText;
import com.digitalpetri.opcua.stack.core.types.builtin.NodeId;
import com.digitalpetri.opcua.stack.core.types.builtin.QualifiedName;
import com.digitalpetri.opcua.stack.core.types.builtin.StatusCode;
import com.digitalpetri.opcua.stack.core.types.builtin.Variant;
import com.digitalpetri.opcua.stack.core.types.builtin.unsigned.UShort;
import com.ntels.nise.common.def.LockAddress;
import io.vertx.core.AsyncResult;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.Lock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.digitalpetri.opcua.sdk.client.OpcUaClient;
import com.digitalpetri.opcua.sdk.client.SessionActivityListener;
import com.digitalpetri.opcua.sdk.client.api.UaSession;
import com.digitalpetri.opcua.sdk.client.api.config.OpcUaClientConfig;
import com.digitalpetri.opcua.sdk.client.api.nodes.attached.UaVariableNode;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaSubscription;
import com.digitalpetri.opcua.stack.client.UaTcpStackClient;
import com.digitalpetri.opcua.stack.core.AttributeId;
import com.digitalpetri.opcua.stack.core.security.SecurityPolicy;
import com.digitalpetri.opcua.stack.core.types.builtin.unsigned.UInteger;
import com.digitalpetri.opcua.stack.core.types.enumerated.MonitoringMode;
import com.digitalpetri.opcua.stack.core.types.enumerated.TimestampsToReturn;
import com.digitalpetri.opcua.stack.core.types.structured.EndpointDescription;
import com.digitalpetri.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
import com.digitalpetri.opcua.stack.core.types.structured.MonitoringParameters;
import com.digitalpetri.opcua.stack.core.types.structured.ReadValueId;
import com.ntels.nise.common.def.ErrorCode;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-09-06.
 */
public class OPCUAClient {

    private final Logger logger = LoggerFactory.getLogger(OPCUAClient.class);
    private Vertx vertx;
    private static JsonObject config;
    private String sendEventbus, recvEventbus;
    private OpcUaClient client = null;
    //    private static UaClient write_client;
    private boolean isCollection = true;
    private JsonArray tag_list = new JsonArray();
//    private OpcUaClientConfig clientConfig;
//    private SessionActivityListener activityListener;

    /* this variable selects the possible data that can be obtained from the node.
	In this case, the parameter "value" of the node (integer number 13 in the
	node attributes list).*/
//    private UnsignedInteger attributeId = UnsignedInteger.valueOf(13);


    public OPCUAClient(Vertx vertx, JsonObject config, String sendEventbus, String recvEventbus, JsonArray tags )
    {
        this.vertx = vertx;
        this.config = config;
        this.sendEventbus = sendEventbus;
        this.recvEventbus = recvEventbus;
        this.tag_list = tags;
        init();
    }


    private void init()
    {

        logger.info("----------------------------------------------------------");
        logger.info("OPC UA client");
        logger.info("----------------------------------------------------------");
        logger.info("endpoint : " + config.getString("endpoint"));
        logger.info("----------------------------------------------------------");

        try {
//            // 실행상태 변경
//            vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
//                AsyncMap<String, Long> asyncMap = result.result();
//                asyncMap.put("running", 0L, h -> {});
//            });

            // 초기 설정값 변경
            vertx.sharedData().getClusterWideMap("opc.status", result -> {
                result.result().put("status", "init", h2 -> {});
            });

            // read only connection
            connectionOPCUA();

            // write only connection
//            initWriteClient();


            // control (write)
            writeConsumer();


            // 재접속 시도
            vertx.setPeriodic(config.getInteger("reactive.session.interval"), rs_recon -> {
                vertx.sharedData().getClusterWideMap("opc.status", (AsyncResult<AsyncMap<String,String>> result)  -> {
                    AsyncMap<String, String> asyncMap = result.result();
                    asyncMap.get("status", rs_status -> {
                        if( rs_status.result().equals("not_connected") )
                        {
                            logger.info("---- try OPC Connect -------------");
//                            try {
//                                client.disconnect().get();
//                            } catch (InterruptedException|ExecutionException e) {
//                                e.printStackTrace();
//                            }
//                            client.connect();

                            // 재접속 시도중으로 변경
                            vertx.sharedData().getClusterWideMap("opc.status", result2 -> {
                                result2.result().put("status", "try", result3 -> {
                                    if(result3.succeeded())
                                        connectionOPCUA();
                                });
                            });
                        }
                    });
                });
            });

        } catch (Exception e) {
            logger.warn("### failed to connect to opc ua : " + e);

            vertx.setTimer(3000, handler -> {
                logger.info("try again connect....");
                init();
            });

        }
    }


    /**
     * OPC UA Connection
     */
    private void connectionOPCUA()
    {

        vertx.sharedData().getLock(LockAddress.OPC_RE_CONN.toString(), lockHandler -> {
            if (lockHandler.succeeded()) {

                Lock lock = lockHandler.result();

                try {
                    EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints(config.getString("endpoint")).get();

                    EndpointDescription endpoint = Arrays.stream(endpoints)
                            .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
                            .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));

                    OpcUaClientConfig clientConfig = OpcUaClientConfig.builder()
                            .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
                            .setApplicationUri("urn:digitalpetri:opcua:client")
                            .setEndpoint(endpoint)
                            .setRequestTimeout(uint(config.getInteger("request.timeout")))
                            .setSessionTimeout(uint(config.getInteger("session.timeout")))
                            .build();

//                    client = null;
                    if(client!=null)
                    {
                        try{ client.disconnect(); }catch (Exception e){}
                        client = null;
                    }
                    client = new OpcUaClient(clientConfig);

                    client.addSessionActivityListener(new SessionActivityListener() {
                        @Override
                        public void onSessionInactive(UaSession session) {

                            logger.warn("---- OPC Connection lost... -------------");

                            // OPC UA 연결상태값 변경
                            vertx.sharedData().getClusterWideMap("opc.status", (AsyncResult<AsyncMap<String,String>> result) -> {
                                AsyncMap<String, String> asyncMap = result.result();
                                asyncMap.get("status", result2 -> {
                                    // 재접속 시도중이 아니라면 not_connected 로 상태값 변경
                                    if(!result2.result().equals("try"))
                                        asyncMap.put("status", "not_connected", h ->{});
                                });
                            });

//                    // opc ua 상태가 변경중이 아닌 경우만 재접속 시도
//                    vertx.setTimer(5000, h -> {
//                        vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
//                            AsyncMap<String, Long> asyncMap = result.result();
//                            asyncMap.get("status", rs_status -> {
//                                asyncMap.get("running", rs_status2 -> {
//                                    if((long)rs_status.result()==0L && (long)rs_status2.result()==0L)
//                                    {
//                                        try{ client.disconnect(); }catch (Exception e){}
//                                        logger.warn("---- Connection Lost.... -------------");
//                                        // 접속 시도중
//                                        asyncMap.put("running", 1L, h2 -> {});
//
//                                        vertx.setTimer(config.getInteger("reactive.session.interval"), rs_recon -> {
//                                            logger.info("---- Re Connect Start -------------");
//                                            // 접속 시도 종료
//                                            asyncMap.put("running", 0L, h2 -> {});
//                                            connectionOPCUA();
//                                            logger.info("---- Re Connect End -------------");
//                                        });
//                                    }
//                                });
//                            });
//                        });
//                    });

                        }

                        @Override
                        public void onSessionActive(UaSession session) {

//                            logger.info("---- OPC Connected -------------");
//
//                            // OPC UA 연결상태값 변경
//                            vertx.sharedData().getClusterWideMap("opc.status", result -> {
//                                result.result().put("status", "connected", h2 -> {});
//                            });
//                            createMonitoring();
                        }
                    });


                    vertx.setTimer(5000, h -> {
                        CompletableFuture<UaSession> c = client.getSession();
                        try {
                            UaSession s = c.get();
                            if (s.getSessionId().isNotNull()) {

                                logger.info("---- OPC Connected -------------");

                                // OPC UA 연결상태값 변경
                                vertx.sharedData().getClusterWideMap("opc.status", result -> {
                                    result.result().put("status", "connected", h2 -> {});
                                });
                                createMonitoring();

                                lock.release();
                            }
                        } catch (InterruptedException | ExecutionException e) {
                            logger.warn("OPC UA Connection exception....");
                            logger.warn(e.getMessage());
                        }
                    });


//                    // opc ua 상태 변경 -> 1:설정변경중 0:일반상태
//                    vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String, Long>> result) -> {
//                        AsyncMap<String, Long> asyncMap = result.result();
//                        asyncMap.put("status", 0L, h -> {
//                        });
//                    });

                } catch (Exception e) {
                    logger.warn("### failed to connect to opc ua : " + e);

                    vertx.setTimer(config.getInteger("reactive.session.interval"), handler -> {
                        logger.info("try again connect....");
                        lock.release();
                        connectionOPCUA();
                    });

                }
            }
        });
    }


    // define some characteristics of the OPC UA Client application
//    private void initWriteClient() throws SecureIdentityException, IOException, URISyntaxException, ServiceException {
//        // initiate the connection to the server
//        write_client = new UaClient(config.getString("endpoint"));
//
//        // define the security level in the OPC UA binary communications
//        write_client.setSecurityMode(SecurityMode.NONE);
//
//        // auto reconnect
//        write_client.setAutoReconnect(true);
//
//        // create an Application Description which is sent to the server
//        ApplicationDescription appDescription = new ApplicationDescription();
//        appDescription.setApplicationName(new org.opcfoundation.ua.builtintypes.LocalizedText("OpcuaClient", Locale.ENGLISH));
//		/* 'localhost' (all lower case) in the ApplicationName and ApplicationURI
//		is converted to the actual host name of the computer in which the application
//		is run.*/
//        // ApplicationUri is a unique identifier for each running instance
//        appDescription.setApplicationUri("urn:localhost:UA:OpcuaClient");
//        // identify the product and should therefore be the same for all instances
//        appDescription.setProductUri("urn:prosysopc.com:UA:OpcuaClient");
//        // define the type of application
//        appDescription.setApplicationType(ApplicationType.Client);
//
//        // define the client application certificate
//        final ApplicationIdentity identity = new ApplicationIdentity();
//        identity.setApplicationDescription(appDescription);
//        // assign the identity to the Client
//        write_client.setApplicationIdentity(identity);
//
//        write_client.connect();
//    }

//    private void reactiveSession()
//    {
//        if(activityListener!=null)
//            client.removeSessionActivityListener(activityListener);
//        client.addSessionActivityListener(activityListener);
//    }


    /**
     * 제어명령 실행
     * @throws Exception
     */
    public void writeConsumer() throws Exception {

        vertx.eventBus().consumer(sendEventbus, result -> {
            JsonObject send_object = (JsonObject) result.body();

            try {

                NodeId nodeId = new NodeId(2, send_object.getString("id"));

                UaVariableNode variableNode = client.getAddressSpace().getVariableNode(nodeId);
                Class c = variableNode.readValue().get().getValue().getValue().getClass();


                logger.info(String.format("# Class Name : %s,   Control : %s",c.getName(), send_object.toString()));
                // write a new random value
//                DataValue newValue = new DataValue(new Variant(send_object.getValue("v")));
                DataValue newValue = new DataValue(parseVariant(String.valueOf(send_object.getValue("v")), c), null, null);
                StatusCode writeStatus = variableNode.writeValue(newValue).get();

                if(writeStatus.isBad())
                    logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + " " + writeStatus.toString());

            } catch (InterruptedException e) {
                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
                result.fail(500, e.getMessage());
            } catch (Exception e) {
                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
                result.fail(500, e.getMessage());
            }
            result.reply(ErrorCode.OK);
        });

    }


    public Variant parseVariant(String var1, Class<?> var2) {
        if(var1 == null) {
            throw new NullPointerException("string is null");
        } else if(var2 == null) {
            throw new NullPointerException("javaclass is null");
        } else {
            Object var7;
//            if(var2 == DateTime.class) {
//                try {
//                    var7 = DateTime.parseDateTime(var1);
//                } catch (ParseException var5) {
//                    throw new IllegalArgumentException(var5);
//                }
//            }
            if(var2 == Double.class) {
                var7 = Double.valueOf(Double.parseDouble(var1));
            } else if(var2 == Float.class) {
                var7 = Float.valueOf(Float.parseFloat(var1));
            } else if(var2 == Boolean.class) {
                var7 = Boolean.valueOf(var1.equals("1")||var1.toLowerCase().equals("true"));
            } else if(var2 == UUID.class) {
                var7 = UUID.fromString(var1);
            } else if(var2 == Short.class) {
                var7 = Short.valueOf(Short.parseShort(var1));
            } else if(var2 == Integer.class) {
                var7 = Integer.valueOf(Integer.parseInt(var1));
            } else if(var2 == Long.class) {
                var7 = Long.valueOf(Long.parseLong(var1));
            } else if(var2 == LocalizedText.class) {
                var7 = new LocalizedText(var1, "");
            } else if(var2 == UShort.class) {
                var7 = UShort.valueOf(var1);
            } else {
                var7 = var1;
            }

            try {
                return new Variant(var2.cast(var7));
            } catch (NumberFormatException var3) {
                throw new IllegalArgumentException(var3);
            } catch (ClassCastException var4) {
                throw new IllegalArgumentException(var4);
            }
        }
    }

    public void updateTagList(JsonArray tag_list)
    {
        try {
//            client.clearSubscription();
//            unSubscibe();

            this.tag_list.clear();
            this.tag_list.addAll(tag_list);


//            if(client==null)
//                connectionOPCUA();
//            else
            {
//                try{ client.disconnect(); }catch (Exception e){}
//                try{ client.connect(); }catch (Exception e){}
                try{ client.clearSubscription(); }catch (Exception e){}
                vertx.setTimer(5000, h -> {
                    try{ this.createMonitoring(); }catch (Exception e){
                        logger.error(e.getMessage());
                    }

//                    // opc ua 상태 변경 -> 1:설정변경중 0:일반상태
//                    vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
//                        AsyncMap<String, Long> asyncMap = result.result();
//                        asyncMap.put("status", 0L, h2 -> {});
//                    });
                });
            }

        } catch (Exception e) {
            logger.error("failed to remove all (OPC)subscription");
        }

    }

    public void createMonitoring()
    {
        vertx.executeBlocking( h -> {
            logger.info("---- OPC createMonitoring size : " + tag_list.size() );

            for (int i = 0; i < tag_list.size(); i++) {
                String tag = tag_list.getString(i);

                // OPC UA subscribe (a tag)
                try{
                    // ACS나 AMR의 경우는 OPC 수집방식이 아니기 때문에 예외처리 고정
//                    if(tag.startsWith("ACS.") || tag.startsWith("AMR."))
//                        continue;

                    subscribe(2, tag);

                    if(i%10==0)
                        Thread.sleep(100);
                }catch (InterruptedException e) {
                    logger.error("NISE-000044 failed to subscribe : " + tag + "\n" + e.getMessage());
                } catch (ExecutionException e) {
                    logger.error("NISE-000044 failed to subscribe : " + tag + "\n" + e.getMessage());
                }
            }
        }, res -> {

        });
    }


    private void unSubscibe() throws Exception
    {
        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0, UInteger.valueOf(100),UInteger.valueOf(100),UInteger.valueOf(100),true, null).get();

        List<MonitoredItemCreateRequest> requests = new ArrayList<MonitoredItemCreateRequest>();

        for (int i = 0; i < tag_list.size(); i++) {
            String tag = tag_list.getString(i);
            NodeId nodeId = new NodeId(2, tag);
            ReadValueId readValueId = new ReadValueId( nodeId, AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);

            MonitoringParameters parameters = new MonitoringParameters(
                    uint(1),    // client handle
                    300.0,     // sampling interval
                    null,       // no (default) filter
                    uint(20),   // queue size
                    false);      // discard oldest

            MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(readValueId, MonitoringMode.Reporting, parameters);
            requests.add(request);
        }

        List<UaMonitoredItem> items = subscription
                .createMonitoredItems(TimestampsToReturn.Server, newArrayList(requests)).get();

        subscription.deleteMonitoredItems(items);
    }

    private void subscribe(int node_id, String tag) throws ExecutionException, InterruptedException {

        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0, UInteger.valueOf(100), UInteger.valueOf(100), UInteger.valueOf(100), true, null).get();

//        System.out.println("# subscribe tag : " + tag);

        NodeId nodeId = new NodeId(node_id, tag);
        ReadValueId readValueId = new ReadValueId(nodeId, AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);

        MonitoringParameters parameters = new MonitoringParameters(
                uint(1),    // client handle
                300.0,     // sampling interval
                null,       // no (default) filter
                uint(20),   // queue size
                false);      // discard oldest

        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(readValueId, MonitoringMode.Reporting, parameters);

        List<UaMonitoredItem> items = subscription
                .createMonitoredItems(TimestampsToReturn.Server, newArrayList(request)).get();

        for (UaMonitoredItem item : items) {


            item.setValueConsumer(handler -> {

//                System.out.println("data : " + handler.toString());
//                System.out.println("tag : " + item.getReadValueId().getNodeId().getIdentifier());
//                System.out.println("value : " + handler.getValue().getValue());

                if (isCollection && handler.getValue().getValue()!=null)
                    vertx.eventBus().send(recvEventbus
                            , new JsonObject().put("id", item.getReadValueId().getNodeId().getIdentifier().toString())
                                    .put("v", handler.getValue().getValue())
                    );

            });
        }
    }


    public void stopSubscribe() throws Exception {
        isCollection = false;
    }

    public void startSubscribe() throws Exception {
        isCollection = true;
    }
}
