package com.ntels.nise.io.protocol.mqtt;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by dervism on 15/11/14.
 */
public interface MQTTClient {

    static MQTTClient create(Vertx vertx, JsonObject config) { return new MQTTClientImpl(vertx, config); }
    public void start(Handler<AsyncResult<Void>> resultHandler);
    public void publish(String topicName, Supplier<byte[]> supplier, Handler<AsyncResult<Void>> resultHandler);
    public void subscribe(String topicName, Consumer<MqttMessage> consumer);
    public void unSubscribe(String topicName) throws MqttException;
    public boolean closeClient();
    public boolean closeClient(String topicName);
    public void connect() throws MqttException;
    public boolean isConnected();
    public void disconnect() throws MqttException;
    public void setCallBackHandler(MqttCallback callBackHandler);
    public String getBrokerId();
    public String getClientId();

}
