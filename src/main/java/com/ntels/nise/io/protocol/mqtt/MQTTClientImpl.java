package com.ntels.nise.io.protocol.mqtt;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ntels.nise.io.protocol.mqtt.publisher.Publisher;
import com.ntels.nise.io.protocol.mqtt.subscriber.Subscriber;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MQTTClientImpl implements MQTTClient {

    private final Vertx vertx;
    private final JsonObject config;
    private MqttClient client;
    private MqttConnectOptions options;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Publisher publisher;

    public MQTTClientImpl(Vertx vertx, JsonObject config) {

        this.vertx =  vertx;
        this.config = config;

        try {

            options = new MqttConnectOptions();
            options.setCleanSession(true);

            if(config.getString("passwd") != null ) {
                options.setPassword(config.getString("passwd").toCharArray());
            }
            if(config.getString("user") != null) {
                options.setUserName(config.getString("user"));
            }
            if(config.getString("broker_url") != null
                && config.getString("broker_url").startsWith("ssl")) {
                options.setSocketFactory(trustAllHosts().getSocketFactory());
            }

            String clientId = config.getString("clientId");

            // Construct an MQTT blocking mode client
            client = new MqttClient(config.getString("broker_url"),clientId, new MemoryPersistence());

        } catch (MqttException e) {
            logger.error("NISE-000019 mqtt config error : " + e.toString());
        }

    }


    @Override
    public void start(Handler<AsyncResult<Void>> resultHandler) {
        vertx.executeBlocking(future -> {
            try {
                connect();
                future.complete();
            } catch (Exception e) {
                logger.error("NISE-000020 failed to connect to mqtt server : " + e.getMessage());
                future.fail(e);
            }
        }, resultHandler);
    }


    @Override
    public void publish(String topicName, Supplier<byte[]> supplier, Handler<AsyncResult<Void>> resultHandler){
        vertx.executeBlocking(future -> {
            try {
                if(publisher==null)
                {
                    publisher = new Publisher(vertx, client, options, config.getInteger("qos"));
                    setCallBackHandler(publisher);
                }

                publisher.publish(topicName, () -> supplier.get(), resultHandler);
                future.complete();
            } catch (Exception e) {
                logger.error("NISE-000021 failed to publish : " + e.getMessage());
                future.fail(e);
            }
        }, resultHandler);


    }

    @Override
    public void subscribe(String topicName, Consumer<MqttMessage> consumer) {
        vertx.executeBlocking(future -> {
            try {
                Subscriber subscriber = new Subscriber(vertx, client, options, config.getInteger("qos"));
                setCallBackHandler(subscriber);
                subscriber.subscribe(topicName, consumer);
                future.complete();
            } catch (Exception e) {
                logger.error("NISE-000022 failed to subscribe : " + e.getMessage());
                future.fail(e);
            }
        }, res -> {
            if(res.succeeded())
                logger.debug("mqtt subscriber ok -> topic[" + topicName + "]");
            else
            logger.error("NISE-000022 failed to subscribe : " + res.cause().getMessage());
        });

    }


    @Override
    public void unSubscribe(String topicName) throws MqttException {
        client.unsubscribe(topicName);
    }

    @Override
    public boolean closeClient()
    {
        return closeClient(null);
    }

    @Override
    public boolean closeClient(String topicName)
    {
        try {
            if(topicName!=null) unSubscribe(topicName);
            disconnect();
        } catch (MqttException e) {
            logger.error(e.toString());
            return false;
        }

        return true;
    }


    @Override
    public void connect() throws MqttException {
        if(!client.isConnected())
            client.connect(options);
//        logger.info("Connected to " + config.getString("broker_url") + " with client ID " + client.getClientId());

    }

    @Override
    public boolean isConnected() {
        return client.isConnected();
    }

    @Override
    public void disconnect() throws MqttException {
        client.disconnect();
//        logger.info("Disconnected");
    }

    @Override
    public void setCallBackHandler(MqttCallback callBackHandler) {
        // Set this wrapper as the callback handler
        client.setCallback(callBackHandler);
    }

    @Override
    public String getBrokerId() {
        return config.getString("broker_url");
    }

    @Override
    public String getClientId() {
        return config.getString("client_id");
    }


    private static SSLContext trustAllHosts() {
        SSLContext sc = null;
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
                // TODO Auto-generated method stub

            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
                // TODO Auto-generated method stub

            }
        }};

        try {
            sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            return sc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sc;
    }

}
