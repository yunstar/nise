package com.ntels.nise.io.protocol.tcp;

import com.ntels.nise.common.util.Utils;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.*;

/**
 * Created by youngdong on 2016-07-29.
 */
public class TCPServer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Vertx vertx;
    private JsonObject config;
    private NetServer server;
    private String sendEventbus, recvEventbus;
    private Handler<Buffer> recv_handler;


    public TCPServer(Vertx vertx, JsonObject config, String recvEventbus)
    {
        this.vertx = vertx;
        this.config = config;
        this.recvEventbus = recvEventbus;
        this.recv_handler = buffer -> {
            vertx.eventBus().send(recvEventbus, buffer.toString("UTF-8"));
        };
        init();
    }

    public TCPServer(Vertx vertx, JsonObject config, String sendEventbus, String recvEventbus)
    {
        this.vertx = vertx;
        this.config = config;
        this.sendEventbus = sendEventbus;
        this.recvEventbus = recvEventbus;
        this.recv_handler = buffer -> {
            vertx.eventBus().send(recvEventbus, buffer.toString("UTF-8"));
        };
        init();
    }


    public TCPServer(Vertx vertx, JsonObject config, Handler<Buffer> recv_handler)
    {
        this.vertx = vertx;
        this.config = config;
        this.recv_handler = recv_handler;
        init();
    }

    public TCPServer(Vertx vertx, JsonObject config, String sendEventbus, Handler<Buffer> recv_handler)
    {
        this.vertx = vertx;
        this.config = config;
        this.sendEventbus = sendEventbus;
        this.recv_handler = recv_handler;
        init();
    }


    private void init()
    {

        NetServerOptions options = new NetServerOptions();
        if(checkConfigInt("idle_timeout")) {
            options.setIdleTimeout(config.getInteger("idle_timeout"));
        }
        if(checkConfigBoolean("ssl"))
        {
            options.setSsl(true);
            try {
                if (checkConfigString("trust_store_type")) {
                    switch (config.getString("trust_store_type").toLowerCase()) {
                        case "jks":
                            options.setTrustStoreOptions(
                                    new JksOptions().
                                            setPath(Utils.replaceEnv(config.getString("path"))).
                                            setPassword(config.getString("password"))
                            );
                            break;
                        case "pem":
                            options.setPemTrustOptions(
                                    new PemTrustOptions().
                                            addCertPath(Utils.replaceEnv(config.getString("path")))
                            );
                            break;
                        default:
                            logger.error("NISE-000035 trust_store_type is only jsk, pem");
                            break;
                    }
                }else
                    logger.error("NISE-000034 TCP ssl config error");
            }catch(Exception e){
                logger.error("NISE-000036 TCP ssl config error");
            }
        }
        logger.info("----------------------------------------------------------");
        server = vertx.createNetServer(options);
        connect();
    }




    private void connect()
    {
        server.connectHandler( socket -> {

            // receive
            socket.handler( recv_handler );

            // send
            if(sendEventbus!=null)
                vertx.eventBus().consumer(sendEventbus, rs_msg -> {
                    socket.write((String)rs_msg.body());
                });

        }).listen(config.getInteger("port"), config.getString("ip"), res -> {
            if(res.failed())
            {
                logger.error("NISE-000037 Failed to open TCP server -> 127.0.0.1:" + config.getInteger("port") + "  " + res.cause().getMessage());

                // 서버실행 실패의 경우 5초뒤 재실행
                vertx.setTimer(1000 * 5, handler -> {
                    connect();
                });
            }
        });
    }


    private boolean checkConfigString(String key)
    {
        return (config.containsKey(key) && config.getString(key)!=null);
    }

    private boolean checkConfigBoolean(String key)
    {
        try{
            return config.getBoolean(key);
        }catch (Exception e){
            return false;
        }
    }

    private boolean checkConfigInt(String key)
    {
        try{
            return (config.containsKey(key) && config.getInteger(key) > 0);
        }catch (Exception e){
            return false;
        }
    }
}
