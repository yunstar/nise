//package com.ntels.nise.io.protocol.opc;
//
//import static com.digitalpetri.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;
//import static com.google.common.collect.Lists.newArrayList;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.concurrent.ExecutionException;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.digitalpetri.opcua.sdk.client.OpcUaClient;
//import com.digitalpetri.opcua.sdk.client.SessionActivityListener;
//import com.digitalpetri.opcua.sdk.client.api.UaSession;
//import com.digitalpetri.opcua.sdk.client.api.config.OpcUaClientConfig;
//import com.digitalpetri.opcua.sdk.client.api.nodes.attached.UaVariableNode;
//import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
//import com.digitalpetri.opcua.sdk.client.api.subscriptions.UaSubscription;
//import com.digitalpetri.opcua.stack.client.UaTcpStackClient;
//import com.digitalpetri.opcua.stack.core.AttributeId;
//import com.digitalpetri.opcua.stack.core.security.SecurityPolicy;
//import com.digitalpetri.opcua.stack.core.types.builtin.*;
//import com.digitalpetri.opcua.stack.core.types.builtin.unsigned.UInteger;
//import com.digitalpetri.opcua.stack.core.types.enumerated.MonitoringMode;
//import com.digitalpetri.opcua.stack.core.types.enumerated.TimestampsToReturn;
//import com.digitalpetri.opcua.stack.core.types.structured.EndpointDescription;
//import com.digitalpetri.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
//import com.digitalpetri.opcua.stack.core.types.structured.MonitoringParameters;
//import com.digitalpetri.opcua.stack.core.types.structured.ReadValueId;
//import com.ntels.nise.common.def.ErrorCode;
//
//import io.vertx.core.Vertx;
//import io.vertx.core.json.JsonArray;
//import io.vertx.core.json.JsonObject;
//
///**
// * Created by youngdong on 2016-09-06.
// */
//public class OPCUAClientOld {
//
//    private final Logger logger = LoggerFactory.getLogger(OPCUAClientOld.class);
//    private Vertx vertx;
//    private JsonObject config;
//    private String sendEventbus, recvEventbus;
//    private OpcUaClient client;
//    private boolean isCollection = true;
//    private JsonArray tag_list = new JsonArray();
//    private OpcUaClientConfig clientConfig;
//    private SessionActivityListener activityListener;
//
//    public OPCUAClientOld(Vertx vertx, JsonObject config, String sendEventbus, String recvEventbus)
//    {
//        this.vertx = vertx;
//        this.config = config;
//        this.sendEventbus = sendEventbus;
//        this.recvEventbus = recvEventbus;
//        init();
//    }
//
//
//    private void init()
//    {
//
//        logger.info("----------------------------------------------------------");
//        logger.info("OPC UA client");
//        logger.info("----------------------------------------------------------");
//        logger.info("endpoint : " + config.getString("endpoint"));
//        logger.info("----------------------------------------------------------");
//
//        try {
//            EndpointDescription[] endpoints = UaTcpStackClient.getEndpoints(config.getString("endpoint")).get();
//
//            EndpointDescription endpoint = Arrays.stream(endpoints)
//                    .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getSecurityPolicyUri()))
//                    .findFirst().orElseThrow(() -> new Exception("no desired endpoints returned"));
//
//            clientConfig = OpcUaClientConfig.builder()
//                    .setApplicationName(LocalizedText.english("digitalpetri opc-ua client"))
//                    .setApplicationUri("urn:digitalpetri:opcua:client")
//                    .setEndpoint(endpoint)
//                    .setRequestTimeout(uint(config.getInteger("request.timeout")))
//                    .setSessionTimeout(uint(config.getInteger("session.timeout")))
//                    .build();
//
//            client = new OpcUaClient(clientConfig);
//
//
//            activityListener = new SessionActivityListener() {
//                @Override
//                public void onSessionInactive(UaSession session) {
//
//                    while(true) {
//                        try {
//                            Thread.sleep(config.getInteger("reactive.session.interval"));
//                            logger.debug("---- Re Connect Start -------------");
////                            client.disconnect().get();
////                            client.connect().get();
////                            client = new OpcUaClient(clientConfig);
//                            client.removeSessionActivityListener(activityListener);
//                            client.disconnect2();
//                            client = new OpcUaClient(clientConfig);
//                            logger.debug("---- Re Connect End -------------");
//                            createMonitoring();
//                            client.addSessionActivityListener(activityListener);
//                            break;
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//                @Override
//                public void onSessionActive(UaSession session) {
//                }
//            };
//
//
//            reactiveSession();
//
//            consumer();
//
//        } catch (Exception e) {
//            logger.error("### failed to connect to opc ua : " + e);
//
//            vertx.setTimer(3000, handler -> {
//                logger.info("try again connect....");
//                init();
//            });
//
//        }
//    }
//
//
//
//    private void reactiveSession()
//    {
//        if(activityListener!=null)
//            client.removeSessionActivityListener(activityListener);
//        client.addSessionActivityListener(activityListener);
//    }
//
//    public void consumer() throws Exception {
//
//        vertx.eventBus().consumer(sendEventbus, result -> {
//            JsonObject send_object = (JsonObject) result.body();
//
//            try {
//
//                NodeId nodeId = new NodeId(2, send_object.getString("id"));
//
//                UaVariableNode variableNode = client.getAddressSpace().getVariableNode(nodeId);
//
//                // write a new random value
//                DataValue newValue = new DataValue(new Variant(send_object.getValue("v")));
//                StatusCode writeStatus = variableNode.writeValue(newValue).get();
//
//                if(writeStatus.isBad())
//                    logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString());
//
//            } catch (InterruptedException e) {
//                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
//                result.fail(500, e.getMessage());
//            } catch (ExecutionException e) {
//                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
//                result.fail(500, e.getMessage());
//            }
//            result.reply(ErrorCode.OK);
//        });
//
//    }
//
//
//    public void updateTagList(JsonArray tag_list)
//    {
//        try {
//              client.clearSubscription();
////            unSubscibe();
//        } catch (Exception e) {
//            logger.error("failed to remove all (OPC)subscription");
//        }
//        this.tag_list.clear();
//        this.tag_list.addAll(tag_list);
//    }
//
//    public void createMonitoring() throws ExecutionException, InterruptedException
//    {
//        System.out.println("---- createMonitoring size : " + tag_list.size() );
//
//        for (int i = 0; i < tag_list.size(); i++) {
//            String tag = tag_list.getString(i);
//
//            // OPC UA subscribe (a tag)
//            try{
//                // ACS나 AMR의 경우는 OPC 수집방식이 아니기 때문에 예외처리 고정
//                if(tag.startsWith("ACS.") || tag.startsWith("AMR."))
//                   continue;
//
//                subscribe(2, tag);
//            }catch (InterruptedException e) {
//                logger.error("NISE-000044 failed to subscribe : " + tag + "\n" + e.getMessage());
//            } catch (ExecutionException e) {
//                logger.error("NISE-000044 failed to subscribe : " + tag + "\n" + e.getMessage());
//            }
//        }
//    }
//
//
//    private void unSubscibe() throws Exception
//    {
//        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0, UInteger.valueOf(100),UInteger.valueOf(100),UInteger.valueOf(100),true, null).get();
//
//        List<MonitoredItemCreateRequest> requests = new ArrayList<MonitoredItemCreateRequest>();
//
//        for (int i = 0; i < tag_list.size(); i++) {
//            String tag = tag_list.getString(i);
//            NodeId nodeId = new NodeId(2, tag);
//            ReadValueId readValueId = new ReadValueId( nodeId, AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);
//
//            MonitoringParameters parameters = new MonitoringParameters(
//                    uint(1),    // client handle
//                    300.0,     // sampling interval
//                    null,       // no (default) filter
//                    uint(20),   // queue size
//                    false);      // discard oldest
//
//            MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(readValueId, MonitoringMode.Reporting, parameters);
//            requests.add(request);
//        }
//
//        List<UaMonitoredItem> items = subscription
//                .createMonitoredItems(TimestampsToReturn.Server, newArrayList(requests)).get();
//
//        subscription.deleteMonitoredItems(items);
//    }
//
//    private void subscribe(int node_id, String tag) throws ExecutionException, InterruptedException {
//
//        UaSubscription subscription = client.getSubscriptionManager().createSubscription(300.0, UInteger.valueOf(100), UInteger.valueOf(100), UInteger.valueOf(100), true, null).get();
//
////        System.out.println("# subscribe tag : " + tag);
//
//        NodeId nodeId = new NodeId(node_id, tag);
//        ReadValueId readValueId = new ReadValueId(nodeId, AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE);
//
//        MonitoringParameters parameters = new MonitoringParameters(
//                uint(1),    // client handle
//                300.0,     // sampling interval
//                null,       // no (default) filter
//                uint(20),   // queue size
//                false);      // discard oldest
//
//        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(readValueId, MonitoringMode.Reporting, parameters);
//
//        List<UaMonitoredItem> items = subscription
//                .createMonitoredItems(TimestampsToReturn.Server, newArrayList(request)).get();
//
//        for (UaMonitoredItem item : items) {
//
//            item.setValueConsumer(handler -> {
//
////                System.out.println("data : " + handler.toString());
////                System.out.println("tag : " + item.getReadValueId().getNodeId().getIdentifier());
////                System.out.println("value : " + handler.getValue().getValue());
//
//                if (isCollection && handler.getValue().getValue()!=null)
//                    vertx.eventBus().send(recvEventbus
//                            , new JsonObject().put("id", item.getReadValueId().getNodeId().getIdentifier().toString())
//                                    .put("v", handler.getValue().getValue())
//                    );
//
//            });
//        }
//    }
//
//
//    public void stopSubscribe() throws Exception {
//        isCollection = false;
//    }
//
//    public void startSubscribe() throws Exception {
//        isCollection = true;
//    }
//}
