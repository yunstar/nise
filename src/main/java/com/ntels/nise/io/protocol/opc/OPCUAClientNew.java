//package com.ntels.nise.io.protocol.opc;
//
//import java.io.IOException;
//import java.net.UnknownHostException;
//import java.util.Locale;
//import java.util.concurrent.ExecutionException;
//
//import org.opcfoundation.ua.builtintypes.DataValue;
//import org.opcfoundation.ua.builtintypes.LocalizedText;
//import org.opcfoundation.ua.builtintypes.NodeId;
//import org.opcfoundation.ua.builtintypes.UnsignedInteger;
//import org.opcfoundation.ua.core.ApplicationDescription;
//import org.opcfoundation.ua.core.ApplicationType;
//import org.opcfoundation.ua.core.Attributes;
//import org.opcfoundation.ua.core.MonitoringMode;
//import org.opcfoundation.ua.transport.security.SecurityMode;
//import org.opcfoundation.ua.utils.MultiDimensionArrayUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ntels.nise.common.def.ErrorCode;
//import com.prosysopc.ua.ApplicationIdentity;
//import com.prosysopc.ua.SecureIdentityException;
//import com.prosysopc.ua.ServiceException;
//import com.prosysopc.ua.StatusException;
//import com.prosysopc.ua.client.*;
//import com.prosysopc.ua.nodes.UaDataType;
//import com.prosysopc.ua.nodes.UaNode;
//import com.prosysopc.ua.nodes.UaVariable;
//
//import io.vertx.core.Vertx;
//import io.vertx.core.json.JsonArray;
//import io.vertx.core.json.JsonObject;
//
///**
// * Created by youngdong on 2016-09-06.
// */
//public class OPCUAClientNew {
//
//    private final Logger logger = LoggerFactory.getLogger(OPCUAClientNew.class);
//    private Vertx vertx;
//    private JsonObject config;
//    private String sendEventbus, recvEventbus;
//    private UaClient client;
//    private boolean isCollection = true;
//    private JsonArray tag_list = new JsonArray();
//    private MonitoredDataItemListener dataChangeListener;
//
//    /* this variable selects the possible data that can be obtained from the node.
//	In this case, the parameter "value" of the node (integer number 13 in the
//	node attributes list).*/
//    private UnsignedInteger attributeId = UnsignedInteger.valueOf(13);
//
//    // allow to monitor variables that are changing in the server
//    private Subscription subscription = new Subscription();
//
//
//    public OPCUAClientNew(Vertx vertx, JsonObject config, String sendEventbus, String recvEventbus)
//    {
//        this.vertx = vertx;
//        this.config = config;
//        this.sendEventbus = sendEventbus;
//        this.recvEventbus = recvEventbus;
//        init();
//    }
//
//
//    private void init()
//    {
//
//        logger.info("----------------------------------------------------------");
//        logger.info("OPC UA client");
//        logger.info("----------------------------------------------------------");
//        logger.info("endpoint : " + config.getString("endpoint"));
//        logger.info("----------------------------------------------------------");
//
//        try {
//
//            // initiate the connection to the server
//            client = new UaClient(config.getString("endpoint"));
//
//            // define the security level in the OPC UA binary communications
//            client.setSecurityMode(SecurityMode.NONE);
//
//            // auto reconnect
//            client.setAutoReconnect(true);
//
//            // create an application instance certificate
//            initialize(client);
//
//            client.connect();
//
//            writeConsumer();
//
//            // define the corresponding listener that monitors and print value changes on items
//            dataChangeListener = new MonitoredDataItemListener() {
//                @Override
//                public void onDataChange(MonitoredDataItem sender, DataValue prevValue, DataValue value) {
//                    if(isCollection && value.getValue().getValue() != null)
//                    {
//                        if (value.getValue().isArray())
//                        {
//                            vertx.eventBus().send(recvEventbus
//                                    , new JsonObject()
//                                            .put("id", sender.getNodeId().getValue().toString())
//                                            .put("v", MultiDimensionArrayUtils.toString(value.getValue().getValue())));
//                        }else{
//                            vertx.eventBus().send(recvEventbus
//                                    , new JsonObject()
//                                            .put("id", sender.getNodeId().getValue().toString())
//                                            .put("v", value.getValue().getValue()));
//                        }
//                    }
//                }
//            };
//
//        } catch (Exception e) {
//            logger.error("### failed to connect to opc ua : " + e);
//
//            vertx.setTimer(3000, handler -> {
//                logger.info("try again connect....");
//                init();
//            });
//
//        }
//    }
//
//
//
//    // define some characteristics of the OPC UA Client application
//    public static void initialize(UaClient client) throws SecureIdentityException, IOException, UnknownHostException {
//        // create an Application Description which is sent to the server
//        ApplicationDescription appDescription = new ApplicationDescription();
//        appDescription.setApplicationName(new LocalizedText("OpcuaClient", Locale.ENGLISH));
//		/* 'localhost' (all lower case) in the ApplicationName and ApplicationURI
//		is converted to the actual host name of the computer in which the application
//		is run.*/
//        // ApplicationUri is a unique identifier for each running instance
//        appDescription.setApplicationUri("urn:localhost:UA:OpcuaClient");
//        // identify the product and should therefore be the same for all instances
//        appDescription.setProductUri("urn:prosysopc.com:UA:OpcuaClient");
//        // define the type of application
//        appDescription.setApplicationType(ApplicationType.Client);
//
//        // define the client application certificate
//        final ApplicationIdentity identity = new ApplicationIdentity();
//        identity.setApplicationDescription(appDescription);
//        // assign the identity to the Client
//        client.setApplicationIdentity(identity);
//    }
//
//
//
//    public void writeConsumer() throws Exception {
//
//        vertx.eventBus().consumer(sendEventbus, result -> {
//            JsonObject send_object = (JsonObject) result.body();
//
//            try {
//
//                if(!client.isConnected())
//                {
//                    logger.warn("## OPC ua client reconnect !! ");
//                    client.connect();
//                }
//
//                NodeId nodeid = new NodeId(2, send_object.getString("id"));
//                UaNode node = client.getAddressSpace().getNode(nodeid);
//
//                /* find the DataType if setting Value - for other properties you must
//                find the correct data type yourself */
//                UaDataType dataType = null;
//                if (attributeId.equals(Attributes.Value) && (node instanceof UaVariable))
//                {
//                    UaVariable v = (UaVariable) node;
//                    // initialize DataType node, if it is not initialized yet
//                    if (v.getDataType() == null)
//                        v.setDataType(client.getAddressSpace().getType(v.getDataTypeId()));
//                    dataType = (UaDataType) v.getDataType();
//
//                    // define the value for the selected node.
//                    // In this case, a boolean value
//                    String value = String.valueOf(send_object.getValue("v"));
//                    Object convertedValue = dataType != null
//                            ? client.getAddressSpace().getDataTypeConverter().parseVariant(value, dataType) : value;
//
//                    if(!client.writeAttribute(nodeid, attributeId, convertedValue))
//                        logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString());
//                }
//
//            } catch (AddressSpaceException e) {
//                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
//                result.fail(500, e.getMessage());
//                return;
//            } catch (ServiceException e) {
//                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
//                result.fail(500, e.getMessage());
//                return;
//            } catch (StatusException e) {
//                logger.error("NISE-000021 failed to publish to gateway : " + send_object.toString() + "\n" + e.getMessage());
//                result.fail(500, e.getMessage());
//                return;
//            }
//            result.reply(ErrorCode.OK);
//        });
//
//    }
//
//
//    public void updateTagList(JsonArray tag_list)
//    {
//        try {
//              client.removeSubscription(subscription);
//        } catch (Exception e) {
//            logger.error("failed to remove all (OPC)subscription");
//        }
//        this.tag_list.clear();
//        this.tag_list.addAll(tag_list);
//    }
//
//    public void createMonitoring() throws ExecutionException, InterruptedException
//    {
//        System.out.println("---- createMonitoring size : " + tag_list.size() );
//
//        for (int i = 0; i < tag_list.size(); i++) {
//            String tag = tag_list.getString(i);
//
//            // OPC UA subscribe (a tag)
//            try{
//                // ACS나 AMR의 경우는 OPC 수집방식이 아니기 때문에 예외처리 고정
//                // SIG 는 내부 제어용으로 사용
//                if(tag.startsWith("ACS.") || tag.startsWith("AMR.") || tag.startsWith("SIG."))
//                   continue;
//
//                // include a number of monitored items, which you listen to
//                MonitoredDataItem item = new MonitoredDataItem(new NodeId(2, tag), attributeId, MonitoringMode.Reporting);
//                // add the monitored item to the subscription
//                subscription.addItem(item);
//                // establish a listener for each item
//                item.setDataChangeListener(dataChangeListener);
//
//            } catch (ServiceException e) {
//                logger.error("NISE-000044 failed to subscribe : " + tag + "\n" + e.getMessage());
//            } catch (StatusException e) {
//                logger.error("NISE-000044 failed to subscribe : " + tag + "\n" + e.getMessage());
//            }
//        }
//
//        try {
//            client.addSubscription(subscription);
//        } catch (ServiceException e) {
//            logger.error("NISE-000044 failed to subscribe : " + e.getMessage());
//        } catch (StatusException e) {
//            logger.error("NISE-000044 failed to subscribe : " + e.getMessage());
//        }
//    }
//
//
//    public void disconnect() throws ServiceException {
//        // remove the subscription from the client
//        client.removeSubscription(subscription);
//        // disconnect from the server
//        client.disconnect();
//    }
//
//    public void stopSubscribe() throws Exception {
//        isCollection = false;
//    }
//
//    public void startSubscribe() throws Exception {
//        isCollection = true;
//    }
//}
