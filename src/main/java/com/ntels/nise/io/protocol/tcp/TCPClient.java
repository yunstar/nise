package com.ntels.nise.io.protocol.tcp;

import com.ntels.nise.common.util.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by youngdong on 2016-07-29.
 */
public class TCPClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Vertx vertx;
    private JsonObject config;
    private NetClient client;
    private String sendEventbus, recvEventbus;

    public TCPClient(Vertx vertx, JsonObject config, String sendEventbus, String recvEventbus)
    {
        this.vertx = vertx;
        this.config = config;
        this.sendEventbus = sendEventbus;
        this.recvEventbus = recvEventbus;
        init();
    }


    private void init()
    {
        logger.info("----------------------------------------------------------");
        logger.info("TCP client connection info");
        logger.info("----------------------------------------------------------");
        logger.info("ip : " + config.getString("ip"));
        logger.info("port : " + config.getInteger("port"));

        NetClientOptions options = new NetClientOptions();
        if(checkConfigInt("connect_timeout")) {
            logger.info("connect_timeout : " + config.getInteger("connect_timeout"));
            options.setConnectTimeout(config.getInteger("connect_timeout"));
        }
        if(checkConfigInt("reconnect_attempts")) {
            logger.info("reconnect_attempts : " + config.getInteger("reconnect_attempts"));
            options.setReconnectAttempts(config.getInteger("reconnect_attempts"));
        }
        if(checkConfigInt("reconnect_interval")) {
            logger.info("reconnect_interval : " + config.getInteger("reconnect_interval"));
            options.setReconnectInterval(config.getInteger("reconnect_interval"));
        }
        if(checkConfigBoolean("ssl"))
        {
            logger.info("ssl : true");
            options.setSsl(true);
            try {
                if (checkConfigString("trust_store_type")) {
                    switch (config.getString("trust_store_type").toLowerCase()) {
                        case "jks":
                            logger.info("jsk - path : " + Utils.replaceEnv(config.getString("path")));
                            logger.info("password : " + config.getString("password"));
                            options.setTrustStoreOptions(
                                    new JksOptions().
                                            setPath(Utils.replaceEnv(config.getString("path"))).
                                            setPassword(config.getString("password"))
                            );
                            break;
                        case "pem":
                            logger.info("pem - path:" + Utils.replaceEnv(config.getString("path")));
                            options.setPemTrustOptions(
                                    new PemTrustOptions().
                                            addCertPath(Utils.replaceEnv(config.getString("path")))
                            );
                            break;
                        default:
                            logger.info("trust all");
                            options.setTrustAll(true);
                            break;
                    }
                }else
                    options.setTrustAll(true);
            }catch(Exception e){
                logger.error("NISE-000036 TCP ssl설정 오류");
                options.setTrustAll(true);
            }
        }
        logger.info("----------------------------------------------------------");
        client = vertx.createNetClient(options);
        connect();
    }




    private void connect()
    {
        client.connect(config.getInteger("port"), config.getString("ip"), res -> {
            if (res.succeeded()) {
                logger.info("tcp connected! -> " + config.getString("ip") + ":" + config.getInteger("port"));
                NetSocket socket = res.result();

                // receive
                socket.handler( buffer -> {
                    if(recvEventbus!=null)
                        vertx.eventBus().send(recvEventbus, buffer.toString("UTF-8"));
                });

                // send
                vertx.eventBus().consumer(sendEventbus, rs_msg -> {
                    socket.write((String)rs_msg.body());
                });

                socket.exceptionHandler(handler -> {
                    logger.error( "NISE-000025 tcp 소켓오류 : " + handler.getMessage() );
                });

                socket.closeHandler(handler -> {
                    logger.error("NISE-000023 tcp connection lost -> " + config.getString("ip") + ":" + config.getInteger("port"));
                    connect();
                });
            } else {
                logger.error("NISE-000024 Failed to connect -> " + config.getString("ip") + ":" + config.getInteger("port") + "  " + res.cause().getMessage());
                // 접속 실패의 경우 5초뒤 재접속
                vertx.setTimer(1000 * 5, handler -> {
                    connect();
                });
            }
        });
    }


    private boolean checkConfigString(String key)
    {
        return (config.containsKey(key) && config.getString(key)!=null);
    }

    private boolean checkConfigBoolean(String key)
    {
        try{
            return config.getBoolean(key);
        }catch (Exception e){
            return false;
        }
    }

    private boolean checkConfigInt(String key)
    {
        try{
            return (config.containsKey(key) && config.getInteger(key) > 0);
        }catch (Exception e){
            return false;
        }
    }
}
