package com.ntels.nise.io.router;


import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.io.protocol.opc.OPCUAClient;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;

/**
 * Created by youngdong on 2016-09-06.
 */
public class OPCUARouter extends BaseVerticle {

    private OPCUAClient opc_client = null;

    @Override
    public void onInit() {
        super.onInit();


        // 1. init OPC UA
        vertx.eventBus().consumer(EventBusAddress.IN_GATEWAY_OPCUA_SUBSCRIBE.toString(), tag_list -> {
            JsonArray tags = (JsonArray)tag_list.body();

            try {

                if(opc_client==null)
                {
                    JsonObject config = config().getJsonObject("opc.ua");

                    opc_client = new OPCUAClient(vertx
                            , config
                            , EventBusAddress.OUT_GATEWAY_OPCUA.toString()
                            , EventBusAddress.IN_GATEWAY_OPCUA.toString()
                            ,tags);
                }else{

                    opc_client.updateTagList(tags);
                }

//                opc_client.createMonitoring();

            }catch (Exception e){
                logger.error("failed to subscribe OPC UA");
                logger.info("try again subscribe....");
                vertx.setTimer(3000, handler -> {
                    vertx.eventBus().send(EventBusAddress.IN_GATEWAY_OPCUA_SUBSCRIBE.toString(), tags);
                });
            }

//            for (int i = 0; i < tags.size(); i++) {
//                String tag = tags.getString(i);
//                try {
//                    // OPC UA subscribe (a tag)
//                    opc_client.subscribe(2, tag);
//                } catch (Exception e) {
//                    logger.error("## failed to opc ua '" + tag + "' subscribe : " + e.getMessage());
//                }
//            }
        });


        // 2. report on/off control
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_GW_REPORT_ON_OFF.toString(), rs_on_off -> {
            String on_off = (String)rs_on_off.body();
            switch (on_off){
                case "on":
                    report_on();
                    break;
                case "off":
                    report_off();
                    break;
                default:
                    logger.error("NISE-000032 "+on_off+" is undefined type");
            }
        });


    }



    private void report_on()
    {
        try {
            opc_client.startSubscribe();
        } catch (Exception e) {
            logger.error("NISE-000031 failed to collect gateway data : " + e.getMessage());
        }
    }

    private void report_off()
    {
        try {
            opc_client.stopSubscribe();
        } catch (Exception e) {
            logger.error("NISE-000031 failed to stop gateway data : " + e.getMessage());
        }
    }

}
