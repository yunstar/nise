package com.ntels.nise.io.router;

import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.io.protocol.mqtt.MQTTClient;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;

import java.util.Base64;

/**
 * Created by youngdong on 2016-07-08.
 */
public class MqttMASOutRouter extends BaseVerticle {


    private JsonObject config;
    private MQTTClient mqtt_client=null, mqtt_new_client=null;
//    private boolean isRetrieve = true;
    private String topic;

    @Override
    public void onInit() {
        super.onInit();
        initRoute();
    }


    private void initRoute()
    {
        vertx.sharedData().getClusterWideMap("mqtt.status", result -> {
            result.result().put("status", true, h2 -> {});
        });

        config = config().getJsonObject("mas.mqtt.pub");


        String access_account = config.getString("mf_id")
                + "." + config.getString("model_id")
                + "." + config.getString("serial_no");
        config.put("user", access_account);
        config.put("passwd", access_account);
        config.put("clientId", access_account + "_p");
        topic = "/" + access_account + "/publish";


        logger.info("------------------------------------------------");
        logger.info("MQTT Connect MAS Out (retrieve)");
        logger.info("------------------------------------------------");
        logger.info("URL       : " + config.getString("broker_url"));
        logger.info("User      : " + config.getString("user"));
        logger.info("passwd    : " + config.getString("passwd"));
        logger.info("clientId  : " + config.getString("clientId"));
        logger.info("qos       : " + config.getInteger("qos"));
        logger.info("topic     : " + topic);
        logger.info("------------------------------------------------");


        mqtt_client = MQTTClient.create(vertx, config);

        // 1. 디바이스 조회된 경우 ID,Password 재접속
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_MAS_CONNECTION_READY.toString(), onReconnect());


        // 2. 디바이스 조회 요청
        mqtt_client.start(result -> {
            vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_PROFILE_RETRIEVE.toString(), onRtrieveDevice());
        });


        // 3. 디바이스 종료된 경우 ID접속 재시도
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_MAS_MQTT_CONNECT_RESET.toString(), result_device -> {
//            logger.info("reset mqtt connection (retrieve)");
//            isRetrieve = true;
            vertx.sharedData().getClusterWideMap("mqtt.status", result -> {
                result.result().put("status", true, h2 -> {});
            });
        });
    }


    /**
     * MAS로부터 Device 조회 된 경우 Device_id로 재접속 시도
     * @return
     */
    private Handler<Message<JsonObject>> onReconnect()
    {
        return result_device -> {

//            isRetrieve = false;
            vertx.sharedData().getClusterWideMap("mqtt.status", result -> {
                result.result().put("status", false, h2 -> {});
            });

            logger.info("try again MAS mqtt connection");

            JsonObject device_profile = (JsonObject)result_device.body();

            if(mqtt_new_client!=null)
            {
                mqtt_new_client.closeClient();
                mqtt_new_client = null;
            }

            // 1) re connect by ID,Key
            config.put("clientId", device_profile.getString("device_id") + "_p");
            config.put("user", device_profile.getString("device_id"));
            config.put("passwd", device_profile.getString("device_key"));
            String new_topic = "/" + device_profile.getString("device_id") + "/publish";

            logger.info("------------------------------------------------");
            logger.info("MQTT Connect MAS Out");
            logger.info("------------------------------------------------");
            logger.info("URL       : " + config.getString("broker_url"));
            logger.info("User      : " + config.getString("user"));
            logger.info("passwd    : " + config.getString("passwd"));
            logger.info("clientId  : " + config.getString("clientId"));
            logger.info("qos       : " + config.getInteger("qos"));
            logger.info("topic     : " + new_topic);
            logger.info("------------------------------------------------");

            // 2017-10-17 Basic token 저장 device_id 및 device_key Cache에 저장 (IEQ API 연동시 인증 토큰으로 사용하기 위함)
            JsonObject device_info = new JsonObject();
            device_info.put("map", "token");
            device_info.put("key", "basic");
            device_info.put("value", createToken(device_profile.getString("device_id"), device_profile.getString("device_key")));
            vertx.eventBus().send(EventBusAddress.CACHE_PUT.toString(), device_info, result -> {
                if(result.failed())
                    logger.error("failed to create Token\n" + result.cause().getMessage());
            });

            mqtt_new_client = MQTTClient.create(vertx, config);
            // 2) send data to MAS
            mqtt_new_client.start(new_result -> {
                vertx.eventBus().publish(EventBusAddress.SIG_DEVICE_MAS_PUBLISH_READY.toString(), "");
                vertx.eventBus().consumer(EventBusAddress.OUT_MAS_MQTT.toString(), send_message -> {
                    try {
                        JsonObject send_object = (JsonObject) send_message.body();

                        // devicd_id는 MAS에서 필수요소로 매번 cache를 호출하지않고 보내기 전에 넣어줌
                        if (send_object.containsKey("device_id")) send_object.put("device_id", device_profile.getString("device_id"));
                        if (send_object.containsKey("device_key")) send_object.put("device_key", device_profile.getString("device_key"));
                        logger.debug("# MAS out : " + send_object.toString());

                        mqtt_new_client.publish(new_topic, () -> send_object.toString().getBytes(), pub_result -> {
                            if (pub_result.failed()) {
//                                logger.error(pub_result.cause().getMessage());
                                vertx.eventBus().publish(EventBusAddress.SIG_DEVICE_MAS_MQTT_CONNECT_RESET.toString(), "");
                            }
                        });
                        send_message.reply(ErrorCode.OK);
                    }catch(Exception e){
                        logger.error(e.getMessage());
                        send_message.reply(ErrorCode.BAD_REQUEST);
                    }
                });

            });

        };
    }




    private Handler<Message<String>> onRtrieveDevice()
    {
        return resultRetrieve -> {


            vertx.sharedData().getClusterWideMap("mqtt.status", (AsyncResult<AsyncMap<String,Boolean>> result)  -> {
                AsyncMap<String, Boolean> asyncMap = result.result();
                asyncMap.get("status", rs_status -> {
                    if( rs_status.result() )
                    {
                        logger.info("retrieve profile of this device");
                        JsonObject message = new JsonObject();
                        message.put("message_type", "device.profile.retrieve");
                        message.put("device_mf_id", config.getString("mf_id"));
                        message.put("device_model_id", config.getString("model_id"));
                        message.put("device_sn", config.getString("serial_no"));

                        mqtt_client.publish(topic, () -> message.toString().getBytes(), pub_result -> {
                            if (pub_result.failed()) {
                                logger.warn("NISE-000030 failed to retrieve device : " + pub_result.cause().getMessage());
                            } else {
                                logger.debug("retrieve device succeeded");
                            }
                        });
                    }
                });
            });


//            if(isRetrieve) {
//                logger.info("retrieve profile of this device");
//                JsonObject message = new JsonObject();
//                message.put("message_type", "device.profile.retrieve");
//                message.put("device_mf_id", config.getString("mf_id"));
//                message.put("device_model_id", config.getString("model_id"));
//                message.put("device_sn", config.getString("serial_no"));
//
//                mqtt_client.publish(topic, () -> message.toString().getBytes(), pub_result -> {
//                    if (pub_result.failed()) {
//                        logger.warn("NISE-000030 failed to retrieve device : " + pub_result.cause().getMessage());
//                    } else {
//                        logger.debug("retrieve device succeeded");
//                    }
//                });
//            }
        };
    }


    /**
     * Basic token 생성
     * @param device_id
     * @param device_key
     * @return
     */
    private String createToken(String device_id, String device_key)
    {
        byte[] authorization = (device_id + ":" + device_key).getBytes();
        String encAuth = Base64.getEncoder().encodeToString(authorization);

        return encAuth;
    }

}
