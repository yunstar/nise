package com.ntels.nise.io.router;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.io.protocol.mqtt.MQTTClient;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.json.JsonObject;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Created by youngdong on 2016-07-08.
 */
public class MqttGatewayInRouter extends BaseVerticle {


    @Override
    public void onInit() {
        super.onInit();

        JsonObject config = config().getJsonObject("gw.mqtt.sub");
        MQTTClient mqtt_client = MQTTClient.create(vertx, config);


        logger.info("------------------------------------------------");
        logger.info("MQTT Connect Gateway In");
        logger.info("------------------------------------------------");
        logger.info("URL       : " + config.getString("broker_url"));
        logger.info("User      : " + config.getString("user"));
        logger.info("passwd    : " + config.getString("passwd"));
        logger.info("clientId  : " + config.getString("clientId"));
        logger.info("qos       : " + config.getInteger("qos"));
        logger.info("topic     : " + config.getString("topic"));
        logger.info("------------------------------------------------");


        // 1. Mqtt connection
        mqtt_client.start(connect_result -> {
            // 2. receive node data (default on)
            report_on(mqtt_client, config.getString("topic"));

            // 3. report on/off control
            vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_GW_REPORT_ON_OFF.toString(), rs_on_off -> {
                String on_off = (String)rs_on_off.body();
                switch (on_off){
                    case "on":
                        report_on(mqtt_client, config.getString("topic"));
                        break;
                    case "off":
                        report_off(mqtt_client, config.getString("topic"));
                        break;
                    default:
                        logger.error("NISE-000032 "+on_off+" is undefined type");
                }
            });

        });
    }



    private void report_on(MQTTClient mqtt_client, String topic)
    {
        mqtt_client.subscribe(topic, recv_data -> {
//            System.out.println("recv data : " + new String(recv_data.getPayload()) );
            vertx.eventBus().send(EventBusAddress.IN_GATEWAY_MQTT.toString() , new JsonObject(new String(recv_data.getPayload())) );
        });
    }

    private void report_off(MQTTClient mqtt_client, String topic)
    {
        try {
            mqtt_client.unSubscribe(topic);
        } catch (MqttException e) {
            logger.error("NISE-000031 failed to collect gateway data : " + e.getMessage());
        }
    }

}
