package com.ntels.nise.io.router;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.io.protocol.mqtt.MQTTClient;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;

/**
 * Created by youngdong on 2016-07-08.
 */
public class MqttMASInRouter extends BaseVerticle {


    private MQTTClient mqtt_new_client = null;
    private String new_topic;

    @Override
    public void onInit() {
        super.onInit();
        initRoute();
    }



    private void initRoute()
    {

        // 0. MQTT 재접속 가능한 상태값 설정
        vertx.sharedData().getClusterWideMap("mqtt.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
            AsyncMap<String, Long> asyncMap = result.result();
            asyncMap.put("status", 0L, h -> {});
        });


        JsonObject config = config().getJsonObject("mas.mqtt.sub");
        String access_account = config.getString("mf_id")
                + "." + config.getString("model_id")
                + "." + config.getString("serial_no");
        config.put("user", access_account);
        config.put("passwd", access_account);
        config.put("clientId", access_account + "_s");
        String topic = "/" + access_account + "/subscribe";


        logger.info("------------------------------------------------");
        logger.info("MQTT Connect MAS In (retrieve)");
        logger.info("------------------------------------------------");
        logger.info("URL       : " + config.getString("broker_url"));
        logger.info("User      : " + config.getString("user"));
        logger.info("passwd    : " + config.getString("passwd"));
        logger.info("clientId  : " + config.getString("clientId"));
        logger.info("qos       : " + config.getInteger("qos"));
        logger.info("topic     : " + topic);
        logger.info("------------------------------------------------");


        MQTTClient mqtt_client = MQTTClient.create(vertx, config);

        // 1. add Subscriber
        mqtt_client.start(result -> {

            mqtt_client.subscribe(topic, mqtt_message -> {

                JsonObject message = new JsonObject(new String(mqtt_message.getPayload()));
                logger.debug("it's retrieved device (devicd_id : "+message.getString("device_id")+")");


                if(mqtt_new_client!=null)
                {
                    logger.debug("close old mqtt connection");
                    mqtt_new_client.closeClient(new_topic);
                    mqtt_new_client = null;
                }

                // 2. reconnect by ID,Key
                config.put("clientId", message.getString("device_id") + "_s");
                config.put("user", message.getString("device_id"));
                config.put("passwd", message.getString("device_key"));
                new_topic = "/" + message.getString("device_id") + "/subscribe";

                logger.info("------------------------------------------------");
                logger.info("MQTT Connect MAS In");
                logger.info("------------------------------------------------");
                logger.info("URL       : " + config.getString("broker_url"));
                logger.info("User      : " + config.getString("user"));
                logger.info("passwd    : " + config.getString("passwd"));
                logger.info("clientId  : " + config.getString("clientId"));
                logger.info("qos       : " + config.getInteger("qos"));
                logger.info("topic     : " + new_topic);
                logger.info("------------------------------------------------");


                mqtt_new_client = MQTTClient.create(vertx, config);

                // 3. add new subscriber
                mqtt_new_client.start(new_result -> {
                    // retrieve success
                    vertx.eventBus().publish(EventBusAddress.SIG_DEVICE_MAS_CONNECTION_READY.toString(), message);
                    Utils.createFile(Utils.getHome() + "/mas.ok");    // Application(myself) 원격 업데이트 후 정상 접속되었다는 signal

                    mqtt_new_client.subscribe(new_topic, new_mqtt_message -> {
                        JsonObject new_message = new JsonObject(new String(new_mqtt_message.getPayload()));

                        // 4. send data to eventbus
                        logger.debug("# MAS in message_type : " + new_message.getString("message_type"));
                        vertx.eventBus().send("in.mqtt.mas." + new_message.getString("message_type"), new_message);

                    });

                });

            });

        });
    }

}
