package com.ntels.nise.io.router;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by youngdong on 2017-10-17.
 */
public class RESTIEQInRouter extends BaseVerticle {

    @Override
    public void start() throws Exception {
        super.start();

        vertx.setTimer(200000, h -> {
            startRESTServer();
        });
    }


    private void routeAPI(Router router)
    {
        // test
//        router.route(HttpMethod.GET, "/test").handler(handler -> {
//            String res = "good";
//            handler.response().putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(res.getBytes().length));
//            handler.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
//            handler.response().write( res ).end();
//        });
        
        // IEQ Sensor 데이터 전송 (POST방식 오류로 GET으로 변경)
        router.route(HttpMethod.GET, "/contents").handler(handler -> {
            JsonObject params = getParams(handler);
//            logger.info("## IEQ Data : " + params.toString());
            if(checkParams(params, handler, "mf_id", "model_id", "sn"))
                vertx.eventBus().send(EventBusAddress.COMP_COLLECT_IEQ.toString(), params, this.onResponse(handler.response()));
        });

        
        // IEQ Sensor 데이터 전송
        router.route(HttpMethod.POST, "/contents").handler(handler -> {
            JsonObject params = handler.getBodyAsJson();
//            logger.info("## IEQ Data : " + params.toString());
            if(checkParams(params, handler, "mf_id", "model_id", "sn"))
                vertx.eventBus().send(EventBusAddress.COMP_COLLECT_IEQ.toString(), params, this.onResponse(handler.response()));

        });
    }


    /**
     * Rest Server 실행
     */
    private void startRESTServer()
    {
        
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        JsonObject config = config().getJsonObject("ieq.rest.server");
        int port = config.getInteger("port", 10002);
        boolean is_ssl = config.getBoolean("ssl", false);
        

        logger.info("------------------------------------------------");
        logger.info("IEQ Server Router (REST)");
        logger.info("------------------------------------------------");
        logger.info("Port      : " + port);
        logger.info("SSL       : " + is_ssl);
        logger.info("------------------------------------------------");

        // Authentication
        router.route("/*").handler(handler -> {
            
            // token 발급 API를 제외하고 인증체크
            if( !handler.request().uri().startsWith("/oauth") )
            {
                auth(handler.request().getHeader(HttpHeaders.AUTHORIZATION), auth -> {
                    if(auth.booleanValue())
                    {
                        handler.next();
                    }else{
                        logger.info("## Token : " + handler.request().getHeader(HttpHeaders.AUTHORIZATION));
                        HttpServerResponse response = handler.response();
                        response.setStatusCode(401).end();
                    }
                });
            }else{
                handler.next();
            }
            
        });

        // Router 설정
        routeAPI(router);
        
        if(is_ssl)
        {
            HttpServerOptions httpOpts = new HttpServerOptions();
            httpOpts.setKeyStoreOptions(new JksOptions()
                    .setPassword(config.getString("certificate-password"))
                    .setPath(config.getString("certificate-path")));
            httpOpts.setSsl(true);
//            String[] ciphers = new String[]{"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", "TLS_RSA_WITH_AES_128_CBC_SHA256", "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", "TLS_RSA_WITH_AES_128_CBC_SHA", "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", "TLS_DHE_RSA_WITH_AES_128_CBC_SHA", "TLS_DHE_DSS_WITH_AES_128_CBC_SHA", "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", "SSL_RSA_WITH_3DES_EDE_CBC_SHA", "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"};
//            for (String cipher : ciphers) {
//                httpOpts.addEnabledCipherSuite(cipher);
//            }
            
            vertx.createHttpServer(httpOpts).requestHandler(router::accept).listen(port);
            
        }else{
            vertx.createHttpServer().requestHandler(router::accept).listen(port);
        }
    }


    /**
     * Token 인증
     * @param token
     * @param handler
     */
    private void auth(String token, Handler<Boolean> handler)
    {
        try {

            JsonObject token_cache = new JsonObject();
            token_cache.put("map", "token");
            token_cache.put("key", "basic");
            vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), token_cache, res_token -> {
                boolean succeeded = false;
                if(res_token.succeeded())
                {
                    succeeded = token.replace("Basic ", "").equals((String)res_token.result().body());

                }else{
                    logger.error("No token\n" + res_token.cause().getMessage());
                }

                handler.handle(succeeded);
            });

        }catch (Exception e){
            logger.error(e.getMessage());
            handler.handle(false);
        }
    }



    /**
     * checking mandatory params
     * @param handler
     * @return
     */
    protected boolean checkParams(JsonObject params, RoutingContext handler, String... param_names)
    {
        boolean result = true;
        StringBuilder sb = new StringBuilder();
        for(String param : param_names)
        {
            if(!params.containsKey(param) || params.getValue(param)==null || params.getString(param).length()==0)
            {
                if(sb.length()==0)
                    sb.append(param);
                else
                    sb.append(", ").append(param);
                result = false;
            }
        }

        // 유효성 검사가 실패한 경우
        if(!result)
            handler.response().setStatusCode(400).setStatusMessage("missing parameter ("+sb.toString()+")").end();

        return result;
    }


    /**
     * REST 결과 전송
     * @return
     */
    protected Handler<AsyncResult<Message<JsonObject>>> onResponse(HttpServerResponse response)
    {
        return result -> {
            if(result.succeeded())
            {
//                System.out.println(result.result().body().encodePrettily());
                String res = result.result().body().encode();
                if(res.equals("{}")) res = "";
                response.putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(res.getBytes().length));
                response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
                response.write( res ).end();
            }else{
                logger.error("" + result.cause().getMessage());
                response.setStatusCode(((ReplyException)result.cause()).failureCode()).setStatusMessage(result.cause().getMessage()).end();
            }
        };
    }



    /**
     * parameters JsonObject 포맷으로 변경
     * @param handler
     * @return
     */
    protected JsonObject getParams(RoutingContext handler)
    {
        final MultiMap requestParams = handler.request().params();
        Iterator<Map.Entry<String, String>> result = requestParams.iterator();
        JsonObject param = new JsonObject();
        while(result.hasNext()){
            Map.Entry<String, String> e = result.next();

            String t = e.getValue();
            try {
                if (t.split("\\.").length > 1)
                {
                    param.put(e.getKey(), Double.parseDouble(t));
                }else{
                    param.put(e.getKey(), Integer.parseInt(t));
                }
            }catch(Exception ex){
                param.put(e.getKey(), e.getValue());
            }
        }

        return param;
    }
}

