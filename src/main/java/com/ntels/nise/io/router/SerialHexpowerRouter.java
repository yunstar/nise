package com.ntels.nise.io.router;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.io.protocol.rxtx.RXTXClient;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2017-11-29.
 */
public class SerialHexpowerRouter extends BaseVerticle {

    @Override
    public void start() throws Exception {
        super.start();

        // Serial Router 실행
        vertx.eventBus().consumer(EventBusAddress.SIG_START_HEX_ROUTER.toString(), onStartRouter());
    }


    /**
     * Serial Master 등록
     * @return
     */
    private Handler<Message<JsonObject>> onStartRouter()
    {
        return result -> {
            RXTXClient rxtx = new RXTXClient(vertx, config().getJsonObject("hexpower.serial.master"), EventBusAddress.IN_HEXP_SERIAL.toString());

            byte[] fault_frame         = {0x05, 0x00, 0x00, 0x52, 0x30, 0x30, 0x30, 0x34, 0x30, 0x34, 0x00, 0x00, 0x00, 0x00, 0x04};
            byte[] solar_voltage_frame = {0x05, 0x00, 0x00, 0x52, 0x30, 0x30, 0x32, 0x30, 0x30, 0x32, 0x00, 0x00, 0x00, 0x00, 0x04};
            byte[] util_voltage_frame  = {0x05, 0x00, 0x00, 0x52, 0x30, 0x30, 0x35, 0x30, 0x30, 0x37, 0x00, 0x00, 0x00, 0x00, 0x04};
            byte[] solar_inv_frame     = {0x05, 0x00, 0x00, 0x52, 0x30, 0x30, 0x36, 0x30, 0x30, 0x38, 0x00, 0x00, 0x00, 0x00, 0x04};
            byte[] env_frame           = {0x05, 0x00, 0x00, 0x52, 0x30, 0x30, 0x37, 0x30, 0x30, 0x34, 0x00, 0x00, 0x00, 0x00, 0x04};


            JsonArray arr = new JsonArray();
            arr.add(fault_frame);
            arr.add(solar_voltage_frame);
            arr.add(util_voltage_frame);
            arr.add(solar_inv_frame);
            arr.add(env_frame);

            rxtx.onSendFrame(config().getJsonObject("hexpower.serial.master").getInteger("send.interval"), arr);
        };
    }

}
