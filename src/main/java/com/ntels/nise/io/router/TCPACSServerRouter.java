package com.ntels.nise.io.router;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.io.protocol.tcp.TCPServer;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;

/**
 * Created by youngdong on 2016-08-02.
 */
public class TCPACSServerRouter extends BaseVerticle{

    @Override
    protected void onInit() {
        super.onInit();

        JsonObject config = config().getJsonObject("acs.tcp.server");


        logger.info("------------------------------------------------");
        logger.info("TCP Server (ACS)");
        logger.info("------------------------------------------------");
        logger.info("port           : " + config.getInteger("port"));
        logger.info("idle_timeout   : " + config.getInteger("idle_timeout"));
        logger.info("ssl            : " + config.getBoolean("ssl"));
        if( config.getBoolean("ssl") )
        {
            logger.info("ssl trust_store_type   : " + config.getString("trust_store_type"));
            logger.info("ssl path               : " + config.getString("path"));
            logger.info("ssl password           : " + config.getString("password"));
        }
        logger.info("tag            : " + config.getString("tag"));
        logger.info("------------------------------------------------");


        TCPServer server = new TCPServer(vertx, config
                , EventBusAddress.OUT_ACS_TCP.toString()
                , parsingHandler(config.getString("tag")) );
    }


    /**
     * TCP로 받은 패킷을 처리하는 부분
     *  1) LF or CRLF Parsing
     *  2) [{"id" : @tag, "v" : @JsonObject}, ...] 형태 JsonArray 포맷으로 변경하여 EventBus로 전달
     * @param tag
     * @return
     */
    private Handler<Buffer> parsingHandler(String tag)
    {
        return buffer -> {

            JsonArray result = new JsonArray();

            int offset = 0;
            Buffer line_buffer = Buffer.buffer();

            LocalMap<String, Buffer> tmp_map = vertx.sharedData().getLocalMap("tmp_buffer");

            Buffer tmp_buffer = tmp_map.get("data");

            if(tmp_buffer==null)
                tmp_buffer = Buffer.buffer();

            tmp_buffer.appendBuffer(buffer);

            int size = tmp_buffer.length();
            for(int i=0;i<size;i++)
            {
                if(tmp_buffer.getByte(i)=='\n' || tmp_buffer.getByte(i)=='\r' ){
                    if(i-offset > 1) {
                        JsonObject row = new JsonObject();
                        row.put("id", tag);
                        row.put("v", new JsonObject(line_buffer.toString("UTF-8")));
                        result.add(row);
                    }
                    line_buffer = Buffer.buffer();
                    offset = i;
                    continue;
                }
                line_buffer.appendByte(tmp_buffer.getByte(i));
            }

            if(result.size() > 0)
                vertx.eventBus().send(EventBusAddress.IN_ACS_TCP.toString(), result);

            tmp_buffer = Buffer.buffer();

            if(line_buffer.length() > 0)
                tmp_buffer.appendBuffer(line_buffer);

            tmp_map.put("data", tmp_buffer);

        };
    }
}
