package com.ntels.nise.io.router;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.io.protocol.ftp.FtpClient;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonObject;

import java.util.function.Consumer;

/**
 * Created by youngdong on 2016-07-18.
 */
public class FTPRouter extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();

        // file download
        vertx.eventBus().consumer(EventBusAddress.FTP_DOWNLOAD.toString(), onDownload());

    }




    private Handler<Message<JsonObject>> onDownload()
    {
        return result -> {
            JsonObject ftp_info = result.body();

            String host         = ftp_info.getString("ftp_ip");
            String user         = new String( java.util.Base64.getDecoder().decode(ftp_info.getString("ftp_id")) );
            String password     = new String( java.util.Base64.getDecoder().decode(ftp_info.getString("ftp_pwd")) );
            int port            = Integer.parseInt(ftp_info.getString("ftp_port"));
            String local_path   = Utils.replaceEnv(config().getString("ftp.tmp.path")) + "/"+ ftp_info.getString("file_name");
            String server_path  = ftp_info.getString("file_path") + "/" + ftp_info.getString("file_name");

            logger.info("------------------------------------------------");
            logger.info("FTP Connect ");
            logger.info("------------------------------------------------");
            logger.info("Host      : " + host);
            logger.info("User      : " + user);
            logger.info("passwd    : " + password);
            logger.info("Port      : " + port);
            logger.info("local_path    : " + local_path);
            logger.info("server_path   : " + server_path);
            logger.info("------------------------------------------------");


            login(host, port, user, password, client -> {
                vertx.fileSystem().open(local_path, new OpenOptions().setWrite(true).setTruncateExisting(true), arfile -> {
                    if (arfile.succeeded()) {
                        client.retr(server_path, arfile.result(), progress -> {
                        }, retr -> {
                            if (retr.succeeded()) {
                                ftp_info.put("work_result", "SUCCESS");
                            }else{
                                logger.error("NISE-000027 failed to download file : " + retr.cause().getMessage());
                                ftp_info.put("work_result", "FAIL");
                            }
                            sendDownloadResult(ftp_info);
                            arfile.result().close(rs_close -> {rs_close.succeeded();});
                            client.close();
                        });
                    }else{
                        logger.error("NISE-000027 failed to download file : " + arfile.cause().getMessage());
                        ftp_info.put("work_result", "FAIL");
                        sendDownloadResult(ftp_info);
                        client.close();
                    }
                });
            });
        };
    }



    private void sendDownloadResult(JsonObject reply_msg)
    {
        switch (reply_msg.getString("message_type"))
        {
            case "app":
                vertx.eventBus().send(EventBusAddress.FTP_APP_DOWNLOAD_RESULT.toString(), reply_msg);
                break;
            case "firmware":
                vertx.eventBus().send(EventBusAddress.FTP_FIRMWARE_DOWNLOAD_RESULT.toString(), reply_msg);
                break;
            default:
                logger.error("NISE-000029 message_type is undefined");
                break;

        }
    }


    private void login(String host, int port, String user, String pass, Consumer<FtpClient> handler)
    {
        FtpClient client = new FtpClient(vertx, host, port);
        client.connect(rs_connect -> {
            if (rs_connect.failed()) {
                logger.error( "NISE-000026 failed to connect ftp : " + rs_connect.cause().getMessage() );
                return;
            }
            client.login(user, pass, rs_login -> {
                if (rs_login.failed()) {
                    logger.error( "NISE-000028 failed to connect ftp : " + rs_login.cause().getMessage() );
                    return;
                }
                handler.accept(client);
            });
        });
    }

}
