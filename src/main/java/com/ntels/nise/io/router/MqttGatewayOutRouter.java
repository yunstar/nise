package com.ntels.nise.io.router;

import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.io.protocol.mqtt.MQTTClient;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-08.
 */
public class MqttGatewayOutRouter extends BaseVerticle {


    @Override
    public void onInit() {
        super.onInit();

        JsonObject config = config().getJsonObject("gw.mqtt.pub");

        logger.info("------------------------------------------------");
        logger.info("MQTT Connect Gateway Out");
        logger.info("------------------------------------------------");
        logger.info("URL       : " + config.getString("broker_url"));
        logger.info("User      : " + config.getString("user"));
        logger.info("passwd    : " + config.getString("passwd"));
        logger.info("clientId  : " + config.getString("clientId"));
        logger.info("qos       : " + config.getInteger("qos"));
        logger.info("topic     : " + config.getString("topic"));
        logger.info("------------------------------------------------");


        MQTTClient mqtt_client = MQTTClient.create(vertx, config);

        // 1. Mqtt connection
        mqtt_client.start(connect_result -> {
            // 2. send control data
            vertx.eventBus().consumer(EventBusAddress.OUT_GATEWAY_MQTT.toString(), send_message -> {

                JsonArray send_object = (JsonArray)send_message.body();

                mqtt_client.publish(config.getString("topic"), () -> send_object.toString().getBytes(), pub_result -> {
                    if (pub_result.failed()) {
                        logger.error("NISE-000021 failed to publish to gateway : " + pub_result.cause().getMessage());
                        send_message.fail(ErrorCode.BAD_REQUEST, pub_result.cause().getMessage());
                    }else
                        send_message.reply(ErrorCode.OK);
                });
            });

        });
    }


}
