package com.ntels.nise.service.control;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-21.
 */
public class DeviceControlService extends BaseVerticle{

    @Override
    protected void onInit() {
        super.onInit();
        // MAS로 부터 Device 제어명령 받아오는 부분
        vertx.eventBus().consumer(EventBusAddress.IN_MAS.DEVICE_COMMAND.toString(), this.onExecuteCommand());
    }


    private Handler<Message<JsonObject>> onExecuteCommand()
    {
        return result -> {

            JsonObject rs_cmd = result.body();

            logger.info("received control device command : " + rs_cmd.toString());

            JsonObject query = Utils.getCacheQuery(CacheAddress.DEVICEKEY_DEVICEID.toString(), rs_cmd.getString("device_key"));
            // 1. 제어명령 인증
            vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, rs_auth -> {
                if(rs_auth.succeeded())
                    // 2. 제어명령 실행
                    vertx.eventBus().send(EventBusAddress.COMP_CONTROL.toString(), rs_cmd, rs_exec -> {
                        if(rs_exec.succeeded())
                            // 3. MAS에 결과 전달
                            vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), rs_exec.result().body());
                        else {
                            if( ((ReplyException)rs_exec.cause()).failureCode() == ErrorCode.NO_DATA) {
                                // node data가 없는 상태.
                                logger.error("NO_DATA ");
                            }else {
                                logger.error(rs_exec.cause().getMessage());
                            }
                        }
                    });
                else
                    logger.error("NISE-000006 request not authorized");
            });
        };
    }
}
