package com.ntels.nise.service.control;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-21.
 */
public class NodeControlService extends BaseVerticle{

    @Override
    protected void onInit() {
        super.onInit();
        // MAS로 부터 노드 제어명령 받아오는 부분
        vertx.eventBus().consumer(EventBusAddress.IN_MAS.NODE_COMMAND.toString(), this.onExecuteCommand());
    }


    private Handler<Message<JsonObject>> onExecuteCommand()
    {
        return result -> {

            JsonObject rs_cmd = result.body();
            logger.debug("received control node command : " + rs_cmd.toString());

            // 1. 제어명령 인증
            JsonObject query = Utils.getCacheQuery(CacheAddress.DEVICEKEY_DEVICEID.toString(), rs_cmd.getString("device_key"));
            vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, rs_auth -> {
                if(rs_auth.succeeded())
                    // 1. 제어명령을 OPC 포맷으로 변경
                    vertx.eventBus().send(EventBusAddress.COMP_FORMAT_NODE_EXEC_CMD.toString(), rs_cmd, rs_exec_json -> {
                        if(rs_exec_json.succeeded())
                            // 2. OPC로 제어명령 전달
                            vertx.eventBus().send(EventBusAddress.OUT_GATEWAY_OPCUA.toString(), rs_exec_json.result().body(), rs_exec -> {
                                // TODO 3. 제어결과 받아와서 : OPC에서 제어명령 결과를 받아오는 부분 확인 후 구현 (topic값 및 포맷 확인)
                                // 4. 결과 포맷으로 변경
                                vertx.eventBus().send(EventBusAddress.COMP_FORMAT_NODE_EXEC_RES.toString(), rs_cmd, rs_exec_res -> {
                                    if(rs_exec_res.succeeded()) {
                                        JsonObject exec_result = (JsonObject) rs_exec_res.result().body();
                                        exec_result.put("command_status", rs_exec.succeeded() ? "SUCCESS" : "FAIL");
                                        // 5. MAS에 결과 전달
                                        vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), exec_result);
                                    }else {
                                        logger.error(rs_exec_res.cause().getMessage());
                                    }
                                });
                            });
                        else
                            logger.error("NISE-000005 failed to convert command to MAS format : " + rs_exec_json.cause().getMessage());
                    });
                else
                    logger.error("NISE-000006 request not authorized");
            });
        };
    }
}
