package com.ntels.nise.service.control;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;


/**
 * Created by youngdong on 2016-07-11.
 */
public class DeviceRetrieveService extends BaseVerticle {

    private int RETRIEVE_INTERVAL = 1000 * 60;

    @Override
    public void onInit() {

        RETRIEVE_INTERVAL = config().getInteger("device.retrieve.interval", 1000 * 60);

        // Device 프로파일 조회 (Manufacturer ID, Model ID, Serial Number 이용)
        vertx.eventBus().send(EventBusAddress.SIG_DEVICE_PROFILE_RETRIEVE.toString(), "");
        vertx.setPeriodic( RETRIEVE_INTERVAL, handler -> {
            vertx.eventBus().send(EventBusAddress.SIG_DEVICE_PROFILE_RETRIEVE.toString(), "");
        });

        // 디바이스 정보 받아오면 더이상 조회하지 않고, 디바이스 정보를 cache에 저장
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_MAS_CONNECTION_READY.toString(), this.onSaveDeviceinfo());
    }




    private Handler<Message<JsonObject>> onSaveDeviceinfo()
    {
        return result -> {
            JsonObject put_data = (JsonObject)result.body();

            vertx.eventBus().send(
                    EventBusAddress.CACHE_PUT.toString(),
                    Utils.getCacheQuery(CacheAddress.DEVICEKEY_DEVICEID.toString(),
                            put_data.getString("device_key"),
                            put_data.getString("device_id")
                    )
            );
        };
    }

}
