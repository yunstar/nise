package com.ntels.nise.service.install;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * Created by youngdong on 2016-07-27.
 */
public class ApplicationInstallService extends BaseVerticle{

    @Override
    protected void onInit() {
        super.onInit();

        // install or download 요청
        vertx.eventBus().consumer(EventBusAddress.IN_MAS.APP_INSTALL.toString(), this.onInstallApp());
    }


    private Handler<Message<JsonObject>> onInstallApp()
    {
        return result -> {
            JsonObject rs_install = result.body();
            String work_type = rs_install.getString("work_type");
            switch (work_type){
                case "download":
                    JsonObject query = Utils.getCacheQuery(CacheAddress.DEVICEKEY_DEVICEID.toString(), rs_install.getString("device_key"));
                    // 1. 인증
                    vertx.eventBus().send(EventBusAddress.CACHE_GET.toString(), query, rs_auth -> {
                        if(rs_auth.succeeded()) {
                            // 1. install 파일 다운로드
                            vertx.eventBus().send(EventBusAddress.FTP_DOWNLOAD.toString(), rs_install);
                            // 2. 다운로드 완료 (성공/실패)
                            vertx.eventBus().consumer(EventBusAddress.FTP_APP_DOWNLOAD_RESULT.toString(), rs_download -> {
                                // 3. 결과 포맷 작성
                                vertx.eventBus().send(EventBusAddress.COMP_FORMAT_DOWNLOAD_APP.toString(), rs_download.body(), reply -> {
                                    // 4. MAS로 결과 전송
                                    vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), reply.result().body());
                                });
                            });
                        }else{
                            logger.error("NISE-000006 request not authorized");
                        }
                    });
                    break;
                case "install":
                    // 1. 설치(재실행)
                    //  1) 다운로드 된 jar 파일을 ${NISE_HOME}/lib 경로에 복사
                    //  2) ${NISE_HOME}/restart 라는 파일을 생성하여 재실행 signal 보냄
                    vertx.eventBus().send(EventBusAddress.COMP_INSTALL_APP.toString(), rs_install);

                    // 2. 설치 및 결과
                    //  1) ntels-NISEctl : restart 시그널을 받아서 lib패치 및 재실행
                    //  2) ntels-NISE : MAS 에 mqtt가 정상적으로 접속된것을 확인하면 ${NISE_HOME}/mas.ok 파일생성
                    //  3) ntels-NISEctl : 재실행 30초후에 mas.ok 시그널이 있으면 ${NISE_HOME}/app.patch.ok 파일을,
                    //                    없으면 ${NISE_HOME}/app.patch.fail 파일을 생성 후 프로세스 재실행
                    //  4) ntels-NISE : 재실행 후 app.patch.ok 또는 app.patch.fail 파일이 존재하면, MAS로 패치결과를 전송
                    break;
                default:
                    logger.error("NISE-000010 "+work_type+" is undefined work_type");

            }
        };
    }


}
