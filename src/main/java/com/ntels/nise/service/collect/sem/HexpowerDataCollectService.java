package com.ntels.nise.service.collect.sem;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.shareddata.AsyncMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by youngdong on 2017-11-29.
 */
public class HexpowerDataCollectService extends BaseVerticle {


    @Override
    public void start() throws Exception {
        super.start();

        // SEM(Hexpower) 컴포넌트 실행
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_MAS_PUBLISH_READY.toString(), handler_ready -> {

            vertx.sharedData().getClusterWideMap("init", (AsyncResult<AsyncMap<String,Boolean>> result) -> {
                AsyncMap<String, Boolean> asyncMap = result.result();

                asyncMap.get("sem.hex.regi", go -> {
                    if(go.failed() || go.result()==null || go.result())
                    {
                        vertx.setTimer(5000, h -> {

                            vertx.eventBus().send(EventBusAddress.SIG_START_HEX_COMP.toString(), null);

                            // SEM(Hexpower) 연동을 위한 Serial Router 실행
                            vertx.setTimer(5000, h2 -> {
                                vertx.eventBus().send(EventBusAddress.SIG_START_HEX_ROUTER.toString(), null);
                            });

                        });
                    }
                });
                asyncMap.put("sem.hex.regi", false, h -> {});
            });

        });

    }
}
