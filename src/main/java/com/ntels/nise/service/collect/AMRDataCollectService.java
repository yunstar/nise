package com.ntels.nise.service.collect;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;

/**
 * Created by youngdong on 2016-08-02.
 */
public class AMRDataCollectService extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();

        vertx.eventBus().consumer(EventBusAddress.IN_AMR_TCP.toString(), rs_data -> {
            vertx.eventBus().send(EventBusAddress.COMP_COLLECT_AMR.toString(), rs_data.body(), result -> {
                if(result.succeeded())
                    vertx.eventBus().send(EventBusAddress.OUT_MAS_MQTT.toString(), result.result().body());
                else
                    logger.error("NISE-000038 failed to convert AMR data to MAS format : " + result.cause().getMessage());
            });
        });
    }


}
