package com.ntels.nise.service.collect;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.vertx.BaseVerticle;


/**
 * Created by youngdong on 2016-07-11.
 */
public class NodeDataCollectService extends BaseVerticle {

    @Override
    public void onInit() {

        // OPC UA
        vertx.eventBus().consumer(EventBusAddress.IN_GATEWAY_OPCUA.toString(), rs_data -> {
            vertx.eventBus().send(EventBusAddress.COMP_COLLECT_OPCUA.toString(), rs_data.body());
        });

        // ACS
        vertx.eventBus().consumer(EventBusAddress.IN_ACS_TCP.toString(), rs_data -> {
            vertx.eventBus().send(EventBusAddress.COMP_COLLECT_ACS.toString(), rs_data.body());
        });

        // AMR
        vertx.eventBus().consumer(EventBusAddress.IN_AMR_TCP.toString(), rs_data -> {
            vertx.eventBus().send(EventBusAddress.COMP_COLLECT_AMR.toString(), rs_data.body());
        });
    }


}
