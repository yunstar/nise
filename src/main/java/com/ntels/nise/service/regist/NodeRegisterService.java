package com.ntels.nise.service.regist;

import com.ntels.nise.common.def.CacheAddress;
import com.ntels.nise.common.def.ErrorCode;
import com.ntels.nise.common.query.Query;
import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by youngdong on 2016-07-14.
 */
public class NodeRegisterService extends BaseVerticle {

    @Override
    protected void onInit() {
        super.onInit();

        vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
            AsyncMap<String, Long> asyncMap = result.result();
            asyncMap.put("update.time", 0L, h ->{});
        });

        // Node 등록결과 저장
        loadNode();

        // 1. MAS로 데이터 전송준비가 완료 상태
        vertx.eventBus().consumer(EventBusAddress.SIG_DEVICE_MAS_PUBLISH_READY.toString(), handler_ready -> {

            vertx.sharedData().getClusterWideMap("init", (AsyncResult<AsyncMap<String,Boolean>> result) -> {
                AsyncMap<String, Boolean> asyncMap = result.result();

                asyncMap.get("node.regi", go -> {
                    if(go.failed() || go.result()==null || go.result())
                    {
                        // 1-1. N-MAS로 주기적으로 ping 전송
                        vertx.setPeriodic(config().getJsonObject("gw.status").getInteger("ping_interval", 5000), handler -> {
                            vertx.eventBus().send(EventBusAddress.COMP_GW_PING.toString(), null);
                        });

                        // 1-2. Node등록 init
                        initLoadNode();
                    }
                });
                asyncMap.put("node.regi", false, h -> {});
            });

        });


        // 2. DB Pool connection 유지를 위해 주기적으로 쿼리 실행 (DB Pool 버그) 2017-12-06
        vertx.setPeriodic(60000L, h -> {
            nothingSQLTest();
        });


    }


    /**
     * DB pool connection 을 유지하기 위한 방법
     */
    private void nothingSQLTest()
    {
        vertx.eventBus().send(EventBusAddress.SQL_SELECT.toString(), new JsonObject().put("sql", "SELECT 1"), rs_select -> {
            rs_select.succeeded();
        });
    }


    /**
     * 노드 등록 init
     */
    private void initLoadNode()
    {
        // 1. 노드정보 등록,조회,저장(local DB & cache)
        loadNodeInfo();

        // 2. tag 목록 파일이 수정되었다면 노드 다시 등록
        vertx.setPeriodic(config().getLong("nodes.detect.interval"), handler -> {
            loadNodeInfo();
        });
    }

    /**
     * Node 등록 결과 저장
     */
    private void loadNode()
    {
        // MAS -> Device 노드 등록 결과
        vertx.eventBus().consumer(EventBusAddress.IN_MAS.NODE_REGISTRATION.toString(), rs_node_regi -> {

            // 1. 노드등록결과 array에 tag를 매핑
            vertx.eventBus().send(EventBusAddress.COMP_NODE_MAPPING_TAG.toString(), (JsonObject)rs_node_regi.body(), rs_node_info -> {

                if(rs_node_info.succeeded())
                {
                    JsonArray nodes = ((JsonObject)rs_node_info.result().body()).getJsonArray("rows");
                    logger.info("response node info : " + nodes.toString());

                    // 2. cache에 저장 <tag,node_id> <node_id,tag>
                    vertx.eventBus().send(EventBusAddress.COMP_NODE_CACHE_INSERT.toString(), nodes);

                    // 3. DB에 저장
                    vertx.eventBus().send(EventBusAddress.COMP_NODE_DB_INSERT.toString(), nodes);

//                    // 4. 추가된 Node id를 ServiceHub 로 전달
//                    JsonArray node_map = ((JsonObject)rs_node_info.result().body()).getJsonArray("node_map");
//                    vertx.eventBus().send(EventBusAddress.COMP_SYNC_NODEIDS.toString(), node_map);
                }else{
                    if(((ReplyException)rs_node_info.cause()).failureCode() == 204)
                    {
                        // Signal Node의 경우 skip
                    }else{
                        logger.error(rs_node_info.cause().getMessage());
                    }
                }

            });
        });
    }

    /**
     * Node정보가 변경되면 등록 요청
     */
    private void loadNodeInfo()
    {
        try {
            Files.list(Paths.get(Utils.replaceEnv(config().getString("nodes.csv.path"))))
                    .filter(p -> {
                        String f = p.getFileName().toString().toLowerCase();
                        return f.endsWith(".xml")||f.endsWith(".json");
                    })
                    .sorted((f1, f2) -> Long.compare(f2.toFile().lastModified(), f1.toFile().lastModified()))
                    .limit(1)
                    .forEach(p -> {
//                            System.out.println(p.toAbsolutePath().toString());
//                            System.out.println(p.toFile().lastModified());

                        vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
                            AsyncMap<String, Long> asyncMap = result.result();

                            asyncMap.get("update.time", rs_opc_time -> {
                                if((long)rs_opc_time.result() != p.toFile().lastModified())
                                {
                                    // 최근 설정파일 수정시간을 저장
                                    asyncMap.put("update.time", p.toFile().lastModified(), d -> {});

                                    // opc ua 상태 변경 (파일변경시 OPC UA connection이 끊어지고 재접속을 하는것을 방지) -> 1:설정변경중 0:일반상태
                                    asyncMap.put("status", 1L, d -> {});

                                    String path = Utils.replaceEnv(p.toAbsolutePath().toString());
                                    String EVENTBUS = (path.toLowerCase().endsWith("xml"))?EventBusAddress.SIG_READ_CSV.toString():EventBusAddress.SIG_READ_JSON.toString();
                                    vertx.eventBus().send(EVENTBUS, path, rs_csv -> {
                                        if (rs_csv.succeeded()) {
                                            logger.info("###################################################################");
                                            logger.info("## Load node data from CSV to Cache");
                                            logger.info("###################################################################");
                                            JsonArray csv_data = (JsonArray) rs_csv.result().body();
                                            // ACS와 AMR의 경우는 하나의 Tag에서 데이터가 전부 들어온다. 따라서 고정으로 등록
//                                    csv_data.add("ACS.ACS.ACS");
//                                    csv_data.add("AMR.AMR.AMR");

//                                    // 내부 신호용으로 등록된 Node
//                                    csv_data.add("SIG.SIG.SIG");

                                            // 1-1. 노드 등록 요청 by CSV
                                            registOPCNodes(csv_data);
                                        }
                                    });
                                }
                            });
                        });

                    });
        } catch (IOException e) {
            logger.error("NISE-000042 failed to update node info : " + e.getMessage());
        }
    }


    /**
     * Node 정보를 전달받아서 N-MAS로 등록 요청
     * @param csv_data
     */
    private void registOPCNodes(JsonArray csv_data)
    {

        // 1. Cache에 tag목록을 저장
        logger.info("# 1. save OPC tags in cache");
        vertx.sharedData().getClusterWideMap(CacheAddress.TAGS.toString(), res -> {
            res.result().put(CacheAddress.TAGS.toString(), csv_data, res2 -> {
                if (res2.succeeded()) {

                    // 2. Tag & NodeID 정보 Clear
                    logger.info("# 2. Clear  Tag & NodeID mapping");
                    vertx.eventBus().send(EventBusAddress.COMP_NODE_DATA_CLEAR.toString(), csv_data, res_clear ->{
                        if(res_clear.succeeded())
                        {

                            // 3. Device -> MAS 노드 등록 요청
                            logger.info("# 3. create nodes by MAS");
                            vertx.eventBus().send(EventBusAddress.COMP_FORMAT_NODE_REGIST_OPC.toString(), csv_data);

                            // 4. HEX Power 노드 등록 요청
                            vertx.eventBus().send(EventBusAddress.SIG_GEN_HEX_NODE.toString(), null);

                        }else{
                            logger.error("NISE-000045 failed to clear Node data : " + res_clear.cause().getMessage());
                        }
                    });

                } else {
                    logger.error("NISE-000016 failed to save data in cache : " + res2.cause().getMessage());
                }
            });
        });

        // 2. OPC Tag목록을 ServiceHub 로 전달
//        vertx.eventBus().send(EventBusAddress.COMP_SYNC_TAGS.toString(), csv_data);

    }
}
