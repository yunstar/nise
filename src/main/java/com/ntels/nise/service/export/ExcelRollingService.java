package com.ntels.nise.service.export;

import com.ntels.nise.common.def.EventBusAddress;
import com.ntels.nise.common.util.Utils;
import com.ntels.nise.vertx.BaseVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by youngdong on 2018-03-13.
 */
public class ExcelRollingService extends BaseVerticle {

    private String FILE_PATH = "/logger";

    @Override
    public void start() throws Exception {
        super.start();

        FILE_PATH = config().getString("write.only.path");

        // 1. 가장 최근에 수정된 설정파일시간 초기화
        vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
            AsyncMap<String, Long> asyncMap = result.result();
            asyncMap.put("update.time", 0L, h ->{});
        });

        // 2. 최근 설정파일이 변경되면 re-subscribe
        vertx.setPeriodic(config().getLong("nodes.detect.interval"), handler -> {
            loadNodeInfo();
        });

        // 3. ua데이터를 받아서 처리
        recvData();
    }


    /**
     * OPC-UA 데이터 수집
     */
    private void recvData(){

        vertx.eventBus().consumer(EventBusAddress.IN_GATEWAY_OPCUA.toString(), rs_data -> {

            System.out.println(rs_data.body().toString());

            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = sf.format(new Date());

            JsonObject obj = (JsonObject)rs_data.body();
            JsonObject data = new JsonObject();
            data.put("file", FILE_PATH + File.separator + date.substring(0,10) + ".csv");
            data.put("data", date + "," + obj.getString("id") + "," + obj.getValue("v") + "\n");

            vertx.eventBus().send(EventBusAddress.FILE_APPEND.toString(), data);
        });
    }



    /**
     * Node정보가 변경되면 등록 요청
     */
    private void loadNodeInfo()
    {
        try {
            Files.list(Paths.get(Utils.replaceEnv(config().getString("nodes.csv.path"))))
                    .filter(p -> {
                        String f = p.getFileName().toString().toLowerCase();
                        return f.endsWith(".xml")||f.endsWith(".json");
                    })
                    .sorted((f1, f2) -> Long.compare(f2.toFile().lastModified(), f1.toFile().lastModified()))
                    .limit(1)
                    .forEach(p -> {
//                            System.out.println(p.toAbsolutePath().toString());
//                            System.out.println(p.toFile().lastModified());

                        vertx.sharedData().getClusterWideMap("opc.config", (AsyncResult<AsyncMap<String,Long>> result) -> {
                            AsyncMap<String, Long> asyncMap = result.result();

                            asyncMap.get("update.time", rs_opc_time -> {
                                if((long)rs_opc_time.result() != p.toFile().lastModified())
                                {
                                    // 최근 설정파일 수정시간을 저장
                                    asyncMap.put("update.time", p.toFile().lastModified(), d -> {});

                                    // opc ua 상태 변경 (파일변경시 OPC UA connection이 끊어지고 재접속을 하는것을 방지) -> 1:설정변경중 0:일반상태
                                    asyncMap.put("status", 1L, d -> {});

                                    String path = Utils.replaceEnv(p.toAbsolutePath().toString());
                                    String EVENTBUS = (path.toLowerCase().endsWith("xml"))? EventBusAddress.SIG_READ_CSV.toString():EventBusAddress.SIG_READ_JSON.toString();
                                    vertx.eventBus().send(EVENTBUS, path, rs_csv -> {
                                        if (rs_csv.succeeded()) {
                                            logger.info("###################################################################");
                                            logger.info("## Load node data from CSV to Cache");
                                            logger.info("###################################################################");
                                            JsonArray csv_data = (JsonArray) rs_csv.result().body();

                                            System.out.println( csv_data.toString() );

                                            vertx.eventBus().send(EventBusAddress.IN_GATEWAY_OPCUA_SUBSCRIBE.toString(), csv_data);
                                        }
                                    });
                                }
                            });
                        });

                    });
        } catch (IOException e) {
            logger.error("NISE-000042 failed to update node info : " + e.getMessage());
        }
    }
}
