package com.ntels.nise.common.def;

/**
 * Created by youngdong on 2016-07-21.
 */
public enum CacheAddress {
    NODEID_TAG,
    TAG_NODEID,
    DEVICEKEY_DEVICEID,
    GW_STATUS,
    TAGS,
//    SIG_NODEID,
    NODEID_TAG_MAP,
    ;
}
