package com.ntels.nise.common.def;

/**
 * Created by youngdong on 2016-07-12.
 */
public class ErrorCode {

    public static final int OK  = 200;
    public static final int SQL_ERROR  = 100;
    public static final int SERVER_ERROR  = 500;
    public static final int BAD_REQUEST  = 400;
    public static final int NOT_FOUND  = 404;
    public static final int NO_DATA  = 204;

    public static final int DOWNLOAD_ERROR  = 10;
}
