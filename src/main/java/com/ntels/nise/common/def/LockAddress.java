package com.ntels.nise.common.def;

/**
 * Created by youngdong on 2018-04-26.
 */
public enum LockAddress {

    MQTT_RE_CONN,
    OPC_RE_CONN
}
