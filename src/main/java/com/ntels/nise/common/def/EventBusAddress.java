package com.ntels.nise.common.def;

/**
 * Created by youngdong on 2016-08-10.
 */
public enum EventBusAddress {


    /** SQL **/
    SQL_EXEC,
    SQL_SELECT,
    SQL_UPDATE,
    SQL_UPDATE_BATCH,


    /** Cache **/
    CACHE_PUT,
    CACHE_GET,
    CACHE_ADD,
    CACHE_REMOVE,


    /** Component **/
    COMP_COLLECT_OPC,
    COMP_COLLECT_ACS,
    COMP_COLLECT_AMR,
    COMP_CONTROL,
    COMP_FORMAT_DOWNLOAD_APP,
    COMP_FORMAT_DOWNLOAD_FIRMWARE,
    COMP_INSTALL_APP,
    COMP_INSTALL_FIRMWARE,
    COMP_NODE_MAPPING_TAG,
    COMP_NODE_DB_INSERT,
    COMP_TAG_DB_INSERT,
    COMP_NODE_CACHE_INSERT,
    COMP_FORMAT_NODE_REGIST_OPC,
    COMP_FORMAT_NODE_REGIST_CSV,
    COMP_FORMAT_NODE_REGIST_DB,
    COMP_FORMAT_NODE_EXEC_CMD,
    COMP_FORMAT_NODE_EXEC_RES,
    COMP_COLLECT_OPCUA,
    COMP_COLLECT_DEFAULT,
    COMP_COLLECT_IEQ,
    COMP_SYNC_TAGS,
    COMP_SYNC_NODEIDS,
    COMP_NODE_DATA_CLEAR,
    COMP_GW_PING,

    /** Signal **/
    SIG_DEVICE_PROFILE_RETRIEVE,
    SIG_READ_CSV,
    SIG_READ_JSON,
    SIG_DEVICE_MAS_MQTT_CONNECT_RESET,
    SIG_DEVICE_MAS_PUBLISH_READY,
    SIG_DEVICE_MAS_CONNECTION_READY,
    SIG_DEVICE_GW_REPORT_ON_OFF,
    SIG_START_HEX_COMP,
    SIG_START_HEX_ROUTER,
    SIG_GEN_HEX_NODE,

    /** Router **/
    OUT_MAS_MQTT,
    OUT_GATEWAY_MQTT,
    IN_GATEWAY_MQTT,
    IN_ACS_TCP,
    IN_AMR_TCP,
    OUT_ACS_TCP,
    OUT_AMR_TCP,
    FTP_DOWNLOAD,
    FTP_APP_DOWNLOAD_RESULT,
    FTP_FIRMWARE_DOWNLOAD_RESULT,
    OUT_GATEWAY_OPCUA,
    IN_GATEWAY_OPCUA,
    IN_GATEWAY_OPCUA_SUBSCRIBE,
    IN_HEXP_SERIAL,
    OUT_HEXP_SERIAL,
    FILE_APPEND,
;



    /** MAS In Message Type  **/
    public enum IN_MAS {
        NODE_REGISTRATION("in.mqtt.mas.node.regi"),
        NODE_COMMAND("in.mqtt.mas.node.cmd"),
        DEVICE_COMMAND("in.mqtt.mas.device.cmd"),
        APP_INSTALL("in.mqtt.mas.app"),
        FIRMWARE_INSTALL("in.mqtt.mas.firmware");

        private String message_type;
        private IN_MAS(String message_type){
            this.message_type = message_type;
        }
        public String toString(){
            return message_type;
        }
    }



}
