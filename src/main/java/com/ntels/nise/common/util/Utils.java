package com.ntels.nise.common.util;

import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import java.lang.ProcessBuilder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by youngdong on 2016-07-13.
 */
public class Utils {

    public static final Logger logger = LoggerFactory.getLogger(Utils.class);
    private static String NISE_HOME = System.getenv("NISE_HOME");
    private static Pattern pattern = Pattern.compile("%([^%]+)%");
//    private static Map<String, Integer> m = new HashMap<String, Integer>();
//    private static byte[] hexb = {0x06, 0x04, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x52};
//    static{
//        m.put("06", 0);m.put("04", 1);m.put("30", 2);m.put("31", 3);m.put("32", 4);m.put("33", 5);m.put("34", 6);m.put("35", 7);m.put("36", 8);m.put("37", 9);
//        m.put("38", 10);m.put("39", 11);m.put("61", 12);m.put("62", 13);m.put("63", 14);m.put("64", 15);m.put("65", 16);m.put("66", 17);m.put("52", 18);
//    }

    public static String getHome()
    {
        return NISE_HOME;
    }

    public static String getCurrentTime()
    {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return df.format(LocalDateTime.now(ZoneOffset.UTC));
    }


    public static String getFormatDate(String format, long millisecond)
    {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(format);
        return df.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(millisecond), ZoneOffset.UTC));
    }


    public static JsonObject getCacheQuery(String category, String key)
    {
        JsonObject query = new JsonObject();
        query.put("map", category);
        query.put("key", key);
        return query;
    }

    public static JsonObject getCacheQuery(String category, String key, String value)
    {
        JsonObject query = new JsonObject();
        query.put("map", category);
        query.put("key", key);
        query.put("value", value);
        return query;
    }



    public static void command_exec(String... cmd) throws Exception
    {

        Process proc = null;
        BufferedReader in = null;

        try {
            for(String c:cmd)
                logger.debug("Exec cmd : " + c);
            ProcessBuilder pb = new ProcessBuilder( cmd );
            pb.redirectErrorStream(true);
            proc = pb.start();
            proc.getOutputStream().close();
            in = new BufferedReader(new InputStreamReader(proc.getInputStream()));

            String word = "";

            while((word = in.readLine())!=null)
            {
                System.out.println(word);
            }

            proc = null;

        } catch (Exception e) {
            // TODO: handle exception
            logger.error("NISE-000000 external command error : " + e.getMessage());
        } finally {
            proc = null;
            try{ if(in!=null) in.close(); }catch(Exception e){}

        }

    }


    public static void createFile(String path){
        try{
            if(!Files.exists(Paths.get(path)))
                Files.createFile(Paths.get(path));
        }catch (IOException e){
            logger.error("NISE-000001 Could not create file " + path + "\n" + e.getMessage());
        }
    }

    public static void writeFile(String file, String line){
        Path path = Paths.get(file);
        try {
            try (BufferedWriter writer = Files.newBufferedWriter(path)) {
                writer.write(line);
            }
        } catch (IOException e) {
            logger.error("NISE-000001 Could not write file " + path + "\n" + e.getMessage());
        }
    }

    public static String readLine(String file, String default_value)
    {
        String result = default_value;
        Path path = Paths.get(file);
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            result = reader.readLine();
        }catch (IOException e) {
            logger.error("NISE-000002 Could not read file from " + path + "\n" + e.getMessage());
        }

        return result;
    }

    public static boolean isWindows()
    {
        String os_name = System.getProperty("os.name").toLowerCase();
        return (os_name.indexOf("win") >= 0 );
    }


    public static String getPID()
    {
        String name = ManagementFactory.getRuntimeMXBean().getName();
        return name.substring(0, name.indexOf("@"));
    }


    /**
     * 환경변수 적용
     * @param targetStr
     * @return
     */
    public static String replaceEnv(String targetStr)
    {
        Matcher matcher = pattern.matcher(targetStr);

        while(matcher.find())
        {
            String env_name = matcher.group(1);
            String env_value = System.getenv(env_name);

            if(env_value!=null)
                targetStr = targetStr.replace("%" + env_name + "%", env_value);
        }

        return targetStr;
    }


    private static String padLeftFixed(String input, int size){
        return input.length() < size ? "00000000000000000000".substring(20 - (size-input.length())) + input : input;
    }

    public static String[] hexa2Binary(byte b[])
    {
        String result = padLeftFixed( Integer.toBinaryString( Integer.parseInt(new String(b), 16) ), b.length * 4 );
        return result.split("");
    }

    public static String[] hexa2BinaryReverse(byte b[])
    {
        String result = padLeftFixed( Integer.toBinaryString( Integer.parseInt(new String(b), 16) ), b.length * 4 );
        result = new StringBuffer(result).reverse().toString();
        return result.split("");
    }

    /**
     * 0을 1로 1을 0으로 변경
     * @param values
     * @return
     */
    public static String[] reverseValue(String[] values)
    {
        int size = values.length;

        for(int i=0;i<size;i++)
        {
            if(values[i].equals("0"))
                values[i] = "1";
            else
                values[i] = "0";
        }

        return values;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }


    /**
     * hex 값을 10진수로 변환
     * @param b
     * @return
     */
    public static int hexa2Integer(byte b[])
    {
        return Integer.parseInt(new String(b), 16);
    }

}
