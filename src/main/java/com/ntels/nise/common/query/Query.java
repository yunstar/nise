package com.ntels.nise.common.query;

/**
 * Created by youngdong on 2016-07-15.
 */
public enum Query {

    /** Select **/
    SELECT_NODE_INFO_ALL("SELECT tag, node_mf_id, node_model_id, node_sn, node_id FROM NODE_INFO"),
    SELECT_NODE_INFO_TAG_NODEID("SELECT tag, node_id FROM NODE_INFO"),


    /** Insert **/
    INSERT_NODE_BATCH("INSERT ignore INTO NODE_INFO( TAG, NODE_ID, NODE_MF_ID, NODE_MODEL_ID, NODE_SN) VALUES(?,?,?,?,?) ON DUPLICATE KEY UPDATE NODE_ID=?, NODE_MF_ID=?, NODE_MODEL_ID=?, NODE_SN=?"),


    /** Create **/

    /** Update **/

    /** Delete **/


    /** Truncate **/
    TRUNCATE_NODE_INFO("TRUNCATE TABLE NODE_INFO"),



    ;


    private String sql;
    private Query(String sql){
        this.sql = sql;
    }
    public String toString(){
        return sql;
    }

}
