setx NISE_HOME %cd%
set NISE_HOME=%cd%

set BAT_FILE=%NISE_HOME%\nise.bat
set CTL_BAT_FILE=%NISE_HOME%\control.bat



echo set NISE_HOME=%NISE_HOME%>%BAT_FILE%
echo set NISE_HOME=%NISE_HOME%>%CTL_BAT_FILE%

type %BAT_FILE%.tmp >> %BAT_FILE%
type %CTL_BAT_FILE%.tmp >> %CTL_BAT_FILE%


nssm-2.24\win64\nssm install ntels-NISE %NISE_HOME%\nise.bat
nssm-2.24\win64\nssm install ntels-NISEctl %NISE_HOME%\control.bat
nssm-2.24\win64\nssm install ntels-MQTT %NISE_HOME%\hivemq-3.1.5\bin\run.bat
nssm-2.24\win64\nssm install ntels-ACS-logstash %NISE_HOME%\logstash-2.3.4\bin\ntels-ACS.bat


timeout 5