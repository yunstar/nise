verticle-deploy com.ntels.nise.service.regist.NodeRegisterService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.service.collect.NodeDataCollectService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.service.control.DeviceRetrieveService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.service.control.NodeControlService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.service.control.DeviceControlService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false

verticle-deploy com.ntels.nise.io.router.MqttMASOutRouter -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.io.router.MqttMASInRouter -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.io.router.MqttGatewayInRouter -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.io.router.MqttGatewayOutRouter -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false

verticle-deploy com.ntels.nise.component.repository.CacheService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=true
verticle-deploy com.ntels.nise.component.repository.JDBCService -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=true
verticle-deploy com.ntels.nise.component.common.ParserComponent -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=true
verticle-deploy com.ntels.nise.component.device.data.CollectComponent -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=false
verticle-deploy com.ntels.nise.component.device.regist.RegisterComponent -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=true
verticle-deploy com.ntels.nise.component.device.control.NodeControlComponent -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=true
verticle-deploy com.ntels.nise.component.device.control.DeviceControlComponent -c=C:\project\nise\src\main\resources\nise-config.json -ha=true -worker=true