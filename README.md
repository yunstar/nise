# g-3000

building gateway g-3000


### 1. what is g-3000?

g-3000 gateway는 디바스영역에서 다양한 프로토콜을 지원하는 데이터 수집/제어 게이트웨이로 Cloud로 연결을 지원한다. 
산업용 뿐 아니라 IoT Home 영역에서도 확장이 가능한 구로고 Adapter를 추가 개발하여 연동 할 수 있다. 


<img src="./image/nise_architecture.JPG">

#### Router 
- 외부 시스템과 통신하기 위한 모듈
- In/Out 으로 구분되며 MQTT, RESTful, TCP, FTP 지원
- Adapter 추가 개발을 통한 신규 프로토콜 연계 가능

#### Component
- File I/O, JDBC, Ignite cache 서비스등을 제공

#### Service 
- 실제 비즈니스록직이 포함된 부분으로 데이터 수집과제어, 스케줄링 제공

#### EventBus
- 내부 통신 채널로 Vert.x 의 Ignite 를 통한 비동기 처리가 가능하다. 


### 2. g-3000 활용 예제
1) 센서 데이터 실시간 조회 및 제어
- 디바이스와 연결된 센서들의 데이터를 실시간 모티너링 가능하며 cloud를 통한 원격 제어 가능
2) 원격 디바이스 관리
 - 원격에서 디바이스 on/off 및 업데이트 가능한 디바이스 관리기능


### 3. 적용 오픈소스 및 기술
1) CEP Engine
- Java 1.8
- [Vert.x](https://vertx.io/)
- [Apache Ignite](https://ignite.apache.org/)
- [Docker, Dokcer Compose](https://www.docker.com/)

### 4. Vert.x structure
<img src="./image/vertx_structure.JPG">

